%%
foregroundDetector = vision.ForegroundDetector('NumGaussians', 8, ...
    'NumTrainingFrames', 5);
blobAnalyser = vision.BlobAnalysis('BoundingBoxOutputPort', true, ...
    'AreaOutputPort', true, 'CentroidOutputPort', true, ...
    'MinimumBlobArea', 400);
% videoReader = vision.VideoFileReader('visiontraffic.avi');
for i = 1:length(ilist)
%     frame =fcn_Normalize(im2double(imread(ilist{i}))); % read the next video frame
%     frame = im2uint8(frame);
    frame = im2double(imread(ilist{i}));
    %     foreground = step(foregroundDetector, frame);
    % mask = (frame);
    % pause(0.01);
    i
    
    % Detect foreground.
    mask = foregroundDetector.step(frame);
    
    % Apply morphological operations to remove noise and fill in holes.
    mask = imdilate(mask, strel('rectangle', [5,5]));
        mask = imfill(mask, 'holes');

    mask = imerode(mask, strel('rectangle', [3,3]));
    mask = imopen(mask, strel('rectangle', [3,3]));
    mask = imclose(mask, strel('rectangle', [5,5]));
    mask = imfill(mask, 'holes');
    
    % Perform blob analysis to find connected components.
    %     [~, centroids, bboxes] = blobAnalyser.step(mask);
    %     result = insertShape(uint8(mask), 'Rectangle', bboxes, 'Color', 'green');
    
    %     numCars = size(bboxes, 1);
    %     result = insertText(result, [10 10], numCars, 'BoxOpacity', 1, ...
    %         'FontSize', 14);
    %     figure;
    %     imshow(result); title('Detected Cars');
    imshow(mask);
    if i>585
    temp_fg(:,:,i-585) = mask;
    end
    pause(0.5)
end
% foregroundDetector.step

%%
% subplot(4,4,1);
for i=100:124
    i
    subplot(5,5,i-99)
    imshow(temp_fg(:,:,i));
    num_frame = ['frame  ',num2str(i)];
    title(num_frame);
end