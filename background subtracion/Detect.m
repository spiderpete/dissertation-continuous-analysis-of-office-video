  %% current test
    %     ini_bg = imread(ilist{1});
    ppl_label = [1 0 0];
    ini_bg = imresize(imread(ilist{1}),[360 640]);
    pre_img = imread(ilist{4}); % is not a double type
    count =1;
    [pre_fore,improp] = Bgsub_re(ini_bg,pre_img);
    phist = histo(pre_fore,imresize(pre_img,[360 640]));
%     [m,n] = ind2sub(size(fore),improp.PixelIdxList(1));
    %     temp_list = {};
    for i=4:length(ilist)
%     for i=7:28
        tic
        %     imshow(ilist{i});
        curr_img = imread(ilist{i});
        [fore,improp] = Bgsub_re(ini_bg,curr_img);
        if ~isempty(improp.BoundingBox)
            thist = histo(fore,imresize(curr_img,[360 640]));
%             phist = histo(pre_fore,imresize(pre_img,[360 640]));
            [ppl_label]= tracking(thist,phist,pre_fore,fore,ppl_label);
            ppl_label
        end
        
%         subplot(3,4,i-3),
%         imshow(imresize(curr_img,[360 640]));
%         num_frame = ['frame ',num2str(i-3)]
%         title(num_frame);
%         hold on
%         rectangle('position', improp.BoundingBox(ppl_label(1),:), 'EdgeColor', 'g');
%         plot(improp.Centroid(ppl_label(1),1),improp.Centroid(ppl_label(1),2),'g-*');
%         hold off
        
        subplot(4,1,4),imshow(fore);
        
        temp_fore = zeros(size(fore));
        temp_fore(improp.PixelIdxList{ppl_label(1)})=1;
        temp_cen = improp.Centroid(ppl_label(1),:)*2;
        fore = temp_fore;
%         ppl_label(1) = 1;
        thist = histo(fore,imresize(curr_img,[360 640]));
        
        figure(1);
        subplot(2,2,1),imshow(ini_bg);        
        if count>=8; %used to Update image every 8 frame
            if ~isempty(improp.BoundingBox)
                [chang_flag,mask_list] = getBoundingindex(improp.BoundingBox(ppl_label(1),:),360,640);
                %     temp_list = improp.PixelIdxList(1);
                %     for ii=2:length(improp.PixelIdxList)
                %         temp_list = [temp_list,improp.PixelIdxList(ii)];
                %     end
                %         temp_pixellist = cell2mat(temp_list);
                %         clear temp_list;
                %         mask_list = temp_pixellist;
            else
                mask_list=0;
            end
            [ini_bg] = BgUpdate(imresize(imread(ilist{i}),[360 640]),ini_bg,mask_list);
            count =1;
        end
        ppl_label(1) = 1;
        count = count+1;
%         figure(2);
        %     imshow(img);
        %     figure(2)
        %     imshow(imread(ilist{i}));
        
        subplot(4,1,2),imshow(imread(ilist{i}));
        hold on
        plot(temp_cen(1,1),temp_cen(1,2),'r-*');
        hold off
        subplot(4,1,3),imshow(fore);
        subplot(4,1,1),imshow(ini_bg);
%     imshow(ini_bg);

        %     figure(2);
        %     imshow(ini_bg)
        %     hold on
        %     for i = 1:length(improp.Area)
        %      rectangle('position', improp.BoundingBox(i,:), 'EdgeColor', 'r');
        %     end
        %     hold off
        toc
        pause(0.05);
        %
%         if i<38
        pre_img = curr_img;
        pre_fore = fore;     
        clearvars phist;
        phist = thist;
        clearvars thist;
%         end
%         feature_acq(i-3,1) = temp_cen(1,1);
%         feature_acq(i-3,2) = temp_cen(1,2);
%         feature_acq(i-3,3) = toc;
    end
    
    %% test without bgupdate
%     ini_bg = imread(ilist{1});
    ini_bg = imresize(imread(ilist{1}),[360 640]);
    for i=1:length(ilist)
        i
%     imshow(ilist{i});
    [img,improp] = Bgsub_re(ini_bg,imread(ilist{3}));
    imshow(img);
    pause(0.01);
    temp_img(:,:,i) = img;
    pause(0.01)
%     
    end
 subplot(1,3,1), imshow(ini_bg);
  subplot(1,3,2), imshow(imread(ilist{3}));
    subplot(1,3,3), imshow(img);
