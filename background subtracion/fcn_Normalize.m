function normalImage = fcn_Normalize(image)
  % convert to normalised RGB
  image_sum = (image(:,:,1)+image(:,:,2)+image(:,:,3));
  normalImage(:,:,1) = image(:,:,1)./image_sum; 
  normalImage(:,:,2) = image(:,:,2)./image_sum;
  normalImage(:,:,3) = image(:,:,3)./image_sum;
end