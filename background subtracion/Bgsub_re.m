function [img,improp]= Bgsub_re(Image_before,Image_after)
    %eliminate the effect of the door
    diffThresh = 0.03;
    doorThresh = 0.06;
%     normal_IBefore = fcn_Normalize(double(Image_before));
%     normal_IAfter = fcn_Normalize(double(Image_after));
    normal_IBefore = imresize(fcn_Normalize(double(Image_before)),[360 640]);
    normal_IAfter = imresize(fcn_Normalize(double(Image_after)),[360 640]);
    Idiff = imabsdiff(normal_IBefore,normal_IAfter);
    Ifound = (Idiff(:,:,1)>diffThresh) | (Idiff(:,:,2)>diffThresh) | (Idiff(:,:,3)>diffThresh);
    getlist = find(Ifound==1);
    [m,n] = size(Ifound);
    s = [m,n];
    [I,J] = ind2sub(s,getlist);
    
    %eliminate the effect of door by specifying the color in current image
    for i=1:length(I)
        if (abs(normal_IAfter(I(i),J(i),1)-0.43)<doorThresh)&&(abs(normal_IAfter(I(i),J(i),2)-0.32)<doorThresh)&&(abs(normal_IAfter(I(i),J(i),3)-0.22)<doorThresh)
            Ifound(I(i),J(i))=0;
        end
    end
    
    %eliminate the effect of door by specify the color in background image
    for i=1:length(I)
        if (abs(normal_IBefore(I(i),J(i),1)-0.43)<doorThresh)&&(abs(normal_IBefore(I(i),J(i),2)-0.32)<doorThresh)&&(abs(normal_IBefore(I(i),J(i),3)-0.22)<doorThresh)
            Ifound(I(i),J(i))=0;
        end
    end
    
    
    imask = strel('square',3);
    imask5 = strel('square',5);
    imask3 = strel('square',3);
    imask8 = strel('square',8);
    imask10 = strel('square',10);
    
%     Iclean = Ifound;
    Iclean = imclose(Ifound,strel('square',2));
    Iclean = imfill(Iclean,'holes');
    Iclean = imerode(Iclean,imask3);
    Iclean(m,:)=1;
    Iclean = imfill(Iclean,'holes');
    Iclean(m,:)=0;
    Iclean = imclose(Iclean,imask3);
    Iclean = imclose(Iclean,imask3);
    Iclean = imclose(Iclean,imask3);
    Iclean = imdilate(Iclean,imask5);
    Iclean = imclose(Iclean,imask5);
%     Iclean = imopen(Iclean,imask3);
 
    
    
    Iclean(m,:)=1;
    Iclean = imfill(Iclean,'holes');
    Iclean(m,:)=0;
    
    
    prop = regionprops('table',Iclean,'area','PixelIdxList');
    a = find(prop.Area<2000);
    for i=1:length(a)
        Iclean(prop.PixelIdxList{a(i)})=0; %clear small object
    end
    Iclean = imdilate(Iclean,imask8);
    Iclean = imfill(Iclean,'holes');


    prop = regionprops('table',Iclean,'area','PixelIdxList','BoundingBox','Centroid');

    
    % carefully consider this part
    %remove this part when finished background update
%     maxind = find(prop.Area==max(prop.Area));
%     if 2*prop.Area(maxind)-sum(prop.Area)>30000
%         temp = zeros(720,1280);
%         temp(prop.PixelIdxList{maxind})=1;  %if there is an area much larger than the other, remove the other object
%     else
%         temp = Iclean;
%     end
%  
    temp = Iclean;
    img = temp;
    improp = prop;
%     imshow(img);
%     hold on
%     for i = 1:length(prop.Area)
%      rectangle('position', prop.BoundingBox(i,:), 'EdgeColor', 'r');  
%     end  
%     hold off
end
