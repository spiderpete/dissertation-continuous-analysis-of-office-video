function [img] = NormalizeGray(rgbImage)
grayImage = rgb2gray(rgbImage);
originalMinValue = double(min(min(grayImage)))
originalMaxValue = double(max(max(grayImage)))
originalRange = originalMaxValue - originalMinValue;

% % Get a double image in the range 0 to +255
% desiredMin = 0;
% desiredMax = 255;
% desiredRange = desiredMax - desiredMin;
% dblImageS255 = desiredRange * (double(grayImage) - originalMinValue)/originalRange + desiredMin;

% Get a double image in the range 0 to +1
desiredMin = 0;
desiredMax = 1;
desiredRange = desiredMax - desiredMin;
dblImageS1 = desiredRange * (double(grayImage) - originalMinValue)/originalRange + desiredMin;

img = dblImageS1;
end