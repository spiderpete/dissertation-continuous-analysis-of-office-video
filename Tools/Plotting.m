load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
day_01{2}{1}(1,:)
day_01{2}{2}(1,:)
day_01{2}{3}(1,:)
day_01{2}{4}(1,:)
day_01{2}{5}(1,:)
day_01{2}{6}(1,:)
day_01{2}{7}(1,:)
day_01{2}{8}(1,:)
day_01{2}{9}(1,:)
day_01{2}{10}(1,:)
day_01{2}{11}(1,:)
clear
autoBehaviorAnnotation
day_01{2}{1}(1,:)
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
day_01{2}{1}(1,:)
day_01{2}{2}(1,:)
autoBehaviorAnnotation
clear
autoBehaviorAnnotation
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
day_01{2}{1}(1,3)
autoBehaviorAnnotation
clear
autoBehaviorAnnotation
clear
autoBehaviorAnnotation
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
day_01{2}{1}(1,3)
clear
autoBehaviorAnnotation
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
day_01{2}{9297}(1,3)
day_01{2}{9298}(1,3)
day_01{2}{1098}(1,3)
day_01{2}{10098}(1,3)
day_01{2}{10000}(1,3)
clear
behaviourCuration
clear
behaviourCuration
zeros(3)
zeros(1, 3)
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
clear
positionError
manualData{2}{9298}(1, 1:2)
manualData{2}{9298}(2, 1:2)
manualData{2}{9298}(3, 1:2)
clear
positionError
errors(1,1)
errors(10,1)
errors(1,10)
errors(1,100)
errors(1,1000)
errors(1,5000)
errors == 0
clear
positionError
sum(errors(1,:) == 0)
sum(errors(1,:) < 100)
sum(errors(1,:) < 200)
sum(errors(1,:) < 300)
sum(errors(1,:) < 400)
sum(errors(1,:) < 500)
sum(errors(1,:) < 600)
sum(errors(1,:) < 900)
sum(errors(1,:) < 1000)
sum(errors(1,:) < 8500)
sum(errors(1,:) < 10000)
sum(errors(1,:) < 10000000)
sum(errors(1,:) < 8000)
sum(errors(1,:) < 7000)
sum(errors(1,:) < 4000)
sum(errors(1,:) < 1000)
sum(errors(1,:) < 2000)
sum(errors(1,:) < 1500)
sum(errors(1,:) < 1200)
sum(errors(1,:) < 1100)
sum(errors(1,:) < 1050)
sum(errors(1,:) < 1051)
sum(errors(1,:) < 1070)
sum(errors(1,:) < 1066)
sum(errors(1,:) < 1063)
sum(errors(1,:) < 1061)
sum(errors(1,:) < 1060)
sum(errors(1,:) < 1061)
for person = 1 : NUM_PEOPLE
bins = zeros(1, 7);
for bin = 1 : 7
bins(bin) = sum(errors(person,:) < bin*0.1*diagonal);
end
end
diagonal = sqrt(720*720 + 1280* 1280);
for person = 1 : NUM_PEOPLE
bins = zeros(1, 7);
for bin = 1 : 7
bins(bin) = sum(errors(person,:) < bin*0.1*diagonal);
end
end
bins
for person = 1 : NUM_PEOPLE
bins = zeros(1, 8);
for bin = 1 : 8
bins(bin) = sum(errors(person,:) < bin*0.1*diagonal);
end
end
bins
diagonal
bins = zeros(NUM_PEOPLE, 8);
for person = 1 : NUM_PEOPLE
for bin = 1 : 8
bins(person, bin) = sum(errors(person,:) < bin*0.1*diagonal);
end
end
bins
histogram(bins(1,:), 8)
histogram(bins(1,:))
histogram(8, bins(1,:))
histogram(bins(1,:))
bins(1,1)
histogram(errors(1,:))
histogram(errors(1,:), 8)
histogram(errors(1,:), 7)
hold on
title('Number of misplased positions with tolerance');
xlabel('Tolerance in pixels')
ylabel('Number of frames wrong')
xticklabels({'10%','20%','30%', '40%','50%','60%','70%', '80%'})
histogram(errors(2,:), 7)
hold off
histogram(errors(2,:), 7)
hold on
title('Number of misplased positions with tolerance');
xlabel('Tolerance in pixels')
ylabel('Number of frames wrong')
title('Number of misplased positions with tolerance for person 2');
hold off
histogram(errors(1,:), 7)
title('Number of misplased positions with tolerance for person 1');
xlabel('Tolerance in pixels')
ylabel('Number of frames wrong')
hold off
histogram(errors(3,:), 7)
title('Number of misplased positions with tolerance for person 3');
xlabel('Tolerance in pixels')
ylabel('Number of frames wrong')
currentFrame = 3000;
frame = imread(fileList{currentFrame});
figure(1);
imgHandle = imshow(frame);
hold on
pos = day_01{2}{currentFrame}(1:3,1:2);
pos = manualData{2}{currentFrame}(1:3,1:2);
plot(pos(1,1), pos(1,2), 'b*');
plot(pos(2,1), pos(2,2), 'b*');
plot(pos(3,1), pos(3,2), 'b*');
posAuto = autoData{2}{currentFrame}(1:3,1:2);
autoData{2}{currentFrame}(1,1:2);
autoData{2}{currentFrame}(1,1:2)
autoData{2}{currentFrame}(2,1:2)
autoData{2}{currentFrame}(3,1:2)
titles = manualData{2}{currentFrame}(1:3,3);
title([titles(1), ' : ', titles(2), ' : ', titles(3)]);
title([num2str(titles(1)), ' : ', num2str(titles(2)), ' : ', num2str(titles(3))]);
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
titles = day_01{2}{currentFrame}(1:3,3);
title([num2str(titles(1)), ' : ', num2str(titles(2)), ' : ', num2str(titles(3))]);
title(['Behaviours in turn: ', num2str(titles(1)), ' : ', num2str(titles(2)), ' : ', num2str(titles(3))]);
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto/Day01.mat')
posAuto = day_01{2}{currentFrame}(1:3,1:2);
posAuto = day_01{2}{currentFrame}(1,1:2);
plot(posAUto(1,1), posAuto(1,2), 'r*');
plot(posAuto(1,1), posAuto(1,2), 'r*');
currentFrame = 50
frame = imread(fileList{currentFrame});
igure(2);
figure(2);
imgHandle = imshow(frame);
hold on
posA = day_01{2}{currentFrame}(1,1:2)
plot(posA(1,1), posA(1,2), 'r*');
posAAuto = manualData{2}{currentFrame}(1,1:2)
plot(posAAuto(1,1), posAAuto(1,2), 'b*');
plot(posA(1,1)+1, posA(1,2)+1, 'r*');
plot(posAAuto(1,1), posAAuto(1,2), 'b*');
behA = manualData{2}{currentFrame}(1,3)
behA = day_01{2}{currentFrame}(1,3)
behA = autoData{2}{currentFrame}(1,3)
autoBehaviorAnnotation
behA = day_01{2}{currentFrame}(1,3)
load('/Users/spiderpete/Documents/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Auto2/Day01.mat')
behA = day_01{2}{currentFrame}(1,3)
behA = day_01{2}{50}(1,3)
hold on
title(['Behaviour is: ', num2str(behA)])
%-- 13/12/2016, 10:47 --%
load('Day01.mat')
auto = day_01;
clear day_01
load('Day01.mat')
manual = day_01;
clear day_01;
auto{2}{1}(1,:)
manual{2}{1}(1,:)
size(manual({2}{1}))
size(manual({2}{1})
size(manual{2}{1})
size(manual{2})
for i = 1 : 10047
result(i) = abs(pdist(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2));
end
for i = 1 : 10047
result(i) = abs(pdist(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2)));
end
for i = 1 : 10047
result(i) = abs(pdist(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2), 'euclidean'));
end
for i = 1 : 10047
result(i) = abs(pdist(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2), 'euclidean'));
end
for i = 1 : 10047
result(i) = pdist(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2), 'euclidean');
end
for i = 1 : 10047
result(i) = pdist2(auto{2}{i}(1,1:2), manual{2}{i}(1,1:2), 'euclidean');
end
result(40)
result(400)
result(4000)
result(5400)
result
max(max(result))
result(<10)
result(result<10)
result < 10
result == 0
result(result == 0)
result(result == 0, result == 1)
edges = [0 10, 0 20];
edges = [0 10 : 0 20];
edges = [-10 -2:0.25:2 10];
diag = 1470
result(result < 0.1*diag);
size(result(result < 0.1*diag))
size(result(result < 0.1*diag), 2)
size(result(result < 0.2*diag), 2)
size(result(result < 0.3*diag), 2)
size(result(result < 0.4*diag), 2)
size(result(result < 0.5*diag), 2)
size(result(result < 0.6*diag), 2)
size(result(result < 0.7*diag), 2)
size(result(result < 0.8*diag), 2)
for i = 1 : 10
counts(i) = size(result(result < i*diag/10), 2);
end
histogram('Categories', {'<10%', '<20%', '<30%', '<40%', '<50%', '<60%', '<70%', '<80%', '<90%', '<100%'}, 'BinCounts', counts);
histogram('Categories',{'Yes','No','Maybe'},'BinCounts',[22 18 3])
histogram('Categories',['Yes','No','Maybe'],'BinCounts',[22 18 3])
doc histogram
doc hist
histogram(_, 'Categories',['Yes','No','Maybe'],'BinCounts',[22 18 3])
histogram(result, 'Categories',['Yes','No','Maybe'],'BinCounts',[22 18 3])
histogram(result, 'Categories',{'Yes','No','Maybe'},'BinCounts',[22 18 3])
histogram(result, 'BinLimits', [0 10, 0 20])
edges
clear edges
for i = 1 : 10
bins(i) = result(result < (i/10)*diag);
end
result < (i/10)*diag
result(result < (i/10)*diag)
doc barh
barh(result)

%% Extracted - centres comparison
day = '03';
%analysis = 'Colour and Edges';
analysis = 'Edges';
advanced = load(['/Users/spiderpete/Documents/OneDrive - University of Edinburgh/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - ', analysis, '/Day', day, '.mat']);
basic = load(['/Users/spiderpete/Documents/OneDrive - University of Edinburgh/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Positions/Day', day, '.mat']);

for i = 1 : size(basic.day_01{2},1)
    dist(i) = pdist2(basic.day_01{2}{i}(1,1:2), advanced.day_01{2}{i}(1,1:2), 'euclidean');
end

for i = 1 : size(basic.day_01{2},1)
    result(i) = abs(dist(i));
end

diag = 1470;
for i = 1 : 10
counts(i) = size(result(result < i*diag/10), 2);
end

barh(counts);

%% Extracted - centres contained by bounding boxes
day = '03';
%analysis = 'Colour and Edges';
analysis = 'Edges';
notes = load(['/Users/spiderpete/Documents/OneDrive - University of Edinburgh/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - ', analysis, '/Day', day, '_notes.mat']);
basic = load(['/Users/spiderpete/Documents/OneDrive - University of Edinburgh/Courses/Dissertation/Data/OFFICE/Ground Truths/GT/GT - Positions/Day', day, '.mat']);

num = size(basic.day_01{2},1);
contained = false(1, num);
for i = 1 : num
    if containedPoint(basic.day_01{2}{i}(1,1:2), notes.notes{i}(1,:))
        contained(i) = true;
    end
end

total = sum(contained == true)/num * 100;