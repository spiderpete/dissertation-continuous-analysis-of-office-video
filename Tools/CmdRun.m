% Colour and Edge Detection
addpath('./system/') 
addpath('./system/labelling tools/')
addpath('./system/labelling tools/intensity and edge detection/')
addpath('./system/labelling tools/positions labelling/') 
addpath('./system/Libraries/')                          
addpath('./system/Libraries/hysteresis3d/')

% Edge Detection
addpath('./system/') 
addpath('./system/labelling tools/')
addpath('./system/labelling tools/edge detection/')
addpath('./system/labelling tools/positions labelling/') 
