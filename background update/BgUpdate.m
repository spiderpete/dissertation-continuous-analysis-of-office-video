% use the mask to update background
% add tracking info to this program later to advance it
function [update_bg] = BgUpdate(img,Bg,masklist)
%     doorThresh = 0.06;
    imgThresh = 0.02;
    [m,n,~] = size(img);
    Bw_BG = zeros(m,n);
    Bw_BG = im2bw(Bw_BG);
    if length(masklist)>1
        for i=1:3
            temp_img = img(:,:,i);
            temp_bg = Bg(:,:,i); 
%             tempdiff = im2bw();
%             Bw_BG(masklist) = Bw_BG(masklist)|(abs(temp_img(masklist)-temp_bg(masklist))>imgThresh);
%             Bw_BG = im2bw(Bw_BG);
            
            
            
            temp_img(masklist) = temp_bg(masklist); %update background without the bounding box
            update_bg(:,:,i) = temp_img;
        end
        [I,J] = ind2sub([m,n],masklist);
%         Bw_BG = im2bw(Bw_BG);
        Bw_Bg_revi = ~Bw_BG;
        Bw_BG = im2double(Bw_BG);
        Bw_Bg_revi = im2double(Bw_Bg_revi);
        for i=1:3
%             update_bg(I,J,i) = (Bw_Bg_revi(I,J).*img(I,J,i))+( Bw_BG(I,J).*Bg(I,J,i)); %update background within the bounding box
            
%             update_bg(I,J,i) = ~Bw_BG(I,J).*img(I,J,i)*0.9+~Bw_BG(I,J).*Bg(I,J,i)*0.1+ Bw_BG(I,J).*Bg(I,J,i); %update background within the bounding box

        end
    else
        update_bg = img;
    end
%     getlist = find(update_bg==1);
%     [m,n] = size(update_bg);
%     s = [m,n];
%     [I,J] = ind2sub(s,getlist);
%     for i=1:length(I)
%         if (abs(normal_IAfter(I(i),J(i),1)-0.43)<doorThresh)&&(abs(normal_IAfter(I(i),J(i),2)-0.32)<doorThresh)&&(abs(normal_IAfter(I(i),J(i),3)-0.22)<doorThresh)
%             update_bg(I(i),J(i))=0;
%         end
%     end
end
% img
% Bg