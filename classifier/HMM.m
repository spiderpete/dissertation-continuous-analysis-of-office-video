%{
%% compute prior
total_data = length(mod_catbehavior);
for i = 1:12
    behavior_class(i) = sum(mod_catbehavior==i);
    prior(i) = behavior_class(i)/total_data;
end

%% compute the transition
count = 0;
trans = zeros(12,12);
for i=1:12
    total_count = sum(mod_catbehavior==i);
    for j =1:12
        for k = 2:length(mod_catbehavior)
            if (mod_catbehavior(k)==i)&&(mod_catbehavior(k-1)==j)
                count = count+1;
            end
        end
        trans(i,j) = count/total_count;
        count = 0;
    end
end
%%
trans(7,:) = 0;
trans(12,:) = 0;
%% train the hmm
%Let us generate nex=50 vector-valued sequences of length T=50; each vector has size O=2.
% O = 2;
% T = 50;
% nex = 50;
% data = randn(O,T,nex);
% re_data = reshape(data, [O T*nex]);

O = 1;
data = featuresss(:,1)';
%Now let use fit a mixture of M=2 Gaussians for each of the Q=2 states using K-means.
M = 1;
Q = 12;
% left_right = 0;

% prior0 = normalise(rand(Q,1));
prior0 = prior;
% transmat0 = mk_stochastic(rand(Q,Q));
transmat0 = trans;
% prior0(7) = [];
% prior0(11) = [];
% transmat0(7,:) = [];
% transmat0(11,:) = [];
%}


Comment our for now: Peter Stefanov
[mu0, Sigma0] = mixgauss_init(Q*M, data,'diag');
mu0 = reshape(mu0, [O Q M]);
Sigma0 = reshape(Sigma0, [O O Q M]);
mixmat0 = mk_stochastic(rand(Q,M));
mixmat0 = ones(Q,M);
% mixmat0 = zeros(Q,M);
% Finally, let us improve these parameter estimates using EM.
[LL, prior1, transmat1, mu1, Sigma1, mixmat1] = mhmm_em(data, prior0, transmat0, mu0, Sigma0, mixmat0, 'max_iter', 10);
% for i=1:10
% [LL, prior0, transmat0, mu0, Sigma0, mixmat0] = mhmm_em(data, prior0, transmat0, mu0, Sigma0, mixmat0, 'max_iter', 10);
% end
%% get likelihood with test data (the larger the better)
loglik = mhmm_logprob(data, prior1, transmat1, mu1, Sigma1, mixmat1);

%% evaluate the accuracy


%% assign speed into different bins
Bins = 735;
for k=1:length(cell_feature)
    realspeed_array = cell_feature{k}(:,3);
    discreat_speed = size(realspeed_array);
    for i =1:length(realspeed_array)
        for j=1:Bins-1
            if realspeed_array(i)<2*j
                discreat_speed(i) = j;
                break;
            elseif realspeed_array(i)>(Bins-1)*2
                discreat_speed(i) = 300;
                break;
            end
        end
    end
    cell_feature{k}(:,3) = discreat_speed;
end

Bins = 640;
for k=1:length(cell_feature)
    realspeed_array = cell_feature{k}(:,4);
    discreat_speed = size(realspeed_array);
    for i =1:length(realspeed_array)
        for j=1:Bins-1
            if realspeed_array(i)<2*j
                discreat_speed(i) = j;
                break;
            elseif realspeed_array(i)>(Bins-1)*2
                discreat_speed(i) = 256;
                break;
            end
        end
    end
    cell_feature{k}(:,4) = discreat_speed;
end

Bins = 360;
for k=1:length(cell_feature)
    realspeed_array = cell_feature{k}(:,5);
    discreat_speed = size(realspeed_array);
    for i =1:length(realspeed_array)
        for j=1:Bins-1
            if realspeed_array(i)<2*j
                discreat_speed(i) = j;
                break;
            elseif realspeed_array(i)>(Bins-1)*2
                discreat_speed(i) = 144;
                break;
            end
        end
    end
    cell_feature{k}(:,5) = discreat_speed;
end
%% modify behavior
for i=1:length(cell_behavior)
%     behv_cell = cell_behavior{i};
    mod_cell = cell_feature{i}(:,6);
    temp_fea = cell_feature{i}(:,1);
    modlist1 = find(mod_cell==1);
    modlist = find(temp_fea>0);
    mod_cell(modlist) = 1;
    mod_cell(modlist1) = 2;
    cell_feature{i}(:,6) = mod_cell;
end
%}
%% acquire cross_validate data
cell_feature = {feature_1,feature_2,feature_3,feature_4,feature_5,feature_6,feature_7,feature_8,feature_9,feature_10};
cell_behavior = {mod1_behavior1,mod1_behavior2,mod1_behavior3,mod1_behavior4,mod1_behavior5,mod1_behavior6,mod1_behavior7,mod1_behavior8,mod1_behavior9,mod1_behavior10};
%% matlab built in hmm cross-validate
datalist = [1 2 3 4 5 6 7 8 9 10];
for i =1:length(cell_feature)
    cross_list = datalist;
    cross_list(i) = [];
    cross_feature = cell_feature(cross_list);
    cross_behav = cell_behavior(cross_list);
    catfea = cross_feature{1};
    catbehav = cross_behav{1};
    for j=2:length(cross_feature)
        temp_fea = cross_feature{j};
        temp_behav = cross_behav{j};
        catfea = [catfea;temp_fea];         % Column vector
        catbehav = [catbehav,temp_behav];   % Row vector
    end

    test_fea = cell_feature{i};
    test_behav = cell_behavior{i};

    aa =round(catfea(:,1)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,1)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    % TRANS1 = TRANS;
    EMIS1 =EMIS;
    PSTATES1 = PSTATES;


    % clearvars aa TRANS EMIS
    aa =round(catfea(:,2)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,2)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    EMIS2 =EMIS;
    PSTATES2 = PSTATES;

    aa =round(catfea(:,3)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,3)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    EMIS3 =EMIS;
    PSTATES3 = PSTATES;

    aa =round(catfea(:,4)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,4)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    EMIS4 =EMIS;
    PSTATES4 = PSTATES;

    aa =round(catfea(:,5)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,5)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    EMIS5 =EMIS;
    PSTATES5 = PSTATES;

    aa =round(catfea(:,6)')+1;
    state = catbehav;
    [TRANS,EMIS] = hmmestimate(aa,state);
    aa = round(test_fea(:,6)')+1;
    try
        PSTATES = hmmdecode(aa,TRANS,EMIS);
        [m, ~] = size(EMIS);
    catch
        PSTATES = zeros(m,length(aa));
    end
    EMIS6 =EMIS;
    PSTATES6 = PSTATES;

    clearvars temp_PSTATES
    temp_PSTATES(:,:,1)= PSTATES1;
    temp_PSTATES(:,:,2)= PSTATES2;
    temp_PSTATES(:,:,3)= PSTATES3;
    temp_PSTATES(:,:,4)= PSTATES4;
    temp_PSTATES(:,:,5)= PSTATES5;
    temp_PSTATES(:,:,6)= PSTATES6;

    [m, n, k] = size(temp_PSTATES);
    for ii=1:m
        for jj=1:n
            PSTATES(ii,jj) = max(temp_PSTATES(ii,jj,:));
        end
    end


    % c = find(featuresss(:,6)==1);
    [Y,kk{i}] = max(PSTATES);
    % likelystates = hmmviterbi(aa, TRANS, EMIS1);
    % likelystates(find(featuresss(:,6)==1)) = 10;
    % I(find(test_fea(:,6)==1))=10;
    accuracy(i) = sum(test_behav==kk{i})/length(test_behav);
end
% for l=1:12
%     pred_hav = length(find(kk(i)==l))/length(find(test_behav(i)==l))
% end
accuracy
mean(accuracy)
%% example from matlab built in code
% TRANS = [.9 .1; .05 .95;];
% 
% EMIS = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6;...
% 7/12, 1/12, 1/12, 1/12, 1/12, 1/12];
% % To generate a random sequence of states and emissions from the model, use hmmgenerate:
% 
% [seq,states] = hmmgenerate(1000,TRANS,EMIS);
% [TRANS,EMIS] = hmmestimate(seq,states);


%{
%% plot prediciton and groundtruth
x=10000:15000;
plot(x,kk(10000:15000),'r')
axis([10000 15500 -1 15]);
set(gca,'ytick',-1:15);
hold on
plot(x,test_behav(10000:15000),'b')
hold off
% axes.set_yticks([-1,0,1,2,3,4,5,6,7,8,9,10]);
%}
%% 
for i=1:10
    if i<=2
        figure(1);
        subplot(2,1,i);
    elseif i>2 & i<=4
        figure(2),
        subplot(2,1,i-2)
    elseif i>4& i<=6
        figure(3),
        subplot(2,1,i-4);
    elseif i>6&i<=8
        figure(4),
        subplot(2,1,i-6);
    elseif i>8
        figure(5),
        subplot(2,1,i-8);
    end

    x=1:length(kk{i});
    plot(x,kk{i},'r')
    data_name = ['Day',num2str(i)];
    title(data_name);

    axis([1 length(kk{i}) -1 15]);
    set(gca,'ytick',-1:15);
    hold on
    plot(x,cell_behavior{i},'b')
    hold off
    legend('prediction','ground truth');
    grid on;
end

%% confusion matrix
for i=1:10
    tempM = zeros(12,12);
    for j=1:12
        for k=1:12
            tempM(j,k) = sum((cell_behavior{i}==j)& (kk{i}==k));
        end
    end
    conM{i} = tempM;
    clearvars tempM
end

%% tolerence
for numD=1:10
    tol_deco = zeros(1,length(cell_behavior{numD}));
    for tol=0:8
        for i=1:length(cell_behavior{numD})
            for j=0:tol
                if(kk{numD}(i)-j)==cell_behavior{numD}(i)||(kk{numD}(i)+j)==cell_behavior{numD}(i)
                    tol_deco(i) = 1;
                end
            end
        end
        accuracy_tol(numD,tol+1) = sum(tol_deco)/length(tol_deco);
    end
end


%% 
hold on
for i =1:10
    X=linspace(1,9,200);
%     Y=spline(1:9,accuracy_tol(i,:),X);
    Y = accuracy_tol(i,:);
    plot(0:8,Y)
    axis([0 9 0.65 1.05]);
    set(gca,'xtick',1:10);
end
hold off
legend('Day 1','Day 2','Day 3','Day 4','Day 5','Day 6','Day 7','Day 8','Day 9','Day 10');
grid on

% hold on
% for i =1:10
% a = 1:9;
% b = accuracy_tol;
% values = spcrv([[a(1) a a(end)];[b(i,1) b(i,:) b(i,end)]],10);
% plot(values(1,:),values(2,:), 'g');
% end
% hold off
%% plot all position on background
% figure(1);
% for numD=1:10
%     pos_x = cell_feature{numD}(:,1);
%     pos_y = cell_feature{numD}(:,2);
%     figure(1);
%     imshow(imread('inspacecam163_2016_02_22_10_00_00.jpg'));
%     hold on
%     for i=1:length(pos_x)
%     plot(pos_x(i),pos_x(i),'r')
%     end
%     hold off
% end

%% find list of error and correct
error_list = find(kk{9}==mod1_behavior9);
error_1k = error_list(3000:3999);
