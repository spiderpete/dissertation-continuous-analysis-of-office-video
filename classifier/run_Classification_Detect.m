% Custamisable parameters
EXECUTION_ON_WINDOWS = false;
BAHAVIOUR_DATA = 'mod_behavior(Detect)';
FEATURE_DATA = 'mod_feature(Detect)';
NUM_DAYS = 10;
NUM_OF_PLOTS_PER_FIGURE = 2;
TOTAL_NUM_BEHAVIOURS = 12;
FONT_SIZE_TITLE = 13;
FONT_SIZE = 13;

% Stationary parameters
if ~EXECUTION_ON_WINDOWS
    DIR = './system/classifier/data/Peter Detect/';
    SAVE_DIR = DIR;
else
    DIR = '.\system\classifier\data\Peter Detect\';
    SAVE_DIR = DIR;
end

% Load in data for classification
load([DIR, BAHAVIOUR_DATA, '.mat']); % 'feature_X' structures loaded
load([DIR, FEATURE_DATA, '.mat']);  % 'mod1_beahavior_X' structures loaded

%% acquire cross_validate data
collected_feature = {};
collected_behaviour = {};
for day = 1 : NUM_DAYS
    collected_feature = [collected_feature, {eval(['feature_', num2str(day)])}];
    collected_behaviour = [collected_behaviour, {eval(['mod1_behavior', num2str(day)])}];
end

%% matlab built in hmm cross-validate
datalist = 1 : NUM_DAYS;
for day = 1 : NUM_DAYS
    crossValid_list = datalist;
    crossValid_list(day) = [];
    crossValid_feature = collected_feature(crossValid_list);
    crossValid_behaviour = collected_behaviour(crossValid_list);
    
    % Train set
    hmmFeatures = [];
    hmmBehaviours = [];
    for behaviour = 1 : length(crossValid_feature)                          % stack days
        hmmFeatures = [hmmFeatures; crossValid_feature{behaviour}];         % Column vector
        hmmBehaviours = [hmmBehaviours, crossValid_behaviour{behaviour}];   % Row vector
    end
    
    % Test set
    test_features = collected_feature{day};
    test_behaviours = collected_behaviour{day};

    % Trainging and Testing on all features separately
    PSTATES = [];
    featuresNum = size(hmmFeatures, 2);
    for feature = 1 : featuresNum
        hmmFeaturesPreped = round(hmmFeatures(:, feature)') + 1;
        states = hmmBehaviours;
        % Estimate tranition and emision probabilities
        [TRANS,EMIS] = hmmestimate(hmmFeaturesPreped, states);
        hmmFeaturesPreped = round(test_features(:, feature)') + 1;
        % Estimate states posterior probabilities
        try
            PSTATES(:,:, feature) = hmmdecode(hmmFeaturesPreped, TRANS, EMIS);
            [statesNum, ~] = size(EMIS);
        catch
            PSTATES(:,:, feature) = zeros(statesNum,length(hmmFeaturesPreped));
        end
    end
    
    % Posterior probability based on all features, max for each 
    % (state, feature) pair
    posteriorStateProb = max(PSTATES, [], 3);
    
    [~, inferedStates{day}] = max(posteriorStateProb);
    accuracy(day) = sum(test_behaviours == inferedStates{day}) / length(test_behaviours);
end

%% Display Accuracy of Behvioural Model
disp(['Accuracy per days: ', num2str(accuracy)]);
disp(['Average accuracy across all days: ', num2str(mean(accuracy))]);

%% Plots
% Display states for every frame per day
for day = 0 : (NUM_DAYS - 1)
    % Set up figures and subfigures
    %if rem(day, 2) == 0
    %    figure(day / 2 + 1);
    %end
    figure('units','normalized','position',[.1 .1 .5 .3]);
    figure(day + 1);
    %subplot(NUM_OF_PLOTS_PER_FIGURE, 1, rem(day, 2) + 1);

    % Plotting Predictions and Ground Truths
    x = 1 : length(inferedStates{day + 1});
    plot(x, inferedStates{day + 1}, 'r');
    data_name = ['Day ', num2str(day + 3)];
    title(data_name, 'fontSize', FONT_SIZE_TITLE);
    xlabel('Frame Number', 'fontSize', FONT_SIZE);
    ylabel('Behaviour Number', 'fontSize', FONT_SIZE);
    axis([1 length(inferedStates{day + 1}) -1 15]);
    set(gca, 'ytick', -1 : 15);
    hold on;
    plot(x, collected_behaviour{day + 1}, 'b');
    hold off;
    legend('Prediction','Ground Truth');
    grid on;
    
    %if rem(day, 2) == 1
    %    name = [SAVE_DIR, 'Classification Features part(', num2str(day / 2 + 1), ').tif'];
    %    saveas(gcf, name);
    %end
    name = [SAVE_DIR, 'Classification Features part(', num2str(day + 3), ').tif'];
    saveas(gcf, name);
end

%% Confusion Matrix
daylyConfusionMatrix = zeros(TOTAL_NUM_BEHAVIOURS, TOTAL_NUM_BEHAVIOURS, NUM_DAYS);
for day = 1 : NUM_DAYS
    confusionMatrix = zeros(TOTAL_NUM_BEHAVIOURS);
    for behaviour = 1 : TOTAL_NUM_BEHAVIOURS
        for behaviourCompar = 1 : TOTAL_NUM_BEHAVIOURS
            confusionMatrix(behaviour, behaviourCompar) = sum((collected_behaviour{day} == behaviour) & (inferedStates{day} == behaviourCompar));
        end
    end
    daylyConfusionMatrix(:,:, day) = confusionMatrix;
end
% Confusion Matrix for all data
totalConfusionMatrix = sum(daylyConfusionMatrix, 3);
fractionsConfustionMatrix = bsxfun(@rdivide, totalConfusionMatrix, sum(totalConfusionMatrix)) .* 100;
fractionsConfustionMatrix(isnan(fractionsConfustionMatrix)) = 0;
disp('Confusion Matrix for all videos:');
disp(round((fractionsConfustionMatrix'*100))/100); % Rounded to 2 decimal places

%% Tolerence Plot
MAX_FRAME_TOLERANCE = 8;
for numD = 1 : NUM_DAYS
    tol_deco = zeros(1, length(collected_behaviour{numD}));
    for tolMargin = 0 : MAX_FRAME_TOLERANCE
        for day = 1 : length(collected_behaviour{numD})
            for behaviour = 0 : tolMargin
                if(inferedStates{numD}(day)-behaviour)==collected_behaviour{numD}(day) || (inferedStates{numD}(day) + behaviour) == collected_behaviour{numD}(day)
                    tol_deco(day) = 1;
                end
            end
        end
        accuracy_tol(numD, tolMargin + 1) = sum(tol_deco)/length(tol_deco);
    end
end
nextFigure = NUM_DAYS + 1;
figure('units','normalized','position',[.1 .1 .5 .4]);
figure(nextFigure);
hold on;
for day = 1 : NUM_DAYS
    Y = accuracy_tol(day,:);
    plot(0 : MAX_FRAME_TOLERANCE, Y);
    axis([0 9 0.65 1.05]);
    set(gca, 'xtick', 1 : 10);
end
hold off;
legend('Day 3','Day 4','Day 5','Day 6','Day 7','Day 8','Day 9','Day 10','Day 11','Day 12');
grid on;
title('Behaviour Classification Accuracy with Tolerance', 'fontSize', FONT_SIZE_TITLE);
xlabel('Frame Tolerance', 'fontSize', FONT_SIZE);
ylabel('Accuracy', 'fontSize', FONT_SIZE);

name = [SAVE_DIR, 'Classification Accuracy.tif'];
saveas(gcf, name);
clear;

% a = './system/classifier/data/Peter Detect/mod_feature(Detect).mat';
% save(a, 'feature_1','feature_2','feature_3','feature_4','feature_5','feature_6','feature_7','feature_8','feature_9','feature_10');
