type = 'GT'; % Prev, GT or Detect

EXECUTION_ON_WINDOWS = false;
PLOT_DATA = 'categorisedFeatures';
NUM_DAYS = 10;
NUM_CLASSES = 11;
NUM_FEATURES = 6;
FONT_SIZE_TITLE = 13;
FONT_SIZE = 13;

% Stationary parameters
if ~EXECUTION_ON_WINDOWS
    DIR = ['./system/classifier/data/Peter ', type, '/Feature Plots/'];
    SAVE_DIR = DIR;
else
    DIR = ['.\system\classifier\data\Peter ', type, '\Feature Plots\'];
    SAVE_DIR = DIR;
end

% Load in data for classification
load([DIR, PLOT_DATA, '.mat']); % 'categorisedFeatures' structure loaded

%categorisedFeatures = categorisedFeatures{1};

classesTitle = {'No one', 'Enterring the room', 'Leaving the room', 'Walking or Standing', 'Using terminals', 'Sitting', 'Whiteboard', 'Near window', 'Near bookshelf', 'Talking to someone (or Having a meeting)', 'Strange behaviour'};
featuresXAxis = {'X-axis', 'Y-axis', 'Speed', 'X-speed', 'Y-speed', 'Number of people'};
xlables = {'Pixels', 'Pixels/second', 'Number of people'};
x1 = 1:20:1280;
x2 = 1:20:720;
x3 = 0:20:sqrt((1280 - 1) * (1280 - 1) + (720 - 1) * (720 - 1));
x4 = 0:20:1280;
x5 = 0:20:720;
x6 = 0:1:3;
xCombined = {x1, x2, x3, x4, x5, x6};


for feature = 1 : NUM_FEATURES
    featureXAxis = featuresXAxis{feature};
    x = xCombined{feature};
    
    for class = 1 : NUM_CLASSES
        classTitle = classesTitle{class};
        
        histogram(categorisedFeatures{feature,class},x)
        grid on;
        title({['Distribution of Classification Feature "', featureXAxis, '"'];['for class "', classTitle, '"']}, 'fontSize', FONT_SIZE_TITLE);
        if feature == 1 || feature == 2
            xlabel(xlables{1}, 'fontSize', FONT_SIZE);
        elseif feature == 3 || feature == 4 || feature == 5
            xlabel(xlables{2}, 'fontSize', FONT_SIZE);
        elseif feature == 6
            xlabel(xlables{3}, 'fontSize', FONT_SIZE);
        else
            disp('Something went wrong!');
        end
        ylabel('Counts', 'fontSize', FONT_SIZE);
        
        % Save plot
        saveas(gcf, [SAVE_DIR, 'Feature ', num2str(feature), ' and class ', num2str(class), '.png']);
    end
end
disp('Process complete!');
clear;
