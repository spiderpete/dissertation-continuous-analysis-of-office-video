type = 'Detect'; % Prev, GT or Detect

EXECUTION_ON_WINDOWS = false;
BAHAVIOUR_DATA = ['mod_behavior(', type, ')'];
FEATURE_DATA = ['mod_feature(', type, ')'];
NUM_DAYS = 10;
NUM_CLASSES = 11;

% Stationary parameters
if ~EXECUTION_ON_WINDOWS
    DIR = ['./system/classifier/data/Peter ', type, '/'];
    SAVE_DIR = [DIR, 'Feature Plots/categorisedFeatures.mat'];
else
    DIR = ['.\system\classifier\data\Peter ', type, '\'];
    SAVE_DIR = [DIR, 'Feature Plots\categorisedFeatures.mat'];
end

% Load in data for classification
load([DIR, BAHAVIOUR_DATA, '.mat']); % 'feature_X' structures loaded
load([DIR, FEATURE_DATA, '.mat']);  % 'mod1_beahavior_X' structures loaded

%% acquire cross_validate data
collected_feature = {};
collected_behaviour = {};
for day = 1 : NUM_DAYS
    collected_feature = [collected_feature, {eval(['feature_', num2str(day)])}];
    collected_behaviour = [collected_behaviour, {eval(['mod1_behavior', num2str(day)])}];
end

hmmFeatures = [];
hmmBehaviours = [];
for behaviour = 1 : length(collected_behaviour)                          % stack days
    hmmFeatures = [hmmFeatures; collected_feature{behaviour}];         % Column vector
    hmmBehaviours = [hmmBehaviours, collected_behaviour{behaviour}];   % Row vector
end
hmmFeatures = round(hmmFeatures'); 

% Extract features
framesNum = size(hmmFeatures, 2);
featureNum = size(hmmFeatures, 1);
categorisedFeatures = cell(featureNum, NUM_CLASSES); %zeros(featureNum, NUM_CLASSES);

for feature = 1 : featureNum
    for class = 1 : NUM_CLASSES
        categorisedFeatures{feature, class} = [];
    end
end

for feature  = 1 : featureNum
    for frame = 1 : framesNum
        featureValue = hmmFeatures(feature, frame);
        class = hmmBehaviours(frame);
        
        %categorisedFeatures(feature, class) = [categorisedFeatures(feature, class) featureValue];
        %categorisedFeatures(feature, class) = cat(3, categorisedFeatures(feature, class), featureValue);
        if (feature == 1 || feature == 2)  && featureValue == 0
            continue;
        else
            categorisedFeatures{feature, class} = [categorisedFeatures{feature, class}, featureValue];
        end
    end
end
        
feature1class1 = categorisedFeatures{1, 1};
feature1class2 = categorisedFeatures{1, 2};
feature1class3 = categorisedFeatures{1, 3};
feature1class4 = categorisedFeatures{1, 4};
feature1class5 = categorisedFeatures{1, 5};
feature1class6 = categorisedFeatures{1, 6};
feature1class7 = categorisedFeatures{1, 7};
feature1class8 = categorisedFeatures{1, 8};
feature1class9 = categorisedFeatures{1, 9};
feature1class10 = categorisedFeatures{1, 10};
feature1class11 = categorisedFeatures{1, 11};
feature2class1 = categorisedFeatures{2, 1};
feature2class2 = categorisedFeatures{2, 2};
feature2class3 = categorisedFeatures{2, 3};
feature2class4 = categorisedFeatures{2, 4};
feature2class5 = categorisedFeatures{2, 5};
feature2class6 = categorisedFeatures{2, 6};
feature2class7 = categorisedFeatures{2, 7};
feature2class8 = categorisedFeatures{2, 8};
feature2class9 = categorisedFeatures{2, 9};
feature2class10 = categorisedFeatures{2, 10};
feature2class11 = categorisedFeatures{2, 11};
feature3class1 = categorisedFeatures{3, 1};
feature3class2 = categorisedFeatures{3, 2};
feature3class3 = categorisedFeatures{3, 3};
feature3class4 = categorisedFeatures{3, 4};
feature3class5 = categorisedFeatures{3, 5};
feature3class6 = categorisedFeatures{3, 6};
feature3class7 = categorisedFeatures{3, 7};
feature3class8 = categorisedFeatures{3, 8};
feature3class9 = categorisedFeatures{3, 9};
feature3class10 = categorisedFeatures{3, 10};
feature3class11 = categorisedFeatures{3, 11};
feature4class1 = categorisedFeatures{4, 1};
feature4class2 = categorisedFeatures{4, 2};
feature4class3 = categorisedFeatures{4, 3};
feature4class4 = categorisedFeatures{4, 4};
feature4class5 = categorisedFeatures{4, 5};
feature4class6 = categorisedFeatures{4, 6};
feature4class7 = categorisedFeatures{4, 7};
feature4class8 = categorisedFeatures{4, 8};
feature4class9 = categorisedFeatures{4, 9};
feature4class10 = categorisedFeatures{4, 10};
feature4class11 = categorisedFeatures{4, 11};
feature5class1 = categorisedFeatures{5, 1};
feature5class2 = categorisedFeatures{5, 2};
feature5class3 = categorisedFeatures{5, 3};
feature5class4 = categorisedFeatures{5, 4};
feature5class5 = categorisedFeatures{5, 5};
feature5class6 = categorisedFeatures{5, 6};
feature5class7 = categorisedFeatures{5, 7};
feature5class8 = categorisedFeatures{5, 8};
feature5class9 = categorisedFeatures{5, 9};
feature5class10 = categorisedFeatures{5, 10};
feature5class11 = categorisedFeatures{5, 11};
feature6class1 = categorisedFeatures{6, 1};
feature6class2 = categorisedFeatures{6, 2};
feature6class3 = categorisedFeatures{6, 3};
feature6class4 = categorisedFeatures{6, 4};
feature6class5 = categorisedFeatures{6, 5};
feature6class6 = categorisedFeatures{6, 6};
feature6class7 = categorisedFeatures{6, 7};
feature6class8 = categorisedFeatures{6, 8};
feature6class9 = categorisedFeatures{6, 9};
feature6class10 = categorisedFeatures{6, 10};
feature6class11 = categorisedFeatures{6, 11};


save(SAVE_DIR, 'feature1class1','feature1class2','feature1class3','feature1class4','feature1class5','feature1class6','feature1class7','feature1class8','feature1class9','feature1class10','feature1class11', ...
                'feature2class1','feature2class2','feature2class3','feature2class4','feature2class5','feature2class6','feature2class7','feature2class8','feature2class9','feature2class10','feature2class11', ...
                'feature3class1','feature3class2','feature3class3','feature3class4','feature3class5','feature3class6','feature3class7','feature3class8','feature3class9','feature3class10','feature3class11', ...
                'feature4class1','feature4class2','feature4class3','feature4class4','feature4class5','feature4class6','feature4class7','feature4class8','feature4class9','feature4class10','feature4class11', ...
                'feature5class1','feature5class2','feature5class3','feature5class4','feature5class5','feature5class6','feature5class7','feature5class8','feature5class9','feature5class10','feature5class11', ...
                'feature6class1','feature6class2','feature6class3','feature6class4','feature6class5','feature6class6','feature6class7','feature6class8','feature6class9','feature6class10','feature6class11', ...
                'categorisedFeatures');

disp('Process complete!');
clear;
