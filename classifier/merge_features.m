%% store feature into one array
feature_1 = [mod_pos1(:,:,1)' mod_pos1(:,:,2)' mod_speed1(:,:,1)' mod_speed1(:,:,2)' mod_speed1(:,:,3)' ppl_inf1'];
feature_2 = [mod_pos2(:,:,1)' mod_pos2(:,:,2)' mod_speed2(:,:,1)' mod_speed2(:,:,2)' mod_speed2(:,:,3)' ppl_inf2'];
feature_3 = [mod_pos3(:,:,1)' mod_pos3(:,:,2)' mod_speed3(:,:,1)' mod_speed3(:,:,2)' mod_speed3(:,:,3)' ppl_inf3'];
feature_4 = [mod_pos4(:,:,1)' mod_pos4(:,:,2)' mod_speed4(:,:,1)' mod_speed4(:,:,2)' mod_speed4(:,:,3)' ppl_inf4'];
feature_5 = [mod_pos5(:,:,1)' mod_pos5(:,:,2)' mod_speed5(:,:,1)' mod_speed5(:,:,2)' mod_speed5(:,:,3)' ppl_inf5'];
feature_6 = [mod_pos6(:,:,1)' mod_pos6(:,:,2)' mod_speed6(:,:,1)' mod_speed6(:,:,2)' mod_speed6(:,:,3)' ppl_inf6'];
feature_7 = [mod_pos7(:,:,1)' mod_pos7(:,:,2)' mod_speed7(:,:,1)' mod_speed7(:,:,2)' mod_speed7(:,:,3)' ppl_inf7'];
feature_8 = [mod_pos8(:,:,1)' mod_pos8(:,:,2)' mod_speed8(:,:,1)' mod_speed8(:,:,2)' mod_speed8(:,:,3)' ppl_inf8'];
feature_9 = [mod_pos9(:,:,1)' mod_pos9(:,:,2)' mod_speed9(:,:,1)' mod_speed9(:,:,2)' mod_speed9(:,:,3)' ppl_inf9'];
feature_10 = [mod_pos10(:,:,1)' mod_pos10(:,:,2)' mod_speed10(:,:,1)' mod_speed10(:,:,2)' mod_speed10(:,:,3)' ppl_inf10'];
%%
strcmp(feature_1(:,1),positions1(:,:,1)');
a = feature_1(:,1);
b= positions1(:,:,1)';
tf = strcmp(a,b)

%% get people information (indicated whether there is more than one people)
ppl_inf10 = (mod1_behavior10==10);



%% merge ground truth
ground_truth1 = {[ilist1 mod_pos1(:,:,1)' mod_pos1(:,:,2)' mod_speed1(:,:,1)' mod_speed1(:,:,2)' mod_speed1(:,:,3)' ppl_inf1']};
ground_truth2 = {[ilist2 mod_pos2(:,:,1)' mod_pos2(:,:,2)' mod_speed2(:,:,1)' mod_speed2(:,:,2)' mod_speed2(:,:,3)' ppl_inf2']};
% ground_truth3 = {[ilist3 mod_pos3(:,:,1)' mod_pos3(:,:,2)' mod_speed3(:,:,1)' mod_speed3(:,:,2)' mod_speed3(:,:,3)' ppl_inf3']};
ground_truth4 = {[ilist4 mod_pos4(:,:,1)' mod_pos4(:,:,2)' mod_speed4(:,:,1)' mod_speed4(:,:,2)' mod_speed4(:,:,3)' ppl_inf4']};
ground_truth5 = {[ilist5 mod_pos5(:,:,1)' mod_pos5(:,:,2)' mod_speed5(:,:,1)' mod_speed5(:,:,2)' mod_speed5(:,:,3)' ppl_inf5']};
ground_truth6 = {[ilist6 mod_pos6(:,:,1)' mod_pos6(:,:,2)' mod_speed6(:,:,1)' mod_speed6(:,:,2)' mod_speed6(:,:,3)' ppl_inf6']};
ground_truth7 = {[ilist7 mod_pos7(:,:,1)' mod_pos7(:,:,2)' mod_speed7(:,:,1)' mod_speed7(:,:,2)' mod_speed7(:,:,3)' ppl_inf7']};
ground_truth8 = {[ilist8 mod_pos8(:,:,1)' mod_pos8(:,:,2)' mod_speed8(:,:,1)' mod_speed8(:,:,2)' mod_speed8(:,:,3)' ppl_inf8']};
ground_truth9 = {[ilist9 mod_pos9(:,:,1)' mod_pos9(:,:,2)' mod_speed9(:,:,1)' mod_speed9(:,:,2)' mod_speed9(:,:,3)' ppl_inf9']};
ground_truth10 = {[ilist10 mod_pos10(:,:,1)' mod_pos10(:,:,2)' mod_speed10(:,:,1)' mod_speed10(:,:,2)' mod_speed10(:,:,3)' ppl_inf10']};