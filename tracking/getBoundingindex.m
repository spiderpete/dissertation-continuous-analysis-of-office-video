function [flag,bound_index] = getBoundingindex(boundingbox,m,n)
% boundingbox is a n * 4 array, the first tow element is the position of
% the top left coner of that box, while the third and forth is width
% and length
    list = [];
%     imsize = size(zeros(720,1280));
    
    [a b] = size(boundingbox);
    
    for i=1:a
       for  j =1:boundingbox(i,3) 
            startind=sub2ind([m n],round(boundingbox(i,2)),round(boundingbox(i,1)))+m*(j-1);
            temp = (startind:1:startind+boundingbox(i,4));
            list = cat(2,list,temp);
            % 这里直接用 sub2ind([720 1280],list)就可以得到相应的xy，下面第一个就是
       end
    end
    bound_index = unique(list);
    bound_index(find(bound_index>n*m))=[];
    
    if length(bound_index) >0
        flag =1;
    else
        flag =0
    end
end


%  boundingbox= improp.BoundingBox;
%  figure
%  imshow(img);
%  hold on
%  plot(round(boundingbox(i,1)),round(boundingbox(i,2)),'r-*')
%  hold off
 
% 注意这里得到的index和实际x y 相反
%  [a b ]=ind2sub([720 1280],sub2ind([720 1280],round(boundingbox(i,2)),round(boundingbox(i,1))))
%  imshow(img);
%  hold on
%  plot(b,a,'r-*')
%  hold off

% y = zeros(360,640);
% y(list) = 1;
% imshow(y)