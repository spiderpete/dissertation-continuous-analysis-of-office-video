function d=bhattacharyya(X1,X2) 
Sum1=sum(sum(X1));Sum2=sum(sum(X2));
Sumup = sqrt(X1.*X2);
SumDown = sqrt(Sum1*Sum2);
Sumup = sum(sum(Sumup));
d=1-sqrt(1-Sumup/SumDown);

% d = pdist2(X1,X2,'mahalanobis');
end