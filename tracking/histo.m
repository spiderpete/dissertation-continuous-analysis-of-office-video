function hist = histo(fore,image)
stats = regionprops('table',fore,'PixelIdxList','Centroid','MinorAxisLength','BoundingBox');
st = (image(:,:,1)+image(:,:,2)+image(:,:,3))/3;
rt = image(:,:,1)./st/3;
gt = image(:,:,2)./st/3;
rt(isnan(rt))= 0;
gt(isnan(gt))= 0;

temp_fg = zeros(size(fore));
for i=1:length(stats.PixelIdxList)
%     d = min(stats.BoundingBox(i,3),stats.BoundingBox(i,4));
temp_fg(round(stats.Centroid(i,2))-round(0.3*stats.MinorAxisLength(i)):round(stats.Centroid(i,2))+round(0.3*stats.MinorAxisLength(i))...
    ,round(stats.Centroid(i,1))-round(0.3*stats.MinorAxisLength(i)):round(stats.Centroid(i,1))+round(0.3*stats.MinorAxisLength(i)))=1;
% temp_fg(round(stats.Centroid(i,1))-0.5*round(d):round(stats.Centroid(i,1))+0.5*round(d)...
%     ,round(stats.Centroid(i,2))-0.5*round(d):round(stats.Centroid(i,2))+0.5*round(d))=1;
end
temp_fg = temp_fg(1:360,1:640);
temp_fg = im2bw(temp_fg);
stats = regionprops('table',temp_fg,'Area','PixelIdxList');


% kidd = zeros(360,640);
% kidd(stats.PixelIdxList{1}) = 1;
% imshow(kidd)
% imshow(temp_fg)

L = length(stats.PixelIdxList);
temp_hist = zeros(1024,2,L);%counts used to be 1024
% tic
for i = 1:L
    [m,n] = ind2sub(size(fore),stats.PixelIdxList{i});
    rp = rt(m,n);
    gp = gt(m,n);
    temp_hist(:,1,i) = imhist(rp,1024);
    temp_hist(:,2,i) = imhist(gp,1024);
    clearvars rp gp;    
end
% parfor i = 1:L
%     [m,n] = ind2sub(size(fore),stats.PixelIdxList{i});
%     rp = rt(m,n);
%     %     gp = gt(m,n);
%     temp_hist(:,1,i) = imhist(rp,1024);
%     %     temp_hist(:,2,i) = imhist(gp,1024);
% %     clearvars rp gp;
% %     clear rp;
%     
% end
% parfor j = 1:L
%     [m,n] = ind2sub(size(fore),stats.PixelIdxList{j});
%     %     rp = rt(m,n);
%     gp = gt(m,n);
%     %     temp_hist(:,1,i) = imhist(rp,1024);
%     temp_hist(:,2,j) = imhist(gp,1024);
% %     clear gp;
% end
% [a b] = size(hist)
hist = temp_hist;
% toc
end
