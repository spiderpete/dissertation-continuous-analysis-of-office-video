There are 6 folders in the package. 

1. background subtraction
Bgsub_re.m -------- algorithms that used in the project, open Detect and include this file to see the result

built_in_GMM.m ------  a matlab built in functions for background subtraction

Detect.m ------ codes that merge background subtraction, tracking and background updates, could be used to test on background subtraction

fcn_Normailize.m -----  code for nomalized RBG colour, must be included in order to run Bgsub_re.m or Detect.m

NormalizeGray.m ------ the MaxMin methods that use grayscale image for background subtraction

2.background updates
BgUpdata.m ------ code for update background, could be tested within Detect.m

3.Classifier

Hmm.m -------- Code for training and testing on Hidden Markov Model

hmm_accuracy_with_tol.mat ------ results of the hmm accuracy with tolerence

hmm_estimation_1000_data.mat ------- estimation of 1000 frames using HMM

HMM_result_0815.mat ------- estimation of 10 days data using hmm, trained by 10 folds cross-validation

merge_features.m ------- code for merge all features together to form the training data

4. ground truth data

include behaviour, positions, speed and feature arrays

5.labelling tools
tools for labelling positions and behaviours, also include code for testing and correction behaviours and positions

6. tracking
bhattacharyya.m ------ code for computing the bhattacharyya distance

getBoundingindex.m -------- code for get the index inside a bounding box

histo.m ------ code for computing the colour histograms

tracking.e ------ code used for tracking objects, could be tested in Detect.m