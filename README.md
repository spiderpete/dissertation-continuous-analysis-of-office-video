# Continuous analysis of office video #

**Supervisor:** Bob Fisher

**Subject Areas:** Computer Graphics/Machine Vision/Visualisation , Intelligent Robotics

**Suitable for the following degrees:** Master of Informatics

**Principal goal of the project:** Extend a living space video monitoring system

**Description of the project:** Some elderly people could stay longer in their own accommodation (i.e. rather than move into a care home) if there was better monitoring, particularly automatic and continuous monitoring. Acquiring home video data is not easy at the moment, but there is an easy alternative: monitoring an academic in his/her office.

The project will develop the analysis tools to classify all the observed behaviour of some academic recorded over several days, based on video data acquired from a fixed camera mounted in the corner of the room. Having a fixed camera will make detecting moving objects easier, but the person might also be largely stationary for long periods of time (e.g. typing on a computer, reading emails, sleeping, etc). One complicating factor is the lighting will be changing over time.

**The stages of the project are:**

* Detecting and tracking individuals, even if they may be largely stationary, e.g. using the technique of Majecka to detect the moving person, and then by inter­frame comparison to keep track of them.

* Recording where the person goes (desk, file cabinet, window, whiteboard, door, bookshelf) and perhaps build a state transition model (although it may be possible that any transition is equally likely).

* An algorithm to determine if the person is doing something unusual (eg. lying on the floor)

This will require producing ground­truth for each of the stages, so that the automatic program's results can be evaluated. This will require development of a simple labelling tool that allows users to mark the person's position and the current action to each observed person in each frame.

Ultimately, the videos and ground truth annotations will be made public to the benefit of the computer vision community. This dissertation will be appropriate for citation whenever the dataset is used in a publication.

**Resources Required:** video data, matlab

**Degree of Difficulty:** Challenging

Suitable to be undertaken by more than one student independently? No

**Completion Criteria:** 

Ground truth:

* understand the GT labeling tool

* extend the GT labeling tool if necessary for the new scenes

* extend the tool to record 0, 1 or 2+ people present in room

* possibly revise some existing labels for number of people in room from 1+ to 2+ ­ correct a small number of behaviour labels

**Activity/behaviour Recogniser:**

* train and evaluate the existing behaviour classifier on the current and new GT

* identify frames where there is a significant discrepancy between the GT and classifier performance when using the GT as data

* invent, implement and evaluate new features that might help reduce misclassifications

* evaluate the trained behaviour classifier on the results from Person Detection and Tracking

**Person Detection and Tracking:**

* develop a change detection algorithm that is illumination independent

* develop a background update algorithm that moves small changes into the background while keeping big changes as foreground

* implement the person tracking algorithm

* evaluate Person Detection and Tracking using the ground truth

* Essential Skills and Knowledge: matlab, completion of AV or RSS

**Desirable Skills and Knowledge:** good matlab programming skills

**Ethical issues involved in this project?** Yes

Plan for ethical issues: The video data will be of Bob Fisher, as well as other academic staff, who have given permission for it to be acquired and used.

**Does this project have any potential health and safety issues?** No

**Does this project have any additional costs?** No

**References:**

B. Majecka, "Statistical models of pedestrian behaviour in the Forum", MSc Dissertation, School of Informatics, University of Edinburgh, 2009.