% Time execution
tic

% Custamisable parameters
DAY_NUM = '01';
INIT_FRAME = 1;
SKIP_RATE = 30;
SPECIFIC_PERSON = 1; % If necessary to annotation one person at a time
                     % 0: All;
                     % 1: Bob; 
                     % 2: Second person to enter;
                     % 3: Third person to enter
POS_DISCREPENCY_LIMIT = 10;
JUMP_BACK_CORRECTION = 2 * SKIP_RATE;
EXECUTION_ON_WINDOWS = false;

% Stationary parameters
if ~EXECUTION_ON_WINDOWS
    DAY = ['Day', DAY_NUM];
    DIR = './Data/OFFICE/';
    READ_DIR = [DIR, 'Ground Truths/GT/GT - Auto2/'];
    SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Behaviour/'];
else
    DAY = ['Day', DAY_NUM];
    DIR = '.\Data\OFFICE\';
    READ_DIR = [DIR, 'Ground Truths\GT\GT - Auto2\'];
    SAVE_DIR = [DIR, 'Ground Truths\GT\GT - Behaviour\'];
end

% Load auto-annotated frames
load([READ_DIR, 'Day', DAY_NUM, '.mat']);           % variable = day_01
NUM_PEOPLE = size(day_01{2}{1}, 1);

% Load any manually annotated frames to propagate results from before
if SPECIFIC_PERSON > 1
    day_01_ManualCorrection = load([SAVE_DIR, 'Day', DAY_NUM, '.mat']);
    day_01_Manual = day_01_ManualCorrection.day_01;
end

dayFile = dayFiles.(DAY).file;
if ~EXECUTION_ON_WINDOWS
    path = [DIR, dayFile, '/inspacecam163'];
else
    path = [DIR, dayFile, '\inspacecam163'];
end

fileList = generateFileList(path, EXECUTION_ON_WINDOWS);
numOfFrames = length(fileList);

startFrameName = dayFiles.(DAY).startFrameSubject;  % Fastforward to the frame 
                                                    % where the subject appears 
                                                    % on stage
matches = strfind(fileList, startFrameName);
START_FRAME = find(not(cellfun('isempty', matches)));
endFrameName = dayFiles.(DAY).endFrameSubject;      % Stop at the frame where
                                                    % the subject disappears 
                                                    % for good from stage
matches = strfind(fileList, endFrameName);
END_FRAME = find(not(cellfun('isempty', matches)));
finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                    % of the video so the frame
                                                    % names can be annotated
matches = strfind(fileList, finishFrameName);
FINISH_FRAME = find(not(cellfun('isempty', matches)));

%% Preparation of anotation holding structrure

% Structures to hold date anotation data
% Properties include: [column location, row location, behavior]
% always starting with the main person tracked
% day_N{1}          list of frames
% day_N{1}{x}       name of file 'x'
% day_N{2}          list of all frames's properties' cells
% day_N{2}{x}       list of frames' properties for frame 'x' for all people
% day_N{2}{x}(y,:)  list of properties for frame 'x' for person 'y'
% day_N{2}{x}(y,1)  col position of person 'y' for frame 'x'
% day_N{2}{x}(y,2)  row position of person 'y' for frame 'x'
% day_N{2}{x}(y,3)  behaviour of person 'y' for frame 'x'

relabelManually = false;
prevObservedFrame = START_FRAME;
goBack = false;
saveCurrent = 0;
backMode = false;

% If a manual error made, initialise variable
goBackTo = 1;
saveBack = 1;

%% Pre- and post- (of main) subject presence in the scene for all

% If subject does not appear at the beginning of the video, before
% fastforwarding to the frame of the subject, annotate information
% structure up to that frame
subjectAppearAtStart = strcmp(dayFiles.(DAY).startFrame, dayFiles.(DAY).startFrameSubject);
if ~subjectAppearAtStart
    for currentFrame = INIT_FRAME : START_FRAME - 1
        for person = 1 : SPECIFIC_PERSON
            day_01{2}{currentFrame}(person,:) = [0, 0, 0]; 
        end
    end
end

% If the subject does not appear to the end of the video, after
% annotating the positions in the frames up to the disappearance of
% the subject, keep going until the end of the video so the names of
% the frames can be added to the annotation
subjectAppearToEnd = strcmp(dayFiles.(DAY).endFrame, dayFiles.(DAY).endFrameSubject);
if ~subjectAppearToEnd
    for currentFrame = END_FRAME + 1 : FINISH_FRAME
        for person = 1 : SPECIFIC_PERSON
            day_01{2}{currentFrame}(person,:) = [0, 0, 0]; 
        end
    end
end

%% Iterate through the whole video to apply work
currentFrame = START_FRAME;
while currentFrame <= END_FRAME
    % Save (propagate) previously curated positons in the case of one persion
    % at a time manual correction
    for person = 1 : (SPECIFIC_PERSON - 1)
        day_01{2}{currentFrame}(person,1:2) = day_01_Manual{2}{currentFrame}(person,1:2);
    end

    % Get the subject position discrepency between current and previous
    % frame
    positionDiscrpency = 0;
    if (currentFrame ~= 1) && (SPECIFIC_PERSON ~= 0)
        prevPos = day_01{2}{currentFrame - 1}(SPECIFIC_PERSON,1:2);
        currPos = day_01{2}{currentFrame}(SPECIFIC_PERSON,1:2);
        positionDiscrpency = pdist2(prevPos, currPos);
    end
    
    %% Skip at certain rate unless a problem occurs
    if ((mod(currentFrame, SKIP_RATE) == 0 || positionDiscrpency > POS_DISCREPENCY_LIMIT) || relabelManually || backMode)

        % See the frame's annotation
        frame = imread(fileList{currentFrame});
        figure(1);
        imgHandle = imshow(frame);
        hold on;
        
        %% Draw behaviour annotation panels
        frameSize = size(frame);
        frameWidth = frameSize(1);
        frameLength = frameSize(2);
        
        horizontalSplit = 1 : frameLength;
        horSplitNum = 6;
        paddedLen = ceil(frameLength/horSplitNum)*horSplitNum;
        horizontalSplit(paddedLen) = 0;  % Pad for later re-shape
        horSplitMatrix = reshape(horizontalSplit,paddedLen/horSplitNum,horSplitNum);
        horizontalLines = horSplitMatrix(paddedLen/horSplitNum,:);
        hline(frameWidth/2,'g');
        vline(horizontalLines,'g');
        add_text();
        
        %% The first person, as most importatant - tracked
        behaviour = day_01{2}{currentFrame}(1,3);
        title(['Frame number ', num2str(currentFrame), ', Person ', num2str(person), ': Behaviour ', num2str(behaviour)]);
        
        % Also show positions of people in scene for further validation
        for subject = 1 : NUM_PEOPLE
            positions = day_01{2}{currentFrame}(subject,1:2);
            plot(positions(1), positions(2), 'b*');
        end
        % End of presentation section
        
        %% Being flexible, if more than one person at a time is to be 
        % annotated
        % Being flexible, if only a certain person at a time to be
        % annotated
        startLoop = 0; % Specifcally set to 0 to break if logic flowed
        endLoop = 0; % Specifcally set to 0 to break if logic flowed
        
        if SPECIFIC_PERSON == 0         % Annotate all people in the same run
            startLoop = 1;
            endLoop = NUM_PEOPLE;
        else                            % Annotate one person at a time
            startLoop = 0 + SPECIFIC_PERSON;
            endLoop = NUM_PEOPLE - (NUM_PEOPLE - SPECIFIC_PERSON);
        end
        
        %% Per person processing
        for person = startLoop : endLoop
            
            % Highlight automatic behaviour guess
            behaviour = day_01{2}{currentFrame}(person,3);
            add_text(behaviour);
            
            if backMode
                title(['Back Mode: Frame number ', num2str(currentFrame), ', Person ', num2str(person), ': Behaviour ', num2str(behaviour)]);
            elseif relabelManually
                title(['Frame-by-Frame mode: Frame number ', num2str(currentFrame), ', Person ', num2str(person), ': Behaviour ', num2str(behaviour)]);
            else
                title(['Frame number ', num2str(currentFrame), ', Person ', num2str(person), ': Behaviour ', num2str(behaviour)]);
            end
                
            % Collect user's curser click location
            set(imgHandle, 'ButtonDownFcn',@clickCallback);
            pause;
            buttonClicked = get(gcf,'Selectiontype');
            dataPosition = guidata(imgHandle);
            
            % Click left button, to re-annotate subject's behaviour
            % Click right button, if subject's behaviour is correct already
            if ~isempty(dataPosition) && strcmp(buttonClicked, 'normal')   % Hack, because 'CurrentPoint' cannot be reset
                % Re-label behaviour
                behaviour = behavior_label(dataPosition.position, frameWidth/2, horizontalLines);
                day_01{2}{currentFrame}(person,3) = behaviour;
               
                relabelManually = true;
                add_text(behaviour);
            elseif strcmp(buttonClicked, 'alt')
                % Do nothing: behaviour labels already correct
            elseif strcmp(buttonClicked, 'open')
                % Go back to a few frames back, manual mistake
                if (currentFrame - JUMP_BACK_CORRECTION > 1)
                    goBackTo = currentFrame - JUMP_BACK_CORRECTION;
                else
                   goBackTo = 1;
                end
                goBack = true;
                break;
            end           
        end
        pause(0.001);
        hold off;
        
        % Procedure if went back
        if backMode
            if currentFrame == saveBack          % Time to continue in present
                backMode = false;
                disp('Leave Back Mode.');
            else
                currentFrame = currentFrame + 1; % Contiue in the past
                continue;
            end
        end
        
        % If re-labelled, do not update previous succeessful automatic label
        if ~relabelManually
            prevObservedFrame = currentFrame;   % So it is known where to go back to for manual annotation
            
            % Go back if something went wrong
            if goBack
                goBack = false;
                disp(['Go back from frame: ', num2str(currentFrame)]);
                saveBack = currentFrame;        % So it is known where to continue having returned
                currentFrame = goBackTo;        % Go back
                backMode = true;                % Switch to being back mode
               continue;                        % meant to be for the out-most while
            end
        else % Went/Going back to re-label skipped frames
            if currentFrame == saveCurrent
                relabelManually = false;
                prevObservedFrame = currentFrame;
                cprintf('green', 'Back to skipping mode.=========================================\n');
            elseif currentFrame < saveCurrent
                % Nothing, just continue
            else
                saveCurrent = currentFrame;     % So it is known where to continue manual annotaiton to
                currentFrame = prevObservedFrame + 1;
                cprintf('err', 'Enter frame-by-frame mode.=====================================\n');
                continue;                       % meant to be for the out-most while
            end
        end    
    end
    
    %% Save continuously to avoid any loss of work or time
    save([SAVE_DIR, DAY, '.mat'], 'day_01');
    
    % Increment processed frame
    currentFrame = currentFrame + 1;
end
hold on;
close(gcf)
% Final save
save([SAVE_DIR, DAY, '.mat'], 'day_01');

% Time execution
toc
elapsedTime = toc;
clear;
disp('Process complete!');