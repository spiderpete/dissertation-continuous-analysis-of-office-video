global person frame positions

NUMFRAMES = 10;
FIRSTFRAME= 110;
positions = zeros(4,NUMFRAMES,2);
colorset = ['r' 'g' 'b' 'k'];

% % % % 

for person = 1 : 4
for frame = 1 : NUMFRAMES
[person,frame]
  I = imread(['frame' int2str(FIRSTFRAME+frame-1)],'jpg');
  figure(1)
  imageHandle = imshow(I);
  set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
  pause
 positions(person,frame,:)
figure(2)
  plot([positions(person,frame,1)],[positions(person,frame,2)],[colorset(person) '*-'])
  pause(0.1)
end
end


% save('positions1.mat','positions')
% 
% figure(3)
% clf
% axis([1 640 1 480])
% I = imread(['TEST1/frame' int2str(FIRSTFRAME)],'jpg');
% imshow(I)
% hold on
% for person = 1 : 4
%   plot([positions(person,1:NUMFRAMES,1)],[positions(person,1:NUMFRAMES,2)],[colorset(person) '*-'])
% end



% imObj = rand(500,500);
% figure(1)
% hAxes = axes();
% %imageHandle = imshow(imObj);
% %set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
% figure(2)
% clf
% hold on
% axis([1 640 1 480])
