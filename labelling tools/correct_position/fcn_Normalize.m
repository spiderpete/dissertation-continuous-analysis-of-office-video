function normalImage = fcn_Normalize(image)
  % convert to normalised RGB
  normalImage(:,:,1) = image(:,:,1)./(image(:,:,1)+image(:,:,2)+image(:,:,3)); 
  normalImage(:,:,2) = image(:,:,2)./(image(:,:,1)+image(:,:,2)+image(:,:,3));
  normalImage(:,:,3) = image(:,:,3)./(image(:,:,1)+image(:,:,2)+image(:,:,3));
end