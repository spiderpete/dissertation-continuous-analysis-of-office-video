%% get erro list, postion =0 but behavior != 1
get_behave_list = find(mod1_behavior3~=1);
get_positions = positions3;
j =1;

for i=1:length(get_behave_list)
    if get_positions(1,get_behave_list(i),1)==0
        new_ilist_num(j) = get_behave_list(i);
        j = j+1;
    end
end

%% 3 test on new list to check if there is error


for i=1:length(new_ilist)
% for i=6500:7000
    I = imread(get_ilist{new_ilist_num(i)});
    imshow(I);
    hold on
    plot(get_positions(1,new_ilist_num(i),1),get_positions(1,new_ilist_num(i),2),'*');
    hold off
    get_behavior(new_ilist_num(i))
%         pause(0.01);
    i
    pause(0.05);
%     pause(0.3);
% pause;
end

%% 2 get new list // get new positions
get_behavior = mod1_behavior3; 
get_ilist = ilist3;
 new_ilist = get_ilist(new_ilist_num);
 new_postions = get_positions(1,new_ilist_num,:);

 
 %% 4
%  go run the behaviors.m
% or go run the postions.m
% change 'ilist' & 'behavior'

 %% 5 test on new list (dont use this one)
 for i=1:length(new_ilist)
% for i=975:1000
     imshow(new_ilist{i});
     hold on
     plot(new_postions(1,i,1),new_postions(1,i,2),'*');
     hold off
     pause(0.01);
%      pause(0.2);
     i
     behavior(i)
%      pause;
 end
%% 6 rewrite the positions
get_positions(1,new_ilist_num,:) = positions;
mod_pos3 = get_positions;

%% clear var
clearvars get_behave_list get_positions get_behavior get_ilist i behavior new_ilist new_postions new_ilist_num
