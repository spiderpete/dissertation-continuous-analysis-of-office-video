global person frame positions
% imlist
%           DONT
%           ADD
%           ANYTHING
%           FROM
%           FUNCTION
ilist = new_ilist;
NUMFRAMES = length(ilist);
positions = zeros(1,NUMFRAMES,2);
colorset = ['r' 'g' 'b' 'k'];
person = 1;
frame = 1;
for frame=1:NUMFRAMES
            frame
            I = imread(ilist{frame});
            imageHandle = imshow(I);
            set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
            pause
            positions(person,frame,:)
            hold on
            plot([positions(person,frame,1)],[positions(person,frame,2)],[colorset(person) '*-'])
            hold off
            pause(0.1)
end