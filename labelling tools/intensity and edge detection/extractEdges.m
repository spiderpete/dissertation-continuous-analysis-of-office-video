function [ frameEdges ] = extractEdges( frame )
%EXTRACTEDGES extracts horizontal and vertical difference images
%   Edges are extracted along all image channels separately
%   frameEdges(Xs,Ys, n):
%   n = 1: Red channel horizontal edge image
%   n = 2: Red channel vertical edge image
%   n = 3: Green channel horizontal edge image
%   n = 4: Green channel vertical edge image
%   n = 5: Blue channel horizontal edge image
%   n = 6: Blue channel vertical edge image
    HOR_AND_VERT_DIMENSION = 2;
    RED_CHANNEL = 1;
    GREEN_CHANNEL = 2;
    BLUE_CHANNEL = 3;
    
    frameEdges = zeros([size(frame(:,:, 1)), 6]);
    
    frameEdges(:,:, RED_CHANNEL) = edge(frame(:,:, RED_CHANNEL),'Sobel', [],'horizontal');
    frameEdges(:,:, RED_CHANNEL + 1) = edge(frame(:,:, RED_CHANNEL),'Sobel', [],'vertical');
    
    frameEdges(:,:, GREEN_CHANNEL * HOR_AND_VERT_DIMENSION - 1) = edge(frame(:,:, GREEN_CHANNEL),'Sobel', [],'horizontal');
    frameEdges(:,:, GREEN_CHANNEL * HOR_AND_VERT_DIMENSION) = edge(frame(:,:, GREEN_CHANNEL),'Sobel', [],'vertical');

    frameEdges(:,:, BLUE_CHANNEL * HOR_AND_VERT_DIMENSION - 1) = edge(frame(:,:, BLUE_CHANNEL),'Sobel', [],'horizontal');
    frameEdges(:,:, BLUE_CHANNEL * HOR_AND_VERT_DIMENSION) = edge(frame(:,:, BLUE_CHANNEL),'Sobel', [],'vertical');

end

