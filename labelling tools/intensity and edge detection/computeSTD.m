function [ currentSTD ] = computeSTD( prevSTD, currentMean, currentFrame, LEARNING_RATE )
%COMPUTESTD Computes standard deviation paramter
%   prevSTD(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
%   currentMean(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
%   currentFrame(Xs,Ys, Colours and/or Horizontal/Vertical Differencess = any)
%
%   currentSTD(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
    currentFrame = double(currentFrame);

    meanSubtracted = currentFrame - currentMean;
    squared = power(meanSubtracted, 2);
    currentSTD = LEARNING_RATE .* squared + (1 - LEARNING_RATE) .* prevSTD;
    
end

