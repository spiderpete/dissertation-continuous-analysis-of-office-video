function intensityAndEdgeDetection(dayNum)
    %dayNum = '03';
    disp(['Day: ', num2str(dayNum)]);
    
    %% Initialisation of algorithm variables
    % Time execution
    tic

    % Custamisable parameters
    DAY_NUM = dayNum;
    PREVIEW_LOC = false;
    INIT_FRAME = 1; % 396
    NUM_OF_BG_HIST = 3;
    EXECUTION_ON_WINDOWS = false;
    LEARNING_RATE = 0.1;
    HYSTERESIS_LOWER = 0.000000001;
    HYSTERESIS_UPPER = 0.80;
    FG_HIST_SIZE = 2;
    PERSON_MIN_AREA = 30000;
    MAX_NUM_OF_PEOPLE_EXPECTED = 3;
    % Should not be too high because otherwise frames where main person leaves will not be handled well
    MAX_NUM_MISSED_TO_FOLLOW_UP_WITH = 30;
    LEARNING_RATE_COLOUR_HIST = 0.25;
    DISPLAY_FROM = 2520;
    % Colour theersholds
    m_C = 15;
    M_C = 25;
    % Edge theersholds
    m_E = 3;
    M_E = 9;

    % Stationary parameters
    if ~EXECUTION_ON_WINDOWS
        DAY = ['Day', DAY_NUM];
        DIR = './Data/OFFICE/';
        SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Colour and Edges/'];
    else
        DAY = ['Day', DAY_NUM];
        DIR = '.\Data\OFFICE\';
        SAVE_DIR = [DIR, 'Ground Truths\GT\GT - Colour and Edges\'];
    end
    
    CHANGE_THRESHOLD = 0;
    dayFile = dayFiles.(DAY).file;
    if ~EXECUTION_ON_WINDOWS
        path = [DIR, dayFile, '/inspacecam163'];
    else
        path = [DIR, dayFile, '\inspacecam163'];
    end
    
    fileList = generateFileList(path, EXECUTION_ON_WINDOWS);
    numOfFrames = length(fileList);
    
    startFrameName = dayFiles.(DAY).startFrameSubject;  % Fastforward to the frame 
                                                        % where the subject appears 
                                                        % on stage
    matches = strfind(fileList, startFrameName);
    START_FRAME = find(not(cellfun('isempty', matches)));
    endFrameName = dayFiles.(DAY).endFrameSubject;      % Stop at the frame where
                                                        % the subject disappears 
                                                        % for good from stage
    matches = strfind(fileList, endFrameName);
    END_FRAME = find(not(cellfun('isempty', matches)));
    finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                        % of the video so the frame
                                                        % names can be annotated
    matches = strfind(fileList, finishFrameName);
    FINISH_FRAME = find(not(cellfun('isempty', matches)));

    % Path to Background History for Adaptive Background Change algorithm
    if ~EXECUTION_ON_WINDOWS
        pathsToBGs = [DIR, dayFile, '/initBG'];
    else
        pathsToBGs = [DIR, dayFile, '\initBG'];
    end
    
    % Auxiliary notes array
    notes = cell(numOfFrames, 0);
    
    %% Preparation of anotation holding structrure

    % Structures to hold date anotation data
    % Properties include: [column location, row location, behavior]
    % always starting with the main person tracked
    % day_N{1}          list of frames
    % day_N{1}{x}       name of file 'x'
    % day_N{2}          list of all frames's properties' cells
    % day_N{2}{x}       list of frames' properties for frame 'x' for all people
    % day_N{2}{x}(y,:)  list of properties for frame 'x' for person 'y'
    % day_N{2}{x}(y,1)  col position of person 'y' for frame 'x'
    % day_N{2}{x}(y,2)  row position of person 'y' for frame 'x'
    % day_N{2}{x}(y,3)  behaviour of person 'y' for frame 'x'
    positions = zeros(numOfFrames, 2);
    day_01 = cell(1, 2);
    day_01{1} = cell(size(fileList));
    day_01{2} = cell(size(fileList));
    for days = 1 : numOfFrames
        % Postions: day_N{2}{x}(y,1) and day_N{2}{x}(y,2) default to 0 in the
        % default situation when the person is not present
        day_01{2}{days} = zeros(1, 3);
    end
    
    %% Algorithm Detection Preparation - Building Background Model

    % If the video of the day does not have sample background frames at its
    % beginning, take the relative example from the initBG folder and
    % synthesise more up to the required size for the history for Adaptive
    % Background Change algorithm
    if ~dayFiles.(DAY).bgHistPresent
       synthesisePhoto(pathsToBGs, NUM_OF_BG_HIST, EXECUTION_ON_WINDOWS); 
    end

    % Calculate initial Mean and Standard Deviation for colour frames
    fileBGList = generateFileList(pathsToBGs, EXECUTION_ON_WINDOWS);
    numOfBGs = length(fileBGList);
    history = zeros([size(imread(fileBGList{1})), numOfBGs]);   
    for currentBGframe = 1 : numOfBGs
        bgImage = (imread(fileBGList{currentBGframe}));
        focusedBGImage = ignoreMonitorAndWindow(bgImage, DAY);
        history(:,:,:, currentBGframe) = focusedBGImage;
    end
    
    % meanColours(Xs, Ys, Colours = 3)
    meanColours = initMean(history);
    % stdColours(Xs, Ys, Colours = 3)
    stdColours = initSTD(history, meanColours);
    
    % Calculate initial Mean and Standard Deviation for edge frames
    % bgFrameEdges(Xs, Ys, Colours * 2 = 6, bgImg-s)
    bgFrameEdges = zeros([size(history(:,:,1, 1)), size(history, 3) * 2, numOfBGs]);
    for bgFrame = 1 : numOfBGs
        bgFrameEdges(:,:,:, bgFrame) = extractEdges(history(:,:,:, bgFrame));
    end
    
    % meanEdges(Xs, Ys, Colours and Horizontal/Vertical Differencess = 6)
    meanEdges = initMean(bgFrameEdges);
    % stdEdges(Xs, Ys, Colours and Horizontal/Vertical Differencess = 6)
    stdEdges = initSTD(bgFrameEdges, meanEdges);
    
    %% Loop through whole video and anotate

    % If the subject does not appear to the end of the video, after
    % annotating the positions in the frames up to the disappearance of
    % the subject, keep going until the end of the video so the names of
    % the frames can be added to the annotation
    subjectAppearToEnd = strcmp(dayFiles.(DAY).endFrame, dayFiles.(DAY).endFrameSubject);
    if ~subjectAppearToEnd
        for currentFrame = END_FRAME + 1 : FINISH_FRAME
            day_01{1}{currentFrame} = fileList{currentFrame}; 
            for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                notes{currentFrame}(person,:) = zeros(1, 4);
            end
        end
    end
    
    % History of Foreground Masks
    frameSize = size(imread(fileBGList{1}));
    fgMaskHist = false([frameSize(1:2), FG_HIST_SIZE]);
    
    % Reference Colour Histogram
    refHist = zeros(1, 3 * 256);
    isRefHistExtracted = false;
    
    % Interpolation: missed frames
    missed = [];
    
    % Iterate through all the frames of the video
    for currentFrame = INIT_FRAME : END_FRAME
        
        % Save Name of frame and prepare structure with default values
        day_01{1}{currentFrame} = fileList{currentFrame};
        for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
            day_01{2}{currentFrame}(person,:) = [0, 0, 0];
            notes{currentFrame}(person,:) = zeros(1, 4);
        end

        % Load the images
        imgCurrent = imread(fileList{currentFrame});
        imgCurrentOrig = imgCurrent;

        % Blank out Monitor pixels
        imgCurrent = ignoreMonitorAndWindow(imgCurrent, DAY);
        
        % Extract edges for horizontal and vertical defferences
        % imgCurrentEdges(Xs,Ys,Colours and/or Horizontal/Vertical Differencess)
        imgCurrentEdges = extractEdges(imgCurrent);
        
        %% Implementation of equations from paper "Detection and Location of 
        % People in Video Images Using Adaptive Fusion of Color and Edge Information "
        
        % Colour-based subtraction
        colourDifference = meanColours - double(imgCurrent);
        colourConfidence = ((colourDifference - m_C .* stdColours) ./ ...
                            (M_C .* stdColours - m_C .* stdColours)) .* 100;
        colourConfidence(colourDifference < m_C .* stdColours) = 0;
        colourConfidence(colourDifference > M_C .* stdColours) = 1;
        
        colourConfidence2D = max(colourConfidence, [], 3);

        % Edge-based subtraction
        edgeDifference = abs(meanEdges - double(imgCurrentEdges));
        
        % Split computation on per colour channel bases
        edgeDiffR = edgeDifference(:,:,1:2);
        edgeDiffG = edgeDifference(:,:,3:4);
        edgeDiffB = edgeDifference(:,:,5:6);
        
        % Computation of Edge Gradiaents per channel
        edgeGradientR = sum(edgeDiffR, 3);
        edgeGradientG = sum(edgeDiffG, 3);
        edgeGradientB = sum(edgeDiffB, 3);
        
        % Computation of Reliability Factor
        edgeGradientCurrentR = sum(abs(imgCurrentEdges(:,:,1:2)), 3);
        edgeGradientPrevR = sum(abs(meanEdges(:,:,1:2)), 3);
        edgeGradientCurrentG = sum(abs(imgCurrentEdges(:,:,3:4)), 3);
        edgeGradientPrevG = sum(abs(meanEdges(:,:,3:4)), 3);
        edgeGradientCurrentB = sum(abs(imgCurrentEdges(:,:,5:6)), 3);
        edgeGradientPrevB = sum(abs(meanEdges(:,:,5:6)), 3);
        % Find maximum between current edge gradients and accumulated until
        % now edge gradients
        normaliserR = max(cat(3, edgeGradientCurrentR, edgeGradientPrevR), [], 3);
        normaliserG = max(cat(3, edgeGradientCurrentG, edgeGradientPrevG), [], 3);
        normaliserB = max(cat(3, edgeGradientCurrentB, edgeGradientPrevB), [], 3);
        % Reliability result
        reliabilityR = edgeGradientR ./ normaliserR;
        reliabilityG = edgeGradientG ./ normaliserG;
        reliabilityB = edgeGradientB ./ normaliserB;
        
        % Confidence factor computation
        weightedDifferenceR = reliabilityR .* edgeGradientR;
        weightedDifferenceG = reliabilityG .* edgeGradientG;
        weightedDifferenceB = reliabilityB .* edgeGradientB;
        % Sum of standard deviations the horizontal and vertical directions
        stdEdgesSumR = sum(stdEdges(:,:,1:2), 3);
        stdEdgesSumG = sum(stdEdges(:,:,3:4), 3);
        stdEdgesSumB = sum(stdEdges(:,:,5:6), 3);
        % Confidence factor output for Red channel
        edgeConfidenceR = ((weightedDifferenceR - m_E .* stdEdgesSumR) ./ ...
                          (M_E .* stdEdgesSumR - m_E .* stdEdgesSumR)) .* 100;
        edgeConfidenceR(weightedDifferenceR < m_E .* stdEdgesSumR) = 0;
        edgeConfidenceR(weightedDifferenceR > M_E .* stdEdgesSumR) = 1;
        edgeConfidenceR(isnan(edgeConfidenceR)) = 0;    % NaNs will appear where no edges are found, when dividing by 0 above
        % Confidence factor output for Green channel
        edgeConfidenceG = ((weightedDifferenceG - m_E .* stdEdgesSumG) ./ ...
                          (M_E .* stdEdgesSumG - m_E .* stdEdgesSumG)) .* 100;
        edgeConfidenceG(weightedDifferenceG < m_E .* stdEdgesSumG) = 0;
        edgeConfidenceG(weightedDifferenceG > M_E .* stdEdgesSumG) = 1;
        edgeConfidenceG(isnan(edgeConfidenceG)) = 0;    % NaNs will appear where no edges are found, when dividing by 0 above
        % Confidence factor output for Blue channel
        edgeConfidenceB = ((weightedDifferenceB - m_E .* stdEdgesSumB) ./ ...
                          (M_E .* stdEdgesSumB - m_E .* stdEdgesSumB)) .* 100;
        edgeConfidenceB(weightedDifferenceB < m_E .* stdEdgesSumB) = 0;
        edgeConfidenceB(weightedDifferenceB > M_E .* stdEdgesSumB) = 1;
        edgeConfidenceB(isnan(edgeConfidenceB)) = 0;    % NaNs will appear where no edges are found, when dividing by 0 above
        
        % Final Edge Confidence Mask
        edgeConfidence = max(cat(3, edgeConfidenceR, edgeConfidenceG, edgeConfidenceB), [], 3);
        
        % Combine Colour and Edge Confidence Maps
        confidenceMap = max(cat(3, colourConfidence2D, edgeConfidence), [], 3);
        
        % Morphology applied - Only Salt and Pepper
        clearedMap = morphology(confidenceMap);
        
        % Hysteresis applied to remove false positives (not connected to area with 100% foreground)
        [~, hysteresisMap] = hysteresis3d(clearedMap, HYSTERESIS_LOWER, HYSTERESIS_UPPER, 8);
        
        % Flickering effected removed
        cancelledFlickering = false(size(hysteresisMap));
        if (currentFrame >= INIT_FRAME + 2)
            diffWithTwoFrameAgo = hysteresisMap & fgMaskHist(:,:, 1); % C && A = p
            diffWithOneFrameAgo = hysteresisMap & fgMaskHist(:,:, 2); % C && B = q
            % Simplified expression: 
            % NOT(p => q) -> NOT(NOT p OR q) -> p AND NOT q
            flickerPoints = diffWithTwoFrameAgo & ~diffWithOneFrameAgo;
            
            % Apply flicker filter
            cancelledFlickering = hysteresisMap & ~flickerPoints;
        end
        % Update Foreground History Map for Flickering effect removal
        fgMaskHist(:,:, 1) = fgMaskHist(:,:,2);
        fgMaskHist(:,:, 2) = hysteresisMap;
        
        % Locate the coordinates of the largest change aka the person
        totalArea = size(hysteresisMap, 1) * size(hysteresisMap, 2);
        largestBlobs = bwareafilt(cancelledFlickering, MAX_NUM_OF_PEOPLE_EXPECTED, 'largest');
        props = regionprops(largestBlobs, 'Area', 'Centroid', 'BoundingBox');
        
        %{
        if currentFrame >= 0 %START_FRAME
            imshow(cancelledFlickering);
            title(['FLICKER free Map at: ', num2str(currentFrame)]);
            pause(0.1);
            hold on;
        end
        %}
        
        %% Take decision about where person/people are
        
        if (size(props, 1) == 0)    % Absoloutely no detection: all black mask
            % No detection => no one in scene
            if currentFrame <= START_FRAME
                for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                    day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                    notes{currentFrame}(person,:) = zeros(1, 4);
                end
            else
                % Keep track of missed frame until an annotated one is seen
                missed = [missed, currentFrame];
            end
        else                        % Some detection done
            % Save properties
            centroids = cat(1, props.Centroid);
            areas = cat(1, props.Area);
            boundingBoxes = cat(1, props.BoundingBox);
            
            % Order the blobs according to areas
            [areas, indecies] = sort(areas, 'descend');
            % Filter small blobs
            numOfPeoplePresent = 0;
            for i = 1 : size(areas)
               if areas(i) >= PERSON_MIN_AREA
                   numOfPeoplePresent = numOfPeoplePresent + 1;
               else
                   break;
               end
            end
            
            % Extract reference colour histogram of main person, assuming
            % the first time the person appears, their blob will be the
            % biggest and the most prominent, if not then the running
            % average updated on single blob frames will tailor this
            % feature to more closely resemble the real main person's
            % colour histogram
            mainPersonIndex = 0;
            if ~isRefHistExtracted && numOfPeoplePresent > 0    % No reference colour histogram extracted (happens when at least one frame's blob satisfies prerequisits) + people found
                isRefHistExtracted = true;
                
                subimage = imcrop(imgCurrent, boundingBoxes(indecies(1),:));
                [r, g, b] = rgbhist(subimage,0,1);
                hist = [r' g' b'];
                refHist = hist / 3;
                
                mainPersonIndex = 1;
            else                                                % reference colour histogram extracted OR not extracted + no people present
                if numOfPeoplePresent == 1                      % 1 person present only => the main person
                    subimage = imcrop(imgCurrent, boundingBoxes(indecies(1),:));
                    [r, g, b] = rgbhist(subimage,0,1);
                    hist = [r' g' b'];
                    hist = hist / 3;
                    refHist = computeMean(refHist, hist, LEARNING_RATE_COLOUR_HIST);
                    
                    mainPersonIndex = 1;
                elseif numOfPeoplePresent > 1                   % 2 or 3 people present
                    histograms = zeros(1, 3 * 256);
                    for blob = 1 : numOfPeoplePresent % numOfPeoplePresent <= 3
                        subimage = imcrop(imgCurrent, boundingBoxes(indecies(blob),:));
                        [r, g, b] = rgbhist(subimage,0,1);
                        hist = [r' g' b'];
                        hist = hist / 3;
                        histograms(blob,:) = hist;
                    end

                    % Find the blob resembling the main person the most
                    distances = zeros(1, numOfPeoplePresent);
                    for blob = 1 : numOfPeoplePresent % numOfPeoplePresent <= 3
                        distances(blob) = bhattacharyya(histograms(blob,:), refHist);
                    end
                    
                    mainPersonIndex = find(distances == min(distances));
                else                                            % 0 or more than 3 people present
                    % No Blobs satisfy people detection criteria => no one
                    % in scene (but there could be people present still)
                    if currentFrame <= START_FRAME
                        for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                            day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                            notes{currentFrame}(person,:) = zeros(1, 4);
                        end
                    else
                        % Keep track of missed frame until an annotated one is seen
                        missed = [missed, currentFrame];
                    end
                end
                
                %disp(['Main persons blob is: ', num2str(mainPersonIndex)]);
            end
            
            if numOfPeoplePresent > 0
                % Save properties for all people present in the scene
                day_01{2}{currentFrame}(1, 1:2) = centroids(indecies(mainPersonIndex),:);
                notes{currentFrame}(1,:) = boundingBoxes(indecies(mainPersonIndex),:);
                personCount = 2;
                for person = 1 : numOfPeoplePresent
                    if person == mainPersonIndex
                        continue;
                    else
                        day_01{2}{currentFrame}(personCount, 1:2) = centroids(indecies(person),:);
                        notes{currentFrame}(personCount,:) = boundingBoxes(indecies(person),:);
                        personCount = personCount + 1;
                    end
                end
            end
            
            %% Interpolation
            % If there are any missed frames, go back and interpolate
            % annotations up to the previously annotated frame only for
            % the main person
            if size(missed, 1) ~= 0 && numOfPeoplePresent > 0
                
                if size(missed, 1) <= MAX_NUM_MISSED_TO_FOLLOW_UP_WITH
                    % Fill non-annotated frames
                    numOfMissedFrames = size(missed, 1);
                    firstMissedFromStreak = missed(1);
                    lastMissedFromStreak = missed(numOfMissedFrames);

                    XS = 1;
                    YS = 2;
                    X_WIDTH = 3;
                    Y_WIDTH = 4;
                    % Interpolate centres of main person

                    beforeAndAfterMissedCentresData = [day_01{2}{firstMissedFromStreak - 1}(1,1:2); ...
                                            day_01{2}{firstMissedFromStreak + 1}(1,1:2)];
                    xs = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedCentresData(:, XS), missed);
                    ys = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedCentresData(:, YS), missed);

                    % Interpolate bounding boxes of main person
                    beforeAndAfterMissedRecData = [notes{firstMissedFromStreak - 1}(1,:); ...
                                            notes{firstMissedFromStreak + 1}(1,:)];
                    recX = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedRecData(:, XS), missed);
                    recY = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedRecData(:, YS), missed);
                    recXWidth = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedRecData(:, X_WIDTH), missed);
                    recYWidth = interpn([firstMissedFromStreak - 1, lastMissedFromStreak + 1], ...
                                    beforeAndAfterMissedRecData(:, Y_WIDTH), missed);

                    % Fill in the missed frames gaps with the interpolated
                    % collected data
                    for i = 1 : numOfMissedFrames
                        day_01{2}{missed(i)}(1,1) = xs(i);
                        day_01{2}{missed(i)}(1,2) = ys(i);
                        notes{missed(i)}(1,1) = recX(i);
                        notes{missed(i)}(1,2) = recY(i);
                        notes{missed(i)}(1,3) = recXWidth(i);
                        notes{missed(i)}(1,4) = recYWidth(i);
                    end
                end
                
                missed = [];
            end
            
            %{
            if currentFrame >= DISPLAY_FROM && rem(currentFrame, 9) == 0
                % Only display the properties of the found up to 3 largest
                % blobs
                figure(1);
                imshow(imgCurrentOrig);
                title(['Frame number ', num2str(currentFrame)]);
                hold on;
                colours = ['r', 'g', 'b'];
                for blob = 1 : numOfPeoplePresent % numOfPeoplePresent <= 3
                    disp(['Centroids: ', num2str(centroids(indecies(blob),:))]);
                    disp(['Area: ', num2str(areas(indecies(blob)))]);
                    disp(['Bounding Box: ', num2str(boundingBoxes(indecies(blob), :))]);

                    plot(centroids(indecies(blob), 1), centroids(indecies(blob), 2), [colours(blob), '*']);
                    rectangle('Position', boundingBoxes(indecies(blob),:), 'EdgeColor', colours(blob), 'LineWidth', 3);
                end
                disp('=============================================================');
                hold off;
                
                figure(2);
                imshow(cancelledFlickering);
                title(['Mask of frame ', num2str(currentFrame)]);

                pause;
            end
            %}
        end
        
        
        % Mean and Standard Deviation Update
        imgCurrentBG = bsxfun(@times, double(imgCurrent), double(~cancelledFlickering)) + bsxfun(@times, meanColours, double(cancelledFlickering));
        imgCurrentEdgesBG = bsxfun(@times, imgCurrentEdges, double(~cancelledFlickering)) + bsxfun(@times, meanEdges, double(cancelledFlickering));
        meanEdges = computeMean(meanEdges, imgCurrentEdgesBG, LEARNING_RATE);
        stdEdges = computeSTD(stdEdges, meanEdges, imgCurrentEdgesBG, LEARNING_RATE);
        
        %% Save continuously to avoid any loss of work or time
        save([SAVE_DIR, DAY, '.mat'], 'day_01'); 
        save([SAVE_DIR, DAY, '_notes', '.mat'], 'notes');
        
        if rem(currentFrame, 100) == 0
            disp(num2str(currentFrame));
        end
    end

    %% Save all information and time

    % Save all the locations and names
    save([SAVE_DIR, DAY, '.mat'], 'day_01'); 
    save([SAVE_DIR, DAY, '_notes', '.mat'], 'notes');

    %Time execution
    toc
    elapsedTime = toc;
    clear;

end