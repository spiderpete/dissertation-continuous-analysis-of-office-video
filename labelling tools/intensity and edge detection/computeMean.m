function [ currentMean ] = computeMean( prevMean, currentFrame, LEARNING_RATE )
%COMPUTEMEAN Computes exponential running average mean (squared!)
%   prevMean(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
%   currentFrame(Xs,Ys, Colours and/or Horizontal/Vertical Differencess = any)
%
%   currentMean(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
    currentFrame = double(currentFrame);
    
    % Computing exponential average of mean
    currentMean = LEARNING_RATE .* currentFrame + (1 - LEARNING_RATE) .* prevMean;
    
end

