function morphedImage = morphology( image )
%MORPHOLOGY Summary of this function goes here
%   Detailed explanation goes here
    SMALL_AREAS = 5000;
    
    saltAndPepperRemoved = medfilt2(image);
    smallRemoved = bwareaopen(saltAndPepperRemoved, SMALL_AREAS);
    
    se = strel('square', 3);
    imClosed = imclose(smallRemoved, se);
    imFilled = imfill(imClosed, 'holes');

    morphedImage = imFilled;
end
