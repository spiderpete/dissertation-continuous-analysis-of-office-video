function [ frameMean ] = initMean( frameList )
%INITMEAN Initiates mean parameter
%   frameList(Xs, Ys, 
%             Colours and/or Horizontal/Vertical Differencess = any, 
%             Num of background frames = any)
%
%   frameMean(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
    %RED_CHANNEL = 1;
    %GREEN_CHANNEL = 2;
    %BLUE_CHANNEL = 3;
    DIMENSION = 4;

    frameList = double(frameList);
    %frameMean = frameList(:,:,:, 1);
    numOfFrames = size(frameList, DIMENSION);
    
    %{
    sumFrameR = frameList(:,:, RED_CHANNEL, 1);
    sumFrameG = frameList(:,:, GREEN_CHANNEL, 1);
    sumFrameB = frameList(:,:, BLUE_CHANNEL, 1);
    %}
    
    %{
    for frameR = 1 : numOfFrames
       sumFrameR = sumFrameR + frameList(:,:, frameR);
    end
    for frameG = 1 : numOfFrames
       sumFrameG = sumFrameG + frameList(:,:, frameG);
    end
    for frameB = 1 : numOfFrames
       sumFrameB = sumFrameB + frameList(:,:, frameB);
    end
    %}
    
    frameMean = sum(frameList, DIMENSION) ./ numOfFrames;
    
    %{
    frameMean(:,:, RED_CHANNEL) = sumFrameR ./ numOfFrames;
    frameMean(:,:, GREEN_CHANNEL) = sumFrameG ./ numOfFrames;
    frameMean(:,:, BLUE_CHANNEL) = sumFrameB ./ numOfFrames;
    %}
    
end

