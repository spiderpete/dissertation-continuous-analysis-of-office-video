function [ frameSTD ] = initSTD( frameList, frameMean )
%INITSTD Initiates standard deviation parameter (squared!)
%   frameList(Xs, Ys, 
%             Colours and/or Horizontal/Vertical Differencess = any, 
%             Num of background frames = any)
%   frameMean(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
%
%   frameSTD(Xs, Ys, Colours and/or Horizontal/Vertical Differencess = any)
    %RED_CHANNEL = 1;
    %GREEN_CHANNEL = 2;
    %BLUE_CHANNEL = 3;
    DIMENSION = 4;

    frameList = double(frameList);
    %frameSTD = frameList(:,:,:, 1);
    numOfFrames = size(frameList, DIMENSION);
    %sumFrameR = frameList(:,:, RED_CHANNEL, 1);
    %sumFrameG = frameList(:,:, GREEN_CHANNEL, 1);
    %sumFrameB = frameList(:,:, BLUE_CHANNEL, 1);
    
    %{
    for frameR = 1 : numOfFrames
       offset = frameList(:,:, frameR) - frameMean(:,:, RED_CHANNEL);
       sumFrameR = sumFrameR + power(offset, 2);
    end
    for frameG = 1 : numOfFrames
        offset = frameList(:,:, frameR) - frameMean(:,:, GREEN_CHANNEL);
       sumFrameG = sumFrameG + power(offset, 2);
    end
    for frameB = 1 : numOfFrames
        offset = frameList(:,:, frameR) - frameMean(:,:, BLUE_CHANNEL);
       sumFrameB = sumFrameB + power(offset, 2);
    end
    %}

    meanSubtracted = bsxfun(@minus, frameList, frameMean);
    squared = power(meanSubtracted, 2);
    summed = sum(squared, DIMENSION);
    frameSTD = summed ./ numOfFrames;
    
    %{
    frameSTD(:,:, RED_CHANNEL) = sumFrameR ./ numOfFrames;
    frameSTD(:,:, GREEN_CHANNEL) = sumFrameG ./ numOfFrames;
    frameSTD(:,:, BLUE_CHANNEL) = sumFrameB ./ numOfFrames;
    %}

end

