function fileList = generateFileList(path, isWindowsOS)
    %path = './Data/OFFICE/2016_02_18/';

    if ~isWindowsOS
        [~, dirContent] = system(['find ', path, ' -name "*.jpg"']);
        files = textscan( dirContent, '%s', 'delimiter', '\n' );
        fileList = files{1};
        fileList = sort(fileList);
    else
        [~, dirContent] = system(['dir ', path, '\*jpg', ' /b']);
        files = textscan( dirContent, '%s', 'delimiter', '\n' );
        fileList = files{1};
        fileList = strcat([path, '\'], fileList);
        fileList = sort(fileList);
    end
end