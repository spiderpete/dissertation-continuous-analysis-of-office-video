function chromedImage = chromCoordsConversion(frame)
    % initialise algorithm variables
    chromRed = 1;
    chromGreen = 2;
    chromIntens = 3;
    
    % prepare output dimensions and cardinalities
    [frameRow, frameCol, channels] = size(frame);
    chromedImage = zeros(frameRow, frameCol, 3);
    
    % double precision for calculations
    currentFrameDouble = double(frame);
    
    % chromaticity coordinates calcualtions: red, green, intensity
    chromCoords = zeros(frameRow, frameCol, channels);
    rgbSum = currentFrameDouble(:,:, 1) + currentFrameDouble(:,:, 2) + currentFrameDouble(:,:, 3);
    
    % safeguarding against unexpected colour values
    rgbSum(rgbSum == 0) = 1;
    
    chromCoords(:,:, chromRed) = currentFrameDouble(:,:, 1) ./ rgbSum;
    chromCoords(:,:, chromGreen) = currentFrameDouble(:,:, 2) ./ rgbSum;
    chromCoords(:,:, chromIntens) = rgbSum ./ 3;
    
    chromedImage = chromCoords;

end