function synthesisePhoto(directory, histSize, isWindowsOS)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    NUM_OF_BG_HIST = histSize;
    
    fileList = generateFileList(directory, isWindowsOS);
    imgNameFull = fileList{1};
    [~,imgName,~] = fileparts(imgNameFull);
    image = imread(imgNameFull);
    
    for numImgHist = 1 : NUM_OF_BG_HIST - 1
        % -1 because, the total number of needed frames is NUM_OF_BG_HIST
        % and there is one already present that the other NUM_OF_BG_HIST -
        % 1 are to be created from
        noisedImage = imnoise(image,'gaussian');
        imwrite(noisedImage,[directory, '/', imgName, '_', num2str(numImgHist), '.jpg']);
    end

end

