function imageOutput = emphasiseInterestArea(imageInput, centre, DAY)
    INTEREST_AREA_RADIUS = dayFiles.(DAY).areaInterestParameter;

    xLimit = size(imageInput, 2);
    yLimit = size(imageInput, 1);
    xCentre = centre(1);
    yCentre = centre(2);
    
    maskImg = zeros(size(imageInput));
    if (yCentre - INTEREST_AREA_RADIUS < 1)
        yLowLimit = 1;
    else
        yLowLimit = yCentre - INTEREST_AREA_RADIUS;
    end
    if yCentre + INTEREST_AREA_RADIUS > yLimit
        yTopLimit = yLimit;
    else
        yTopLimit = yCentre + INTEREST_AREA_RADIUS;
    end
    if xCentre - INTEREST_AREA_RADIUS < 1
        xLowLimit = 1;
    else
        xLowLimit = xCentre - INTEREST_AREA_RADIUS;
    end
    if xCentre + INTEREST_AREA_RADIUS > xLimit
        xTopLimit = xLimit;
    else
        xTopLimit = xCentre + INTEREST_AREA_RADIUS;
    end
    maskImg(yLowLimit : yTopLimit, xLowLimit : xTopLimit,:) = 1;

    if isa(imageInput, 'uint8')
        imageOutput = imageInput .* uint8(maskImg);
    elseif isa(imageInput, 'double')
        imageOutput = imageInput .* double(maskImg);
    else
        imageOutput = imageInput;
        disp('ERROR: Unexpected type for input to emphasiseInterestArea.m');
        
end