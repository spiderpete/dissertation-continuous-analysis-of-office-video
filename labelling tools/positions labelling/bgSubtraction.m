function foreground = bgSubtraction(currentFrame, bgFrame, isFocused)
%BGSUBTRACTION Summary of this function goes here
%   Detailed explanation goes here
    SUBTRACTION_THRESHOLD = 10;

    % subtract background & select pixels with a big difference
    foregroundSubtracted = zeros(size(currentFrame));
    
    if isFocused
        currentNoMonitor = ignoreMonitor(currentFrame);
        bgNoMonitor = ignoreMonitor(bgFrame);
        img1Focused = emphasiseInterestArea(currentNoMonitor);
        img2Focused = emphasiseInterestArea(bgNoMonitor);
        
        currentArg1 = img1Focused;
        bgArg2 = img2Focused;
    else
        currentArg1 = currentFrame;
        bgArg2 = bgFrame;
    end
    
    foregroundSubtracted = (abs(bgArg2(:,:,1) - currentArg1(:,:,1)) > SUBTRACTION_THRESHOLD) ...
     | (abs(bgArg2(:,:,2) - currentArg1(:,:,2)) > SUBTRACTION_THRESHOLD) ...
     | (abs(bgArg2(:,:,3) - currentArg1(:,:,3)) > SUBTRACTION_THRESHOLD);
    
    foregroundSubtracted = bwmorph(foregroundSubtracted, 'clean');
    I1 = bwareaopen(foregroundSubtracted, 100);
    I2 = bwmorph(I1, 'open', Inf);
    I3 = bwmorph(I2, 'dilate', 3);
    I4 = bwmorph(I3, 'majority', Inf);
    I5 = bwareaopen(I4, 100);
    I6 = bwmorph(I5, 'clean');
    foreground = I6;

end

