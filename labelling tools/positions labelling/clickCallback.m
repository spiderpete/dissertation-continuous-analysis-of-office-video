function clickCallback(objectHandle , ~)
%CLICKCALLBACK Summary of this function goes here
%   Detailed explanation goes here
    
    axesHandle  = get(objectHandle,'Parent');
    imgHandle = guidata(objectHandle);
    coordinates = get(axesHandle,'CurrentPoint');
    imgHandle.position = coordinates(1, 1:2);
    guidata(objectHandle, imgHandle);
end

