% Saved data has to be labeled with the day format '00'
autoPositionAnnotation_AdaptiveChange('01');
autoPositionAnnotation_AdaptiveChange('02');
autoPositionAnnotation_AdaptiveChange('03');
autoPositionAnnotation_AdaptiveChange('04');
autoPositionAnnotation_AdaptiveChange('05');
autoPositionAnnotation_AdaptiveChange('06');
autoPositionAnnotation_AdaptiveChange('07');
autoPositionAnnotation_AdaptiveChange('08');
autoPositionAnnotation_AdaptiveChange('09');
for day = 10 : 20
    autoPositionAnnotation_AdaptiveChange(num2str(day));
end
