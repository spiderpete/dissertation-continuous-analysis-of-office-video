function sigmas = paramEstimator(currFrame, history)
% Expects current frame and the whole of the history of Backgrounds to be
% already in chromaticity coordinates (chromRed, chromGreen and chromIntensity)

%function [means, sigmas, chromHistory] = paramEstimator(background, history)
    % initialise algorithm variables
    chromRed = 1;
    chromGreen = 2;
    
    %completeHist = cat(4, background, history);    
    %[frameRow, frameCol, frameChannel, frameNum] = size(completeHist);
    [frameRow, frameCol, ~, frameNum] = size(history);
    sigmas = zeros(frameRow, frameCol, 2);                                      % only need Red and Green Channels
    %means = zeros(frameRow, frameCol,  2);                                      % only need Red and Green Channels
    
    % % calculation of chromaticity coordinates
    %chromBackground = chromCoordsConversion(background);
    %chromCoordsBGFrame = zeros(frameRow,frameCol, frameChannel, frameNum);
    %for frameIndex = 1 : frameNum
    %    chromCoordsBGFrame(:,:,:, frameIndex) = chromCoordsConversion(completeHist(:,:,:, frameIndex));
    %end
    
    % estimate sigmas using robust estimator to avoid outliers
    %frameDiff = zeros(frameRow, frameCol, 2, frameNum - 1);
    frameDiff = zeros(frameRow, frameCol, 2, frameNum);
    %for frameIndex = 1 : frameNum - 1
    for frameIndex = 1 : frameNum
        %frameDiff(:,:, chromRed, frameIndex) = abs(chromCoordsBGFrame(:,:,
        %chromRed, frameIndex) - chromCoordsBGFrame(:,:, chromRed, frameIndex + 1));
        frameDiff(:,:, chromRed, frameIndex) = abs(currFrame(:,:, chromRed) - history(:,:, chromRed, frameIndex));
        %frameDiff(:,:, chromGreen, frameIndex) = abs(chromCoordsBGFrame(:,:, chromGreen, frameIndex) - chromCoordsBGFrame(:,:, chromGreen, frameIndex + 1));
        frameDiff(:,:, chromGreen, frameIndex) = abs(currFrame(:,:, chromGreen) - history(:,:, chromGreen, frameIndex));
    end
    sigmas = median(frameDiff, 4) ./ (0.68 * sqrt(2));
    
    % % estimate means using robust estimator to avoid outliers
    %means(:,:, chromRed) = median(chromCoordsBGFrame(:,:, chromRed,:), 4);
    %means(:,:, chromGreen) = median(chromCoordsBGFrame(:,:, chromGreen,:), 4);  
    
    %chromHistory = repmat(chromBackground, [1, 1, 1, frameNum - 1]);
    
end