%function clearedImage = interframeDiff(currentFrame, img1, img2)
function clearedImage = interframeDiff(img1, img2)
%IMAGEDIFFERENCE Summary of this function goes here
%   Detailed explanation goes here
    SUBTRACTION_THRESHOLD = 3; %5
    consecFrameDiff = zeros(size(img1));
    
    consecFrameDiff = rgb2gray(img2 - img1);
    
    % apply threshold
    foregroundThresholded = (abs(consecFrameDiff) > SUBTRACTION_THRESHOLD);
    
     % remove some of the noise
    clearedImage = bwmorph(foregroundThresholded, 'erode', 4);
    
    %figure(3);
    %imshow(~clearedImage);
    %title(['(Inter-frame) Mask of frame ', num2str(currentFrame)]);
    
end

