function frameValue = kernel(parameters, xFrame)
% kernel function adapted for optimal purposes of 'adaptiveChange'
    % initialise formula variables
    %meansParm = 1;
    %sigmasParam = 2;
    %means = parameters(:,:, meansParm);
    %sigmas = parameters(:,:, sigmasParam);
    sigmas = parameters;
    sigmas (sigmas == 0) = 0.00000001;
    
    normFactor = 1 ./ (sigmas .* sqrt(2 * pi));
    %variable = xFrame - means;
    variable = xFrame;
    variablePart = exp(-(variable .* variable) ./ (2 * sigmas .* sigmas));
    
    frameValue = variablePart .* normFactor;
    
end