function [AnyChange ,Iclean]= fcn_Difference2(Image_before,Image_after)
    diffThresh = 15;
%     normal_IBefore = fcn_Normalize(Image_before);
%     normal_IAfter = fcn_Normalize(Image_after);
%     Idiff = abs(normal_IBefore - normal_IAfter);
    Idiff = imabsdiff(Image_before,Image_after);
%     Idiff = imabsdiff(normal_IBefore,normal_IAfter);
    Ifound = (Idiff(:,:,1)>diffThresh) | (Idiff(:,:,2)>diffThresh) | (Idiff(:,:,3)>diffThresh);
    imask=strel('square',3);
    Iclean = imdilate(imdilate(imdilate(imerode(imerode(imerode(Ifound,imask),imask),imask),imask),imask),imask); % remove small pieces
    if nnz(Iclean) > 30000
        AnyChange = 1;
    else
        AnyChange = 0;
    end
end