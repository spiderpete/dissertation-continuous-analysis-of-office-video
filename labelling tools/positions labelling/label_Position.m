global person frame positions
% imlist
%load('ilist.mat');
NUMFRAMES = length(ilist);
% FIRSTFRAME= 110;
positions = zeros(1,NUMFRAMES,2);
colorset = ['r' 'g' 'b' 'k'];
person = 1;
frame = 1;
while(frame <= NUMFRAMES)
    [person,frame]
    temp_frame = frame+8;
    [Imdiff,Ibw] = fcn_Difference2(imread(ilist{frame}),imread(ilist{temp_frame}));
%     Imdiff
    if (Imdiff)
        [Imdiff5,Ibw] = fcn_Difference2(imread(ilist{frame}),imread(ilist{frame+3}));
        if (~Imdiff5)
            positions(person,frame:frame+3,1)=positions(person,frame,1);
            positions(person,frame:frame+3,2)=positions(person,frame,2);
            frame = frame +4;
            disp('+4')
            frame
        end
        while(frame <= temp_frame)
            frame
            I = imread(ilist{frame});
%             figure;
            imageHandle = imshow(I);
            set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
            pause
            positions(person,frame,:)
            hold on
            plot([positions(person,frame,1)],[positions(person,frame,2)],[colorset(person) '*-'])
            hold off
            pause(0.1)
            frame = frame +1;
        end
    else
        if (frame+8>NUMFRAMES)
            while(frame <= temp_frame)
                frame
                I = imread(ilist{frame});
                figure;
                imageHandle = imshow(I);
                set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
                pause
                positions(person,frame,:)
                hold on
                plot([positions(person,frame,1)],[positions(person,frame,2)],[colorset(person) '*-'])
                hold off
                pause(0.1)
                frame = frame +1; 
            end
%             break;
        else
            if frame>1
            positions(person,frame:frame+8,1)=positions(person,frame-1,1);
            positions(person,frame:frame+8,2)=positions(person,frame-1,2);
            end
            frame = frame +9;
%             positions(person,frame,:)
        end
    end
end


% I = imread(ilist{2});
% figure;
% imshow(ilist{2});
% hold on
% plot(360,640,'*-r');
% hold off