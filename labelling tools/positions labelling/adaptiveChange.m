function [foreground, history] = adaptiveChange(currentFrame, currentHistory, positions, DAY)
%function [foreground, history] = adaptiveChange(parameters, currentFrame, currentHistory, totalFrameNum)
% frameIndex makes history update procedure faster and more optimal
    
    % initialise algorithm variables
    chromRed = 1;
    chromGreen = 2;
    chromIntens = 3;

    % thresholds
    alpha = 0.6;
    beta = 1.4;
    probBG = 0.99;
    probImage_FG = 0.001;
    threshold = 0.005;

    % prepare structure dimensions and cardinalities
    [frameRow, frameCol, ~] = size(currentFrame);
    historySize = size(1, 4);
    history = currentHistory;
    foreground = zeros(frameRow, frameCol);
    
    % calculation of chromaticity coordinates
    chromCoords = chromCoordsConversion(currentFrame);
    
    % matched subset of history + pixel probabilities
    histBGMatchCount = zeros(frameRow, frameCol);
    bgProbAccumulate = zeros(frameRow, frameCol);
    
    % Estimate the sigma parameters from the difference between the current
    % frame and all the frames in the Background history array for the
    % kernel function
    sigmas = paramEstimator(chromCoords, currentHistory);
    
    for histframe = 1 : historySize
        % stage one: lightness similarity test
        
        % take ratio between current frame intensity and historical
        % background saturations
        if currentHistory(:,:, chromIntens, histframe) > 0
            ratio = chromCoords(:,:, chromIntens) ./ currentHistory(:,:, chromIntens, histframe);
        else
            %ratio = chromCoords(:,:, chromIntens) ./ 10;
        end
        
        % criterion for background satisfiability
        % Background = 0; Foreground = 1: for more intuitive printing:
        % Background(0) - Blackm Foreground(1) = White;
        histBGMatches = ratio < alpha | beta < ratio;
        % Add ~histBGMatches, as need to count the BG pixes, not FG
        histBGMatchCount = histBGMatchCount + (~histBGMatches);
        
        % start calculating P(x|Background), but for absolutely all pixels
        %bgProbsRedChannel = kernel(parameters(:,:, 1,:), double(currentFrame(:,:, 1)) - double(currentHistory(:,:, chromRed, histframe)));
        %bgProbsGreenChannel = kernel(parameters(:,:, 2,:), double(currentFrame(:,:, 2)) - double(currentHistory(:,:, chromGreen, histframe)));
        %bgProbsRedChannel = kernel(parameters(:,:, 1), double(currentFrame(:,:, 1)) - double(currentHistory(:,:, chromRed, histframe)));
        bgProbsRedChannel = kernel(sigmas(:,:, chromRed), double(currentFrame(:,:, chromRed)) - double(currentHistory(:,:, chromRed, histframe)));
        %bgProbsGreenChannel = kernel(parameters(:,:, 2), double(currentFrame(:,:, 2)) - double(currentHistory(:,:, chromGreen, histframe)));
        bgProbsGreenChannel = kernel(sigmas(:,:, chromGreen), double(currentFrame(:,:, chromGreen)) - double(currentHistory(:,:, chromGreen, histframe)));
        
        bgProbInCurrentHist = bgProbsRedChannel .* bgProbsGreenChannel;
        
        % background probabilities of pixels with foreground like ratios are zero
        % only accummulate the probabilities of those which passed the
        % ratios test and scap the rest
        bgProbAccumulate = bgProbAccumulate + bgProbInCurrentHist .* double(~histBGMatches);
        
    end  
    
    % stage two: bayes probabilities
    
    % save the count of matched history backgrounds for every pixel
    histBGMatchCountForProb = histBGMatchCount;
    
    % calculate P(x|Background)
    % Avoid division by 0 for pixels which have not been found in the
    % background history
    histBGMatchCountForProb (histBGMatchCountForProb == 0) = 1;  
    probImage_BG = bgProbAccumulate ./ histBGMatchCountForProb;
    
    % Scrap away all foreground pixels for background pixels probability
    % calculation
    probImage_BG = probImage_BG .* imbinarize(histBGMatchCount);
    
    % Bayes Equation
    probBG_Image = (probImage_BG * probBG) ./ ((probImage_BG * probBG) + probImage_FG * (1 - probBG));
    
    %foreground = probBG_Image;
    foreground(probBG_Image > threshold) = 1;
    % Take both FG pixels from stage one and those from stage 2
    fgStage1 = ~imbinarize(histBGMatchCount);
    foregroundCombined = foreground + fgStage1;
    %foreground = ~histBGMatchCount;
    
    % Remove monitor and windows
    foreground = focusImage(foregroundCombined, true, positions, DAY);
    
    % Clean image
    foreground = bwmorph(foreground, 'clean');
    foreground = bwmorph(foreground, 'open', Inf);
    foreground = bwmorph(foreground, 'dilate', 3);
    foreground = bwmorph(foreground, 'fill', Inf);
    foreground = bwareaopen(foreground, 100);
    foreground = bwmorph(foreground, 'clean');
    
    % Update the background pixels of all the history frames one by one
    background = ~foreground;
    background3D = cat(3, background, background, background);
    for hist = 1 : historySize - 1
        history(:,:,:, hist) = history(:,:,:, hist + 1) .* background3D + history(:,:,:, hist) .* ~background3D;
    end
    % Update the background pixels of the newest history frame
    bgAreas = chromCoords .* background3D;
    history(:,:,:, historySize) = bgAreas .* background3D + history(:,:,:, historySize) .* ~background3D;
    
    
    
    %%figure(2);
    %imshow(~background);
    %title(['(Adaptive BG) Mask of frame ', num2str(currentFrame)]);
    
end