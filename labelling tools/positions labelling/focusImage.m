function imgArg = focusImage(img, isFocused, centreOfInterest, DAY)
%IMAGEDIFFERENCE Summary of this function goes here
%   Detailed explanation goes here
    if isFocused
        % Blank out Monitor pixels
        img1NoMonitorAndWindow = ignoreMonitorAndWindow(img, DAY);
        
        % Foxus only on possible area of movement
        imgFocused = emphasiseInterestArea(img1NoMonitorAndWindow, centreOfInterest, DAY);
        
        imgArg = imgFocused;
    else
        % Foxus only on possible area of movement
        imgFocused = emphasiseInterestArea(img, centreOfInterest, DAY);
        
        imgArg = imgFocused;
    end
end

