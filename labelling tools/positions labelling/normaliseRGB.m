function normalImage = normaliseRGB(image)
  % convert to normalised RGB
  imageSum = (image(:,:,1) + image(:,:,2) + image(:,:,3));
  normalImage(:,:,1) = image(:,:,1) ./ imageSum; 
  normalImage(:,:,2) = image(:,:,2) ./ imageSum;
  normalImage(:,:,3) = image(:,:,3) ./ imageSum;
end