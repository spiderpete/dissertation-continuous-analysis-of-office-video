%% Initialisation of algorithm variables
% Time execution
tic

% Custamisable parameters
DAY_NUM = '01';
PREVIEW_LOC = true;
START_FRAME = 1;
IS_FOCUSED = true;

% Stationary parameters
DAY = ['Day', DAY_NUM];
DIR = './Data/OFFICE/';
CHANGE_THRESHOLD = 1000;
RATE_OF_CHANGE_TRESHOLD = 700;

dayFile = dayFiles.(DAY).file;
Path = [DIR, dayFile, ''];
fileList = generateFileList(Path);
numOfFrames = length(fileList);

%% Preparation of anotation holding structrure

% Structures to hold date anotation data
% Properties include: [column location, row location, behavior]
% always starting with the main person tracked
% day_N{1}          list of frames
% day_N{1}{x}       name of file 'x'
% day_N{2}          list of all frames's properties' cells
% day_N{2}{x}       list of frames' properties for frame 'x' for all people
% day_N{2}{x}(y)    list of properties for frame 'x' for person 'y'
positions = zeros(numOfFrames, 2);
day_01 = cell(1, 2);
day_01{1} = cell(size(fileList));
day_01{2} = cell(size(fileList));
for days = 1 : numOfFrames
    day_01{2}{days} = zeros(1, 4);
end

% Annotate the first frame
frame = imread(fileList{START_FRAME});

figure(1); % Current Full Coloured Frame
imgHandle = imshow(frame);
title(['Frame number ', num2str(START_FRAME)]);
set(imgHandle, 'ButtonDownFcn',@clickCallback);
pause;
dataPosition = guidata(imgHandle);
positions(START_FRAME,:) = dataPosition.position;
hold on;
plot(positions(START_FRAME, 1), positions(START_FRAME, 2), 'b*');
hold off;
pause(1);

% Save Name of frame and location properties for first frame
day_01{1}{START_FRAME} = fileList{START_FRAME};
day_01{2}{START_FRAME}(1,1:2) = positions(START_FRAME,:);

%% Loop through whole video and anotate

changeRatePrev = 0;
% First frame has to be tagged manually
for currentFrame = (START_FRAME + 1) : numOfFrames
    % Save Name of frame
    day_01{1}{currentFrame} = fileList{currentFrame};
    
    % Load the images
    imgCurrent = imread(fileList{currentFrame});
    imgPrev = imread(fileList{currentFrame - 1});
    
    % 'Focus' and 'clear' images - areas of interest only
    imgCurrent = focusImage(imgCurrent, IS_FOCUSED, positions(currentFrame - 1,:));
    imgPrev = focusImage(imgPrev, IS_FOCUSED, positions(currentFrame - 1,:));

    % Interframe Difference to find changes/foreground
    imgDiffGrayFocused = interframeDiff(imgCurrent, imgPrev);
    
    figure(2); % Current Detection in black and white
    imshow(imgDiffGrayFocused);
    title(['Frame number ', num2str(currentFrame)]);
    hold on;
    pause(0.1)
    
    
    % To reduce the impact of noise, consider the rate of change of image
    % differencing
    changeRateCurrent = nnz(imgDiffGrayFocused);
    rateOfChangeRate = abs(changeRateCurrent - changeRatePrev);
    % No rate of change for the image differencing in the second frame
    if (currentFrame == START_FRAME + 1)
        rateOfChangeRate = 0;
    end
    %{
    disp('========================================');
    disp(['Frame num is ', num2str(currentFrame)]);
    disp(['Change rate is: ', num2str(changeRateCurrent)])
    disp(['Rate of change is: ', num2str(rateOfChangeRate)]);
    %}
    
    if (rateOfChangeRate > RATE_OF_CHANGE_TRESHOLD)
        % If no detection, consider the whole frame
        if changeRateCurrent == 0
            IS_FOCUSED = false;
            imgDiffGrayFocused = interframeDiff(imgCurrent, imgPrev, IS_FOCUSED, positions(currentFrame - 1,:));
            IS_FOCUSED = true;
        end
        
        % Locate the coordinates of the largest change aka the person
        largestBlob = bwareafilt(imgDiffGrayFocused, 1);
        props = regionprops(largestBlob, 'centroid');
        centroids = cat(1, props.Centroid);

        % Highlingt lack of any detections
        if (max(max(largestBlob)) == 0)
            %positions(currentFrame,:) = [-1 -1];
            imshow(imgDiffGrayFocused);
            title(['Frame number ', num2str(currentFrame)]);
            %pause;
            positions(currentFrame,:) = positions(currentFrame - 1,:);
            disp(['Failed at frame ', num2str(currentFrame)]);
        else
            positions(currentFrame,:) = round(centroids);
        end
        % Show result of localisation
        if PREVIEW_LOC
            figure(1); % Current Full Coloured Frame
            imshow(imgCurrent);
            title(['Frame number ', num2str(currentFrame)]);
            hold on;
            plot(positions(currentFrame, 1), positions(currentFrame, 2), 'r*');
            disp('Re-calced!');
            hold off;
            %{
            figure(2); % Current Detection in black and white
            plot(positions(currentFrame, 1), positions(currentFrame, 2), 'r*');
            hold off;
            %}
            pause(0.1);
        end
    else
        % If not much change, just propagate the properties
        positions(currentFrame,:) = positions(currentFrame - 1,:);
        % Show result of localisation
        if PREVIEW_LOC
            figure(1); % Current Full Coloured Frame
            imshow(imgCurrent);
            title(['Frame number ', num2str(currentFrame)]);
            hold on;
            plot(positions(currentFrame, 1), positions(currentFrame, 2), 'g*');
            disp('Propagated!');
            hold off;
            %{
            figure(2); % Current Detection in black and white
            plot(positions(currentFrame, 1), positions(currentFrame, 2), 'g*');
            hold off;
            %}
            pause(0.1);
        end
    end
    
    % Save lication properties for the current frame
    day_01{2}{currentFrame}(1,1:2) = positions(currentFrame,:);

    changeRatePrev = changeRateCurrent;
end

%% Save all information and time

% Save all the locations and names
save([DAY, '.mat'], 'day_01'); 

%Time execution
toc
elapsedTime = toc;
%disp(['Processing time is: ', num2str(elapsedTime)]);