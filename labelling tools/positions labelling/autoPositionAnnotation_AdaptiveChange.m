function autoPositionAnnotation_AdaptiveChange(dayNum)

    %% Initialisation of algorithm variables
    % Time execution
    tic

    % Custamisable parameters
    DAY_NUM = dayNum; %'01';
    PREVIEW_LOC = false;
    IS_FOCUSED = true;
    INIT_FRAME = 1;
    NUM_OF_BG_HIST = 3;
    EXECUTION_ON_WINDOWS = false;
    DISPLAY_FROM = 2520;

    % Stationary parameters
    if ~EXECUTION_ON_WINDOWS
        DAY = ['Day', DAY_NUM];
        DIR = './Data/OFFICE/';
        SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Auto/'];
    else
        DAY = ['Day', DAY_NUM];
        DIR = '.\Data\OFFICE\';
        SAVE_DIR = [DIR, 'Ground Truths\GT\GT - Auto\'];
    end
    
    CHANGE_THRESHOLD = 1000;
    RATE_OF_CHANGE_TRESHOLD = 700;
    INTEREST_AREA_RADIUS = dayFiles.(DAY).areaInterestParameter;

    dayFile = dayFiles.(DAY).file;
    if ~EXECUTION_ON_WINDOWS
        path = [DIR, dayFile, '/inspacecam163'];
    else
        path = [DIR, dayFile, '\inspacecam163'];
    end
    fileList = generateFileList(path, EXECUTION_ON_WINDOWS);
    numOfFrames = length(fileList);
    
    startFrameName = dayFiles.(DAY).startFrameSubject;  % Fastforward to the frame 
                                                        % where the subject appears 
                                                        % on stage
    matches = strfind(fileList, startFrameName);
    START_FRAME = find(not(cellfun('isempty', matches)));
    endFrameName = dayFiles.(DAY).endFrameSubject;      % Stop at the frame where
                                                        % the subject disappears 
                                                        % for good from stage
    matches = strfind(fileList, endFrameName);
    END_FRAME = find(not(cellfun('isempty', matches)));
    finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                        % of the video so the frame
                                                        % names can be annotated
    matches = strfind(fileList, finishFrameName);
    FINISH_FRAME = find(not(cellfun('isempty', matches)));

    % Path to Background History for Adaptive Background Change algorithm
    if ~EXECUTION_ON_WINDOWS
        pathsToBGs = [DIR, dayFile, '/initBG'];
    else
        pathsToBGs = [DIR, dayFile, '\initBG'];
    end

    % Auxiliary notes array
    notes = cell(numOfFrames, 0);

    %% Preparation of anotation holding structrure

    % Structures to hold date anotation data
    % Properties include: [column location, row location, behavior]
    % always starting with the main person tracked
    % day_N{1}          list of frames
    % day_N{1}{x}       name of file 'x'
    % day_N{2}          list of all frames's properties' cells
    % day_N{2}{x}       list of frames' properties for frame 'x' for all people
    % day_N{2}{x}(y,:)  list of properties for frame 'x' for person 'y'
    % day_N{2}{x}(y,1)  col position of person 'y' for frame 'x'
    % day_N{2}{x}(y,2)  row position of person 'y' for frame 'x'
    % day_N{2}{x}(y,3)  behaviour of person 'y' for frame 'x'
    positions = zeros(numOfFrames, 2);
    positionsInterFrame = zeros(numOfFrames, 2);
    positionsAdaptive = zeros(numOfFrames, 2);
    day_01 = cell(1, 2);
    day_01{1} = cell(size(fileList));
    day_01{2} = cell(size(fileList));
    for days = 1 : numOfFrames
        % Postions: day_N{2}{x}(y,1) and day_N{2}{x}(y,2) default to 0 in the
        % default situation when the person is not present
        day_01{2}{days} = zeros(1, 3);
    end

    % Annotate the first frame
    frame = imread(fileList{START_FRAME});

    figure(1); % Current Full Coloured Frame
    imgHandle = imshow(frame);
    title(['Frame number ', num2str(START_FRAME)]);
    set(imgHandle, 'ButtonDownFcn',@clickCallback);
    pause;
    dataPosition = guidata(imgHandle);
    positions(START_FRAME,:) = dataPosition.position;
    positionsInterFrame(START_FRAME,:) = dataPosition.position;
    positionsAdaptive(START_FRAME,:) = dataPosition.position;
    hold on;
    plot(positions(START_FRAME, 1), positions(START_FRAME, 2), 'b*');
    hold off;
    pause(1);

    % Save Name of frame and location properties for first frame
    day_01{1}{START_FRAME} = fileList{START_FRAME};
    day_01{2}{START_FRAME}(1,1:2) = positions(START_FRAME,:);

    %% Adaptive Background Preparation

    % If the video of the day does not have sample background frames at its
    % beginning, take the relative example from the initBG folder and
    % synthesise more up to the required size for the history for Adaptive
    % Background Change algorithm
    if ~dayFiles.(DAY).bgHistPresent
       synthesisePhoto(pathsToBGs, NUM_OF_BG_HIST, EXECUTION_ON_WINDOWS); 
    end

    % Prepare history of backgrounds for Adaptive Background Change
    fileBGList = generateFileList(pathsToBGs, EXECUTION_ON_WINDOWS);
    numOfBGs = length(fileBGList);
    history = zeros([size(frame), numOfBGs]);   
    %sampleHistory = zeros([size(frame), numOfBGs]);    
    for currentBGframe = 1 : numOfBGs
        bgImage = (imread(fileBGList{currentBGframe}));
        focusedBGImage = ignoreMonitorAndWindow(bgImage, DAY);
        history(:,:,:, currentBGframe) = chromCoordsConversion(focusedBGImage);
    end

    %% Loop through whole video and anotate

    % If subject does not appear at the beginning of the video, before
    % fastforwarding to the frame of the subject, annotate information
    % structure up to that frame
    subjectAppearAtStart = strcmp(dayFiles.(DAY).startFrame, dayFiles.(DAY).startFrameSubject);
    if ~subjectAppearAtStart
        for currentFrame = INIT_FRAME : START_FRAME - 1
            day_01{1}{currentFrame} = fileList{currentFrame}; 
        end
    end

    % If the subject does not appear to the end of the video, after
    % annotating the positions in the frames up to the disappearance of
    % the subject, keep going until the end of the video so the names of
    % the frames can be added to the annotation
    subjectAppearToEnd = strcmp(dayFiles.(DAY).endFrame, dayFiles.(DAY).endFrameSubject);
    if ~subjectAppearToEnd
        for currentFrame = END_FRAME + 1 : FINISH_FRAME
            day_01{1}{currentFrame} = fileList{currentFrame}; 
        end
    end

    changeRatePrev = 0;
    % First frame has to be tagged manually - Used, not anymore!
    for currentFrame = (START_FRAME + 1) : END_FRAME
        % Save Name of frame
        day_01{1}{currentFrame} = fileList{currentFrame};

        % Load the images
        imgCurrent = imread(fileList{currentFrame});
        imgPrev = imread(fileList{currentFrame - 1});

        % 'Focus' and 'clear' images - areas of interest only
        imgCurrentInterFrame = focusImage(imgCurrent, IS_FOCUSED, positionsInterFrame(currentFrame - 1,:), DAY);
        imgPrevInterFrame = focusImage(imgPrev, IS_FOCUSED, positionsInterFrame(currentFrame - 1,:), DAY);

        
        
        figure(2); % Current Detection in black and white
        imshow(imgCurrentInterFrame);
        title(['Frame number ', num2str(currentFrame)]);
        hold on;
        pause;
        
        
        % Interframe Difference to find changes/foreground
        imgDiffGrayFocused = interframeDiff(imgCurrentInterFrame, imgPrevInterFrame);

        % 'Focus' and 'clear' images - areas of interest only
        imgCurrentAdaptiveBG = ignoreMonitorAndWindow(imgCurrent, DAY);

        % Adaptive Background to find changes/foreground
        [imgAdaptiveBGFocused, history] = adaptiveChange(imgCurrentAdaptiveBG, history, positionsAdaptive(currentFrame - 1,:), DAY);

        % Locate the coordinates of the largest change aka the person
        largestBlobAdaptive = bwareafilt(imgAdaptiveBGFocused, 1);
        propsAdaptive = regionprops(largestBlobAdaptive, 'centroid');
        centroidsAdaptive = cat(1, propsAdaptive.Centroid);

        % Highlingt lack of any detections
        if (max(max(largestBlobAdaptive)) == 0)
            imshow(imgAdaptiveBGFocused);
            title(['Frame number ', num2str(currentFrame)]);
            %pause;
            positionsAdaptive(currentFrame,:) = positionsAdaptive(currentFrame - 1,:);
            disp(['Adaptive Failed at frame ', num2str(currentFrame)]);
            notes{currentFrame} = ['Adaptive Failed at frame ', num2str(currentFrame)];
        else
            positionsAdaptive(currentFrame,:) = round(centroidsAdaptive);
        end

        %{
        figure(2); % Current Detection in black and white
        imshow(imgAdaptiveBGFocused);
        title(['Frame number ', num2str(currentFrame)]);
        hold on;
        pause(0.1)
        %}

        % To reduce the impact of noise, consider the rate of change of image
        % differencing
        changeRateCurrent = nnz(imgDiffGrayFocused);
        rateOfChangeRate = abs(changeRateCurrent - changeRatePrev);
        % No rate of change for the image differencing in the second frame
        if (currentFrame == START_FRAME + 1)
            rateOfChangeRate = 0;
        end
        %{
        disp('========================================');
        disp(['Frame num is ', num2str(currentFrame)]);
        disp(['Change rate is: ', num2str(changeRateCurrent)])
        disp(['Rate of change is: ', num2str(rateOfChangeRate)]);
        %}
        if rem(currentFrame, 100) == 0
            disp(['Frame num is ', num2str(currentFrame)]);
        end

        if (rateOfChangeRate > RATE_OF_CHANGE_TRESHOLD)
            % If no detection, consider the whole frame
            if changeRateCurrent == 0
                imgDiffGrayFocused = interframeDiff(imgCurrent, imgPrev);
            end

            % Locate the coordinates of the largest change aka the person
            largestBlob = bwareafilt(imgDiffGrayFocused, 1);
            props = regionprops(largestBlob, 'centroid');
            centroids = cat(1, props.Centroid);

            % Highlingt lack of any detections
            if (max(max(largestBlob)) == 0)
                %positions(currentFrame,:) = [-1 -1];
                imshow(imgDiffGrayFocused);
                title(['Frame number ', num2str(currentFrame)]);
                %pause;
                positionsInterFrame(currentFrame,:) = positionsInterFrame(currentFrame - 1,:);
                disp(['Failed at frame ', num2str(currentFrame)]);
                notes{currentFrame} = ['Failed at frame ', num2str(currentFrame)];
            else
                positionsInterFrame(currentFrame,:) = round(centroids);
            end
            % Show result of localisation
            if PREVIEW_LOC
                figure(1); % Current Full Coloured Frame
                imshow(imgCurrent);
                title(['Frame number ', num2str(currentFrame)]);
                hold on;
                plot(positionsInterFrame(currentFrame, 1), positionsInterFrame(currentFrame, 2), 'r*');
                plot(positionsAdaptive(currentFrame, 1), positionsAdaptive(currentFrame, 2), 'ro');
                disp('Re-calced!');
                hold off;
                %{
                figure(2); % Current Detection in black and white
                plot(positions(currentFrame, 1), positions(currentFrame, 2), 'r*');
                hold off;
                %}
                pause(0.1);
            end
        else
            % If not much change, just propagate the properties
            positionsInterFrame(currentFrame,:) = positionsInterFrame(currentFrame - 1,:);
            % Show result of localisation
            if PREVIEW_LOC
                figure(1); % Current Full Coloured Frame
                imshow(imgCurrent);
                title(['Frame number ', num2str(currentFrame)]);
                hold on;
                plot(positionsInterFrame(currentFrame, 1), positionsInterFrame(currentFrame, 2), 'g*');
                plot(positionsAdaptive(currentFrame, 1), positionsAdaptive(currentFrame, 2), 'go');
                disp('Propagated!');
                hold off;
                %{
                figure(2); % Current Detection in black and white
                plot(positions(currentFrame, 1), positions(currentFrame, 2), 'g*');
                hold off;
                %}
                pause(0.1);
            end
        end

        % Save lication properties for the current frame
        positions(currentFrame,:) = (positionsInterFrame(currentFrame,:) + positionsAdaptive(currentFrame,:)) / 2;
        day_01{2}{currentFrame}(1,1:2) = positions(currentFrame,:);
        posDist = pdist2(positionsInterFrame(currentFrame,:), positionsAdaptive(currentFrame,:));

        % Take a note when algorithms perform too differently for subsequent
        % validation and data curation
        if (posDist > INTEREST_AREA_RADIUS/2)
            notes{currentFrame} = ['Positions too far apart: ', ...
                                    'xInter = ', num2str(positionsInterFrame(currentFrame,1)), ', ', ...
                                    'yInter = ', num2str(positionsInterFrame(currentFrame,2)), ' | ', ...
                                    'xAdapt = ', num2str(positionsAdaptive(currentFrame,1)), ', ', ...
                                    'yAdapt = ', num2str(positionsAdaptive(currentFrame,2)), ' | ', ...
                                    'total = ', num2str(posDist)];
        end
        if PREVIEW_LOC %&& currentFrame >= DISPLAY_FROM && rem(currentFrame, 9) == 0
            figure(1);
            hold on;
            plot(positions(currentFrame, 1), positions(currentFrame, 2), 'b+');
            hold off;
            
            %figure(1);
            %imshow(imgCurrent);
            %title(['Frame number ', num2str(currentFrame)]);
            %hold on;
            %plot(day_01{2}{currentFrame}(1,1), day_01{2}{currentFrame}(1,2), 'r*');
            %rectangle('Position', notes{currentFrame}(1,:), 'EdgeColor', 'r', 'LineWidth', 3);
        end
        pause;

        changeRatePrev = changeRateCurrent;
    end

    %% Save all information and time

    % Save all the locations and names
    save([SAVE_DIR, DAY, '.mat'], 'day_01'); 
    save([SAVE_DIR, DAY, '_notes', '.mat'], 'notes');

    %Time execution
    toc
    elapsedTime = toc;
    clear;
    %disp(['Processing time is: ', num2str(elapsedTime)]);

end