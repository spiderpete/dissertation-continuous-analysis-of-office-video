function imageOutput = ignoreMonitorAndWindow(imageInput, DAY)
%IGNOREMONITOR Summary of this function goes here
%   Detailed explanation goes here
    % Coordinates of top left and bottom right points of monitor
    Y_TOP_LEFT_CORNER = dayFiles.(DAY).ignoreArea1(1);
    Y_LOW_RIGHT_CORNER = dayFiles.(DAY).ignoreArea1(2);
    X_TOP_LEFT_CORNER = dayFiles.(DAY).ignoreArea1(3);
    X_LOW_RIGHT_CORNER = dayFiles.(DAY).ignoreArea1(4);
    
    % Coordinates of top left and bottom right points of bigger window
    Y_TOP_LEFT_CORNER1  = dayFiles.(DAY).ignoreArea2(1);
    Y_LOW_RIGHT_CORNER1 = dayFiles.(DAY).ignoreArea2(2);
    X_TOP_LEFT_CORNER1  = dayFiles.(DAY).ignoreArea2(3);
    X_LOW_RIGHT_CORNER1 = dayFiles.(DAY).ignoreArea2(4);
    
    % Coordinates of top left and bottom right points of smaller window
    Y_TOP_LEFT_CORNER2  = dayFiles.(DAY).ignoreArea3(1);
    Y_LOW_RIGHT_CORNER2 = dayFiles.(DAY).ignoreArea3(2);
    X_TOP_LEFT_CORNER2  = dayFiles.(DAY).ignoreArea3(3);
    X_LOW_RIGHT_CORNER2 = dayFiles.(DAY).ignoreArea3(4);
    
    % Ignore deadzone (Videos 1-12 top 100pxs)
    Y_TOP_LEFT_CORNER3  = dayFiles.(DAY).ignoreArea4(1);
    Y_LOW_RIGHT_CORNER3 = dayFiles.(DAY).ignoreArea4(2);
    X_TOP_LEFT_CORNER3  = dayFiles.(DAY).ignoreArea4(3);
    X_LOW_RIGHT_CORNER3 = dayFiles.(DAY).ignoreArea4(4);
    
    
    maskImg = ones(size(imageInput));
    maskImg(Y_TOP_LEFT_CORNER : Y_LOW_RIGHT_CORNER, X_TOP_LEFT_CORNER : X_LOW_RIGHT_CORNER,:) = 0;
    maskImg(Y_TOP_LEFT_CORNER1 : Y_LOW_RIGHT_CORNER1, X_TOP_LEFT_CORNER1 : X_LOW_RIGHT_CORNER1,:) = 0;
    maskImg(Y_TOP_LEFT_CORNER2 : Y_LOW_RIGHT_CORNER2, X_TOP_LEFT_CORNER2 : X_LOW_RIGHT_CORNER2,:) = 0;
    maskImg(Y_TOP_LEFT_CORNER3 : Y_LOW_RIGHT_CORNER3, X_TOP_LEFT_CORNER3 : X_LOW_RIGHT_CORNER3,:) = 0;
    
    if isa(imageInput, 'uint8')
        imageOutput = imageInput .* uint8(maskImg);
    elseif isa(imageInput, 'double')
        imageOutput = imageInput .* maskImg;
    else
        imageOutput = imageInput;
        disp('ERROR: Unexpected type for input to ignoreMonitorAndWindow.m');
    end
    
end

