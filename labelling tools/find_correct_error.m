% %% 1 get erro list behavior =1 but positions != 0
get_behave_list = find(behavior3==1);
get_positions = positions3;
j =1;

for i=1:length(get_behave_list)
    if get_positions(1,get_behave_list(i),1)>0
        new_ilist_num(j) = get_behave_list(i);
        j = j+1;
    end
end

%% get erro list, postion =0 but behavior != 1
get_behave_list = find(mod1_behavior3~=1);
get_positions = positions3;
j =1;

for i=1:length(get_behave_list)
    if get_positions(1,get_behave_list(i),1)==0
        new_ilist_num(j) = get_behave_list(i);
        j = j+1;
    end
end

%% 3 test on new list to check if there is error


for i=1:length(new_ilist)
% for i=6500:7000
    I = imread(get_ilist{new_ilist_num(i)});
    imshow(I);
    hold on
    plot(get_positions(1,new_ilist_num(i),1),get_positions(1,new_ilist_num(i),2),'*');
    hold off
    get_behavior(new_ilist_num(i))
        pause(0.01);
    i
%     pause(0.05);
%     pause(0.3);
% pause;
end

%% 2 get new list // get new positions
get_behavior = mod1_behavior3; 
get_ilist = ilist3;
 new_ilist = get_ilist(new_ilist_num);
 new_postions = get_positions(1,new_ilist_num,:);

 
 %% 4
%  go run the behaviors.m
% or go run the postions.m
% change 'ilist' & 'behavior'

 %% 5 test on new list (dont use this one)
 for i=1:length(new_ilist)
% for i=975:1000
     imshow(new_ilist{i});
     hold on
     plot(new_postions(1,i,1),new_postions(1,i,2),'*');
     hold off
     pause(0.01);
%      pause(0.2);
     i
     behavior(i)
%      pause;
 end
%% 6 rewrite the bahavior
behavior3(new_ilist_num) = behavior;
mod1_behavior3 = behavior3;

%% modify by hand 
behavior(1:352) = 5;

behavior(19:6657) = 10;

%% clear var
clearvars get_behave_list get_positions get_behavior get_ilist i behavior
%% clear and load data and variable
clear
clc
load('use_to_correct_behaviors.mat')