function edgeMask = extractEdges_EdgeDetect(image)
%EXTRACTEDGES Summary of this function goes here
%   Detailed explanation goes here

    %threshR = [0.0250, 0.0625];
    %threshG = [0.0250, 0.0625];
    %threshB = [0.0188, 0.0469];

    threshR = [0.0800, 0.2525];
    threshG = [0.0800, 0.2525];
    threshB = [0.1344, 0.2069];

    % Previous Frame - used as Background
    edgesImgR = edge(image(:,:,1), 'Canny', threshR);
    edgesImgB = edge(image(:,:,2), 'Canny', threshB);
    edgesImgG = edge(image(:,:,3), 'Canny', threshG);

    edgeMask = edgesImgR | edgesImgB | edgesImgG;

end

