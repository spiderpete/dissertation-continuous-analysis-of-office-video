function morphedImage = morphology_Edge( image )
%MORPHOLOGY Summary of this function goes here
%   Detailed explanation goes here
    SMALL_AREAS = 2000;

    morphedImage = medfilt2(image);
    
    se = strel('disk',20);
    for i = 1 : 10
        morphedImage = imclose(morphedImage, se);
    end
    
    morphedImage = bwmorph(morphedImage,'bridge');
    morphedImage = bwmorph(morphedImage,'diag');
    morphedImage = bwmorph(morphedImage,'majority');
    morphedImage = bwmorph(morphedImage,'thicken', 5);
    morphedImage = bwmorph(morphedImage,'bridge');
    
    morphedImage = imfill(morphedImage, 'holes');
    
    morphedImage = bwareaopen(morphedImage, SMALL_AREAS);
    
end
