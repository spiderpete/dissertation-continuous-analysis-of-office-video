function edgeDetection(dayNum)
    %dayNum = '03';
    disp(['Day: ', num2str(dayNum)]);
    
    %% Initialisation of algorithm variables
    % Time execution
    tic

    % Custamisable parameters
    DAY_NUM = dayNum; %'01';
    PREVIEW_LOC = false;
    INIT_FRAME = 1;
    NUM_OF_BG_HIST = 1;
    EXECUTION_ON_WINDOWS = false;
    TIME_WINDOW_INTERFRAME = 5;
    MAX_NUM_OF_PEOPLE_EXPECTED = 3;
    PERSON_MIN_AREA = 4000;
    DISPLAY_FROM = 505;

    % Stationary parameters
    if ~EXECUTION_ON_WINDOWS
        DAY = ['Day', DAY_NUM];
        DIR = './Data/OFFICE/';
        SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Edges/'];
    else
        DAY = ['Day', DAY_NUM];
        DIR = '.\Data\OFFICE\';
        SAVE_DIR = [DIR, 'Ground Truths\GT\GT - Edges\'];
    end
    
    CHANGE_THRESHOLD = 0;
    dayFile = dayFiles.(DAY).file;
    if ~EXECUTION_ON_WINDOWS
        path = [DIR, dayFile, '/inspacecam163'];
    else
        path = [DIR, dayFile, '\inspacecam163'];
    end
    
    fileList = generateFileList(path, EXECUTION_ON_WINDOWS);
    numOfFrames = length(fileList);
    
    startFrameName = dayFiles.(DAY).startFrameSubject;  % Fastforward to the frame 
                                                        % where the subject appears 
                                                        % on stage
    matches = strfind(fileList, startFrameName);
    START_FRAME = find(not(cellfun('isempty', matches)));
    endFrameName = dayFiles.(DAY).endFrameSubject;      % Stop at the frame where
                                                        % the subject disappears 
                                                        % for good from stage
    matches = strfind(fileList, endFrameName);
    END_FRAME = find(not(cellfun('isempty', matches)));
    finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                        % of the video so the frame
                                                        % names can be annotated
    matches = strfind(fileList, finishFrameName);
    FINISH_FRAME = find(not(cellfun('isempty', matches)));

    % Path to Background History for Adaptive Background Change algorithm
    if ~EXECUTION_ON_WINDOWS
        pathsToBGs = [DIR, dayFile, '/initBG'];
    else
        pathsToBGs = [DIR, dayFile, '\initBG'];
    end
    
    %% Preparation of anotation holding structrure

    % Structures to hold date anotation data
    % Properties include: [column location, row location, behavior]
    % always starting with the main person tracked
    % day_N{1}          list of frames
    % day_N{1}{x}       name of file 'x'
    % day_N{2}          list of all frames's properties' cells
    % day_N{2}{x}       list of frames' properties for frame 'x' for all people
    % day_N{2}{x}(y,:)  list of properties for frame 'x' for person 'y'
    % day_N{2}{x}(y,1)  col position of person 'y' for frame 'x'
    % day_N{2}{x}(y,2)  row position of person 'y' for frame 'x'
    % day_N{2}{x}(y,3)  behaviour of person 'y' for frame 'x'
    positions = zeros(numOfFrames, 2);
    positionsEdge = zeros(numOfFrames, 2);
    day_01 = cell(1, 2);
    day_01{1} = cell(size(fileList));
    day_01{2} = cell(size(fileList));
    for days = 1 : numOfFrames
        % Postions: day_N{2}{x}(y,1) and day_N{2}{x}(y,2) default to 0 in the
        % default situation when the person is not present
        day_01{2}{days} = zeros(1, 3);
        
        % Auxiliary notes array
        notes = cell(numOfFrames, 0);
    end

    %% Edge Detection Preparation

    % If the video of the day does not have sample background frames at its
    % beginning, take the relative example from the initBG folder and
    % synthesise more up to the required size for the history for Adaptive
    % Background Change algorithm
    if ~dayFiles.(DAY).bgHistPresent
       synthesisePhoto(pathsToBGs, NUM_OF_BG_HIST, EXECUTION_ON_WINDOWS); 
    end

    % Prepare backgrounds for Background Subtraction
    fileBGList = generateFileList(pathsToBGs, EXECUTION_ON_WINDOWS);
    numOfBGs = length(fileBGList);
    for currentBGframe = 1 : numOfBGs
        bgImage = (imread(fileBGList{currentBGframe}));
        focusedBGImage = ignoreMonitorAndWindow(bgImage, DAY);
    end
    
    %% Loop through whole video and anotate

    % If subject does not appear at the beginning of the video, before
    % fastforwarding to the frame of the subject, annotate information
    % structure up to that frame
    subjectAppearAtStart = strcmp(dayFiles.(DAY).startFrame, dayFiles.(DAY).startFrameSubject);
    if ~subjectAppearAtStart
        for currentFrame = INIT_FRAME : START_FRAME - 1
            day_01{1}{currentFrame} = fileList{currentFrame}; 
            for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                notes{currentFrame}(person,:) = zeros(1, 4);
            end
        end
    end

    % If the subject does not appear to the end of the video, after
    % annotating the positions in the frames up to the disappearance of
    % the subject, keep going until the end of the video so the names of
    % the frames can be added to the annotation
    subjectAppearToEnd = strcmp(dayFiles.(DAY).endFrame, dayFiles.(DAY).endFrameSubject);
    if ~subjectAppearToEnd
        for currentFrame = END_FRAME + 1 : FINISH_FRAME
            day_01{1}{currentFrame} = fileList{currentFrame}; 
            for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                notes{currentFrame}(person,:) = zeros(1, 4);
            end
        end
    end
    
    % First frame has to be tagged manually
    for currentFrame = START_FRAME : END_FRAME
        diary on;
        
        % Save Name of frame and prepare structure with default values
        day_01{1}{currentFrame} = fileList{currentFrame};
        for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
            day_01{2}{currentFrame}(person,:) = [0, 0, 0];
            notes{currentFrame}(person,:) = zeros(1, 4);
        end

        %% Background Subtraction
        % Load the images
        imgCurrent = imread(fileList{currentFrame});
        imgCurrentOrg = imgCurrent;
        % Blank out Monitor pixels
        imgCurrent = ignoreMonitorAndWindow(imgCurrent, DAY);
        % Extract edges and find foreground simple BG subtraction
        edgeMaskBG = edgeDiff(focusedBGImage, imgCurrent);
        
        %% Interframe Differencing
        % Extract edges and find foreground - Interframe (IF) differencing
        windowMasksIF = zeros([size(edgeMaskBG), (2 * TIME_WINDOW_INTERFRAME)]);
        
        % Interframe Differencing with the past few frames
        windowMasksIFBeforeCurr = zeros([size(edgeMaskBG), TIME_WINDOW_INTERFRAME]);
        windowLowerBound = currentFrame - TIME_WINDOW_INTERFRAME;
        if windowLowerBound > 0
            for frame = windowLowerBound : (currentFrame - 1)
                imgPrev = imread(fileList{frame});
                focusedImgPrev = ignoreMonitorAndWindow(imgPrev, DAY);
                edgeMaskIF = edgeDiff(focusedImgPrev, imgCurrent);
                windowMasksIFBeforeCurr(:,:, frame - windowLowerBound + 1) = edgeMaskIF;
            end
        end
        
        % Interframe Differencing with the next few frames
        windowMasksIFAfterCurr = zeros([size(edgeMaskBG), TIME_WINDOW_INTERFRAME]);
        windowUpperBound = currentFrame + TIME_WINDOW_INTERFRAME;
        if windowUpperBound <= FINISH_FRAME
            for frame = (currentFrame + 1) : windowUpperBound
                imgPrev = imread(fileList{frame});
                focusedImgPrev = ignoreMonitorAndWindow(imgPrev, DAY);
                edgeMaskIF = edgeDiff(focusedImgPrev, imgCurrent);
                windowMasksIFAfterCurr(:,:, frame - (currentFrame + 1) + 1) = edgeMaskIF;
            end
        end
        
        % Merge difference masks with frames before and after the current
        % one and agregate foregrounds
        windowMasksIF = cat(3, windowMasksIFBeforeCurr, windowMasksIFAfterCurr);
        aggregatedWindow = sum(windowMasksIF, 3);
        
        % Find changes around every pixel
        neighbourChanges = zeros(size(edgeMaskBG));
        % Offset of 1 from top + bottom, and left + right boundaries
        for x = 2 : size(neighbourChanges, 1) - 1
            for y = 2 : size(neighbourChanges, 2) - 1
                neighbourChanges(x, y) = accumNeighbours([x, y], aggregatedWindow);
            end
        end
        
        % Min possible = 0, Max possible = 3 * 3 * (2 * N). A real change 
        % is likely to have a count of something like 3 * (2 * N).
        % Therefore, threshold the count at 2 * N
        finalIFMask = neighbourChanges >= 2 * TIME_WINDOW_INTERFRAME;
        
        % Combine Background Subtraction and Interframe Differenceing
        bgMaskMorphed = bwmorph(edgeMaskBG,'dilate',1);
        ifMaskMorphed = bwmorph(finalIFMask,'dilate',1);
        combinedMask = bgMaskMorphed & ifMaskMorphed;
        
        % Clearing the combined mask
        cleanedMask = morphology_Edge(combinedMask);
        
        %{
        imshow(bgMaskMorphed);
        title(['BG for ', num2str(currentFrame)]);
        pause;
        imshow(ifMaskMorphed);
        title(['IF for ', num2str(currentFrame)]);
        pause;
        imshow(combinedMask);
        title(['Combined Mask for ', num2str(currentFrame)]);
        pause;
        imshow(cleanedMask);
        title(['Cleaned Combined Mask for ', num2str(currentFrame)]);
        pause;
        %}
        
        % Locate the coordinates of the largest change aka the person
        noDetection = true;
        if any(cleanedMask(:))
            noDetection = false;
            largestBlobs = bwareafilt(cleanedMask, 1, 'largest');
            props = regionprops(largestBlobs, 'Area', 'Centroid', 'BoundingBox');
        end
        
        %% Take decision about where person/people are
        
        if (noDetection || size(props, 1) == 0)    % Absoloutely no detection: all black mask
            % No detection => no one in scene
            for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                notes{currentFrame}(person,:) = zeros(1, 4);
            end
        else
            if props.Area >= PERSON_MIN_AREA
                % Save properties for all people present in the scene
                day_01{2}{currentFrame}(1, 1:2) = props.Centroid;
                notes{currentFrame}(1,:) = props.BoundingBox;
                
                for person = 2 : MAX_NUM_OF_PEOPLE_EXPECTED
                    day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                    notes{currentFrame}(person,:) = zeros(1, 4);
                end
            else
                for person = 1 : MAX_NUM_OF_PEOPLE_EXPECTED
                    day_01{2}{currentFrame}(person,:) = [0, 0, 0];
                    notes{currentFrame}(person,:) = zeros(1, 4);
                end
            end
        end
        
        %{
        hold on;
        plot(day_01{2}{currentFrame}(1,1), day_01{2}{currentFrame}(1,2), 'r*');
        rectangle('Position', notes{currentFrame}(1,:), 'EdgeColor', 'r', 'LineWidth', 3);
        pause;
        if currentFrame >= DISPLAY_FROM && rem(currentFrame, 9) == 0
            % Only display the properties of the found up to 3 largest
            % blobs
            figure(1);
            imshow(imgCurrentOrg);
            title(['Frame number ', num2str(currentFrame)]);
            hold on;
            plot(day_01{2}{currentFrame}(1,1), day_01{2}{currentFrame}(1,2), 'r*');
            rectangle('Position', notes{currentFrame}(1,:), 'EdgeColor', 'r', 'LineWidth', 3);

            figure(2);
            imshow(cleanedMask);
            title(['Mask of frame ', num2str(currentFrame)]);

            pause;
        end
        %}

        %% Save continuously to avoid any loss of work or time
        save([SAVE_DIR, DAY, '.mat'], 'day_01'); 
        save([SAVE_DIR, DAY, '_notes', '.mat'], 'notes');
        
        if rem(currentFrame, 10) == 0
            disp(num2str(currentFrame));
        end

        diary off;
    end
    diary on;

    %% Save all information and time

    % Save all the locations and names
    save([SAVE_DIR, DAY, '.mat'], 'day_01'); 
    save([SAVE_DIR, DAY, '_notes', '.mat'], 'notes');

    %Time execution
    toc
    elapsedTime = toc;
    clear;

end