function outputImage = edgeDiff(imgPrev, imgCurr)
%EXTRACTEDGES Summary of this function goes here
%   Detailed explanation goes here
    
    % Extract edges
    edgePrevMask = extractEdges_EdgeDetect(imgPrev);
    edgeMask = extractEdges_EdgeDetect(imgCurr);
    
    % Apply morphology to the previous or background frames
    edgePrevMaskMorphed1 = bwmorph(edgePrevMask,'dilate',1);

    % Subtract previous from current image
    noMorph_yesMorph1 = edgeMask & ~edgePrevMaskMorphed1;
    
    outputImage = noMorph_yesMorph1;
    
end

