function [ total ] = accumNeighbours( pos, image2D )
%ACCUMNEIGHBOURS aggreagtes foreground neighbours
%   Indexes into an image and finds the total number of foreground
%   neightbours for all imemdiately surrounding in a 8-connectivity
%   neighbours. Their numbers are the corresponding totals along a window
%   span immediately before and after a current frame
%   
%   'pos' has to be ofset from the boundaries of 'image2D' by at least 1 pixel
%   'image2D' a 2D image to be indexed
%
%   'total' accumulated over the immediate neighbours of 'pos'
    
    total = 0;
    for x = pos(1) - 1 : pos(1) + 1
        for y = pos(2) - 1 : pos(2) + 1
            total = total + image2D(x, y);
        end
    end

end

