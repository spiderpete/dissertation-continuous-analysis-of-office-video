for i = 85 : 100 
frame = imread(day_01{1}{i});
imshow(frame);
hold on;
plot(day_01{2}{i}(1,1), day_01{2}{i}(1,2), 'b*');
%rectangle('Position', notes{i}(1,:), 'EdgeColor', 'b', 'LineWidth', 3);
plot(day_01{2}{i}(2,1), day_01{2}{i}(2,2), 'g*');
%rectangle('Position', notes{i}(2,:), 'EdgeColor', 'g', 'LineWidth', 3);
title(num2str(i));
pause;
end

person = 1;
frame = 91;
day_01{2}{frame}(person,1:2) = day_01{2}{frame + 1}(person,1:2);

DAY_NUM = '01';
DAY = ['Day', DAY_NUM];
DIR = './Data/OFFICE/';
%READ_DIR = [DIR, 'Ground Truths/GT/GT - Auto/'];
SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Positions/'];
save([SAVE_DIR, DAY, '.mat'], 'day_01');

day_01{2}{i}(3,1:2) = day_01{2}{i}(2,1:2);
day_01{2}{i}(2,1:2) = day_01{2}{i}(1,1:2);
day_01{2}{i}(1,1:2) = day_01{2}{i}(3,1:2);
day_01{2}{i} = day_01{2}{i}(1:2,:);

finish = size(day_01{1},1);
% Check if all slots present
for i = 1 : finish
    day_01{2}{i}(1,:);
    day_01{2}{i}(2,:);
    day_01{2}{i}(3,:);
end

% Fill any missing slots
for i = 1 : finish
    if size(day_01{2}{i},1) == 1
        day_01{2}{i}(2,:) = [0 0 0];
    end
    if size(day_01{2}{i},1) == 2
        day_01{2}{i}(3,:) = [0 0 0];
    end
end