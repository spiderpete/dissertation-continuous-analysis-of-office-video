% compute speed
clear speed_pos
clear speed_inf
speed_pos = mod_pos3;
speed_inf = zeros(1,length(speed_pos));
% first column is velocity
% second column is direction
for i=2:length(speed_pos)
%     i;
    distanceX = abs(speed_pos(1,i,1)-speed_pos(1,i-1,1));
    distanceY = abs(speed_pos(1,i,2)-speed_pos(1,i-1,2));
    distance = sqrt(distanceX.^2 + distanceY.^2);
    speed_inf(1,i,1) = distance;
    speed_inf(1,i,2) = distanceX;
    speed_inf(1,i,3) = distanceY;
%     a = atand(distance/distance)
end
mod_speed3 = speed_inf;
% ilist = ilist5;
% a = find(speed_inf > 0);
% % a = acosd(1/sqrt(2))
% for i=1:length(a)
%     imshow(ilist{a(i)});
%     pause(0.05);
% end