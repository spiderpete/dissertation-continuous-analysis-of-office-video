% Saved data has to be labeled with the day format '00'
autoBehaviorAnnotation('01');
autoBehaviorAnnotation('02');
autoBehaviorAnnotation('03');
autoBehaviorAnnotation('04');
autoBehaviorAnnotation('05');
autoBehaviorAnnotation('06');
autoBehaviorAnnotation('07');
autoBehaviorAnnotation('08');
autoBehaviorAnnotation('09');
for day = 10 : 20
    autoBehaviorAnnotation(num2str(day));
end