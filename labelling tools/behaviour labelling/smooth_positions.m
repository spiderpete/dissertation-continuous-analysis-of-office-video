%% smooth the position data
% Between two points
smooth_pos(1,:,1) = esti1000;
% test_pos = positions1;
for i=2:length(smooth_pos)-1
    if (smooth_pos(1,i,1)==0)&&(smooth_pos(1,i-1,1)>0)&&(smooth_pos(1,i+1,1)>0)
        smooth_pos(1,i,:)=smooth_pos(1,i-1,:);
    end
end
re_esti1000 = smooth_pos(1,:,1);
%% smooth the position data (use this one to correct position)
% Between four points
% val|0|0|val
clearvars smooth_pos test_pos i j
smooth_pos = positions3;
test_pos = smooth_pos;
for i=3:length(smooth_pos)-2
    if (smooth_pos(1,i,1)==0)&&(smooth_pos(1,i-1,1)>0)&&(smooth_pos(1,i+1,1)>0)
        smooth_pos(1,i,:)=smooth_pos(1,i-1,:);
    elseif (smooth_pos(1,i,1)==0)&&(smooth_pos(1,i-1,1)>0)&&(sum(smooth_pos(1,i:i+2,1)>0))
        smooth_pos(1,i,:)=smooth_pos(1,i-1,:);
    end
end

%% between 5 points
% val|0|0|0|val


%% compare difference
j=0;
test_pos = positions3;
for i=1:length(smooth_pos)
    if smooth_pos(1,i,:)~= test_pos(1,i,:)
        j=j+1;
    end
end
j
%% save the modify data
%be careful 
positions3 = smooth_pos;