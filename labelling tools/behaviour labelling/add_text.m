function add_text(behaviour)
    text(71,  180,'NO ONE',     'Color','green'); %1
    text(284, 180,'ENTERING',   'Color','green'); %2
    text(505, 180,'LEAVING',    'Color','green'); %3
    text(715, 180,'WALKING',    'Color','green'); %4
    text(920, 180,'COMPUTER',   'Color','green'); %5
    text(1141,180,'SITTING',    'Color','green'); %6
    text(53,  540,'WHITEBOARD', 'Color','green'); %7
    text(291, 540,'WINDOW',     'Color','green'); %8
    text(485, 540,'BOOKSHELF',  'Color','green'); %9
    text(715, 540,'TALKING',    'Color','green'); %10
    text(925, 540,'STRANGE',    'Color','green'); %11
    text(1127,540,'UNDEFINED',  'Color','green'); %12
    
    switch nargin
        case 0
            return
        case 1
            % Behavious chosen, thus continue
    end
    
    switch behaviour
        case 1
            text(71,  180,'NO ONE',     'Color','red'); %1
        case 2
            text(284, 180,'ENTERING',   'Color','red'); %2
        case 3
            text(505, 180,'LEAVING',    'Color','red'); %3
        case 4
            text(715, 180,'WALKING',    'Color','red'); %4
        case 5
            text(920, 180,'COMPUTER',   'Color','red'); %5
        case 6
            text(1141,180,'SITTING',    'Color','red'); %6
        case 7
            text(53,  540,'WHITEBOARD', 'Color','red'); %7
        case 8
            text(291, 540,'WINDOW',     'Color','red'); %8
        case 9
            text(485, 540,'BOOKSHELF',  'Color','red'); %9
        case 10
            text(715, 540,'TALKING',    'Color','red'); %10
        case 11
            text(925, 540,'STRANGE',    'Color','red'); %11
        case 12
            text(1127,540,'UNDEFINED',  'Color','red'); %12
        otherwise
            % No legal behaviour chosen
    end
end