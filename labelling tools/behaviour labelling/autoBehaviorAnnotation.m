function autoBehaviorAnnotation( DAY_NUM )

    % Time execution
    tic

    % Custamisable parameters
    %DAY_NUM = '04';
    INIT_FRAME = 1;
    EXECUTION_ON_WINDOWS = false;

    DOOR_X_MIN = 300;
    DOOR_X_MAX = 720;
    DOOR_Y_MIN = 1;
    DOOR_Y_MAX = 700;

    CENTRE_X_MIN = 300;
    CENTRE_X_MAX = 650;
    CENTRE_Y_MIN = 205;
    CENTRE_Y_MAX = 1065;

    MONITOR_X_MIN = 215;
    MONITOR_X_MAX = 475;
    MONITOR_Y_MIN = 365;
    MONITOR_Y_MAX = 595;

    BOARD_X_MIN = 115;
    BOARD_X_MAX = 350;
    BOARD_Y_MIN = 625;
    BOARD_Y_MAX = 820;

    WINDOW_X_MIN = 115;
    WINDOW_X_MAX = 390;
    WINDOW_Y_MIN = 785;
    WINDOW_Y_MAX = 1010;

    SELVES1_X_MIN = 465;
    SELVES1_X_MAX = 720;
    SELVES1_Y_MIN = 1110;
    SELVES1_Y_MAX = 1280;

    SELVES2_X_MIN = 560;
    SELVES2_X_MAX = 720;
    SELVES2_Y_MIN = 90;
    SELVES2_Y_MAX = 585;

    % Stationary parameters
    DAY = ['Day', DAY_NUM];
    DIR = './Data/OFFICE/';
    READ_DIR = [DIR, 'Ground Truths/GT/GT - Positions/'];
    SAVE_DIR = [DIR, 'Ground Truths/GT/GT - Auto2/'];

    dayFile = dayFiles.(DAY).file;
    path = [DIR, dayFile, '/inspacecam163'];
    fileList = generateFileList(path, EXECUTION_ON_WINDOWS);

    % Load auto-annotated frames and notes
    load([READ_DIR, 'Day', DAY_NUM, '.mat']);  % variable = day_01
    NUM_PEOPLE = size(day_01{2}{1}, 1);                 % # of people constant all throught

    startFrameName = dayFiles.(DAY).startFrameSubject;  % Fastforward to the frame 
                                                        % where the subject appears 
                                                        % on stage
    matches = strfind(fileList, startFrameName);
    START_FRAME = find(not(cellfun('isempty', matches)));
    endFrameName = dayFiles.(DAY).endFrameSubject;      % Stop at the frame where
                                                        % the subject disappears 
                                                        % for good from stage
    matches = strfind(fileList, endFrameName);
    END_FRAME = find(not(cellfun('isempty', matches)));
    finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                        % of the video so the frame
                                                        % names can be annotated
    matches = strfind(fileList, finishFrameName);
    FINISH_FRAME = find(not(cellfun('isempty', matches)));

    % Structures to hold date anotation data
    % Properties include: [column location, row location, behavior]
    % always starting with the main person tracked
    % day_N{1}          list of frames
    % day_N{1}{x}       name of file 'x'
    % day_N{2}          list of all frames's properties' cells
    % day_N{2}{x}       list of frames' properties for frame 'x' for all people
    % day_N{2}{x}(y,:)  list of properties for frame 'x' for person 'y'
    % day_N{2}{x}(y,1)  col position of person 'y' for frame 'x'
    % day_N{2}{x}(y,2)  row position of person 'y' for frame 'x'
    % day_N{2}{x}(y,3)  behaviour of person 'y' for frame 'x'

    %% Iterate through the whole video to apply work

    % If subject does not appear at the beginning of the video, before
    % fastforwarding to the frame of the subject, annotate information
    % structure up to that frame
    subjectAppearAtStart = strcmp(dayFiles.(DAY).startFrame, dayFiles.(DAY).startFrameSubject);
    if ~subjectAppearAtStart
        for currentFrame = INIT_FRAME : START_FRAME - 1
            for person = 1 : NUM_PEOPLE
                day_01{2}{currentFrame}(person, 3) = 1;
            end
        end
    end

    % If the subject does not appear to the end of the video, after
    % annotating the behaviour in the frames up to the disappearance of
    % the subject, keep going until the end of the video so the names of
    % the frames can be added to the annotation
    subjectAppearToEnd = strcmp(dayFiles.(DAY).endFrame, dayFiles.(DAY).endFrameSubject);
    if ~subjectAppearToEnd
        for currentFrame = END_FRAME + 1 : FINISH_FRAME
            for person = 1 : NUM_PEOPLE
                day_01{2}{currentFrame}(person, 3) = 1;
            end
        end
    end

    for currentFrame = START_FRAME : END_FRAME

        for person = 1 : NUM_PEOPLE
            position = day_01{2}{currentFrame}(person, 1:2);

            % Labels
            [action1, action2, action3, action4, action5, action6, action7, ...
             action8, action9, action10] = deal(false);

            %% Label 1: No one
            if isequal(position, [0, 0])
                action1 = true;
            end

            %% Label 2: Entering the room
            position1Before = [-1, -1];
            position2Before = [-1, -1];
            position3Before = [-1, -1];
            %behaviour1Before = 0;
            %behaviour2Before = 0;
            if currentFrame - 1 >= INIT_FRAME
                position1Before = day_01{2}{currentFrame - 1}(person, 1:2);
            end
            if currentFrame - 2 >= INIT_FRAME
                position2Before = day_01{2}{currentFrame - 2}(person, 1:2);
            end
            if currentFrame - 3 >= INIT_FRAME
                position3Before = day_01{2}{currentFrame - 3}(person, 1:2);
            end

            %{
            % Pre-checking for same behaviour, 2, in previous two frames
            contFrom1Before = true;
            if ~isequal(position1Before, [0, 0]) 
                contFrom1Before = behaviour1Before == 2;
            end
            contFrom2Before = true;
            if ~isequal(position2Before, [0, 0]) 
                contFrom2Before = behaviour2Before == 2;
            end
            %}

            if (((position(1) >= DOOR_Y_MIN && position(1) <= DOOR_Y_MAX && ...
                position(2) >= DOOR_X_MIN && position(2) <= DOOR_X_MAX) || ...
                (position1Before(1) >= DOOR_Y_MIN && position1Before(1) <= DOOR_Y_MAX && ...
                position1Before(2) >= DOOR_X_MIN && position1Before(2) <= DOOR_X_MAX) || ...
                (position2Before(1) >= DOOR_Y_MIN && position2Before(1) <= DOOR_Y_MAX && ...
                position2Before(2) >= DOOR_X_MIN && position2Before(2) <= DOOR_X_MAX) ...
                ) && ...
                (isequal(position1Before, [0, 0]) || ...
                 isequal(position2Before, [0, 0]) || ...
                 isequal(position3Before, [0, 0])) ) %...
                %&& contFrom1Before && contFrom2Before)

                action2 = true;
            end

            %% Label 3: Leaving the room
            position1After = [-1, -1];
            position2After = [-1, -1];
            position3After = [-1, -1];

            if currentFrame + 1 <= FINISH_FRAME
                position1After = day_01{2}{currentFrame + 1}(person, 1:2);
            end
            if currentFrame + 2 <= FINISH_FRAME
                position2After = day_01{2}{currentFrame + 2}(person, 1:2);
            end
            if currentFrame + 3 <= FINISH_FRAME
                position3After = day_01{2}{currentFrame + 3}(person, 1:2);
            end

            if (((position(1) >= DOOR_Y_MIN && position(1) <= DOOR_Y_MAX && ...
                position(2) >= DOOR_X_MIN && position(2) <= DOOR_X_MAX) || ...
                (position1After(1) >= DOOR_Y_MIN && position1After(1) <= DOOR_Y_MAX && ...
                position1After(2) >= DOOR_X_MIN && position1After(2) <= DOOR_X_MAX) || ...
                (position2After(1) >= DOOR_Y_MIN && position2After(1) <= DOOR_Y_MAX && ...
                position2After(2) >= DOOR_X_MIN && position2After(2) <= DOOR_X_MAX) ...
                ) && ...
                (isequal(position1After, [0, 0]) || ...
                 isequal(position2After, [0, 0]) || ...
                 isequal(position3After, [0, 0])))

                action3 = true;
            end

            %% Label 4: Walking or Standing
            if (position(1) >= CENTRE_Y_MIN && position(1) <= CENTRE_Y_MAX && ...
                position(2) >= CENTRE_X_MIN && position(2) <= CENTRE_X_MAX)

                action4 = true;
            end

            %% Label 5: Using terminals
            position4Before = [-1, -1];
            position5Before = [-1, -1];
            position6Before = [-1, -1];
            position7Before = [-1, -1];
            position8Before = [-1, -1];
            position9Before = [-1, -1];
            position10Before = [-1, -1];
            position4After = [-1, -1];
            position5After = [-1, -1];
            position6After = [-1, -1];
            position7After = [-1, -1];
            position8After = [-1, -1];
            position9After = [-1, -1];
            position10After = [-1, -1];

            if currentFrame - 4 >= INIT_FRAME
                position4Before = day_01{2}{currentFrame - 4}(person, 1:2);
            end
            if currentFrame - 5 >= INIT_FRAME
                position5Before = day_01{2}{currentFrame - 5}(person, 1:2);
            end
            if currentFrame - 6 >= INIT_FRAME
                position6Before = day_01{2}{currentFrame - 6}(person, 1:2);
            end
            if currentFrame - 7 >= INIT_FRAME
                position7Before = day_01{2}{currentFrame - 7}(person, 1:2);
            end
            if currentFrame - 8 >= INIT_FRAME
                position8Before = day_01{2}{currentFrame - 8}(person, 1:2);
            end
            if currentFrame - 9 >= INIT_FRAME
                position9Before = day_01{2}{currentFrame - 9}(person, 1:2);
            end
            if currentFrame - 10 >= INIT_FRAME
                position10Before = day_01{2}{currentFrame - 10}(person, 1:2);
            end
            if currentFrame + 4 <= FINISH_FRAME
                position4After = day_01{2}{currentFrame + 4}(person, 1:2);
            end
            if currentFrame + 5 <= FINISH_FRAME
                position5After = day_01{2}{currentFrame + 5}(person, 1:2);
            end
            if currentFrame + 6 <= FINISH_FRAME
                position6After = day_01{2}{currentFrame + 6}(person, 1:2);
            end
            if currentFrame + 7 <= FINISH_FRAME
                position7After = day_01{2}{currentFrame + 7}(person, 1:2);
            end
            if currentFrame + 8 <= FINISH_FRAME
                position8After = day_01{2}{currentFrame + 8}(person, 1:2);
            end
            if currentFrame + 9 <= FINISH_FRAME
                position9After = day_01{2}{currentFrame + 9}(person, 1:2);
            end
            if currentFrame + 10 <= FINISH_FRAME
                position10After = day_01{2}{currentFrame + 10}(person, 1:2);
            end

            if (position(1) >= MONITOR_Y_MIN && position(1) <= MONITOR_Y_MAX && ...
                position(2) >= MONITOR_X_MIN && position(2) <= MONITOR_X_MAX) ...
                && (...
              ((position1Before(1) >= MONITOR_Y_MIN && position1Before(1) <= MONITOR_Y_MAX && ...
                position1Before(2) >= MONITOR_X_MIN && position1Before(2) <= MONITOR_X_MAX) && ...
               (position2Before(1) >= MONITOR_Y_MIN && position2Before(1) <= MONITOR_Y_MAX && ...
                position2Before(2) >= MONITOR_X_MIN && position2Before(2) <= MONITOR_X_MAX) && ...
               (position3Before(1) >= MONITOR_Y_MIN && position3Before(1) <= MONITOR_Y_MAX && ...
                position3Before(2) >= MONITOR_X_MIN && position3Before(2) <= MONITOR_X_MAX) && ...
               (position4Before(1) >= MONITOR_Y_MIN && position4Before(1) <= MONITOR_Y_MAX && ...
                position4Before(2) >= MONITOR_X_MIN && position4Before(2) <= MONITOR_X_MAX) && ...
               (position5Before(1) >= MONITOR_Y_MIN && position5Before(1) <= MONITOR_Y_MAX && ...
                position5Before(2) >= MONITOR_X_MIN && position5Before(2) <= MONITOR_X_MAX) && ...
               (position6Before(1) >= MONITOR_Y_MIN && position6Before(1) <= MONITOR_Y_MAX && ...
                position6Before(2) >= MONITOR_X_MIN && position6Before(2) <= MONITOR_X_MAX) && ...
               (position7Before(1) >= MONITOR_Y_MIN && position7Before(1) <= MONITOR_Y_MAX && ...
                position7Before(2) >= MONITOR_X_MIN && position7Before(2) <= MONITOR_X_MAX) && ...
               (position8Before(1) >= MONITOR_Y_MIN && position8Before(1) <= MONITOR_Y_MAX && ...
                position8Before(2) >= MONITOR_X_MIN && position8Before(2) <= MONITOR_X_MAX) && ...
               (position9Before(1) >= MONITOR_Y_MIN && position9Before(1) <= MONITOR_Y_MAX && ...
                position9Before(2) >= MONITOR_X_MIN && position9Before(2) <= MONITOR_X_MAX) && ...
               (position10Before(1) >= MONITOR_Y_MIN && position10Before(1) <= MONITOR_Y_MAX && ...
                position10Before(2) >= MONITOR_X_MIN && position10Before(2) <= MONITOR_X_MAX)) ...
                || ...
              ((position1After(1) >= MONITOR_Y_MIN && position1After(1) <= MONITOR_Y_MAX && ...
                position1After(2) >= MONITOR_X_MIN && position1After(2) <= MONITOR_X_MAX) && ...
               (position2After(1) >= MONITOR_Y_MIN && position2After(1) <= MONITOR_Y_MAX && ...
                position2After(2) >= MONITOR_X_MIN && position2After(2) <= MONITOR_X_MAX) && ...
               (position3After(1) >= MONITOR_Y_MIN && position3After(1) <= MONITOR_Y_MAX && ...
                position3After(2) >= MONITOR_X_MIN && position3After(2) <= MONITOR_X_MAX) && ...
               (position4After(1) >= MONITOR_Y_MIN && position4After(1) <= MONITOR_Y_MAX && ...
                position4After(2) >= MONITOR_X_MIN && position4After(2) <= MONITOR_X_MAX) && ...
               (position5After(1) >= MONITOR_Y_MIN && position5After(1) <= MONITOR_Y_MAX && ...
                position5After(2) >= MONITOR_X_MIN && position5After(2) <= MONITOR_X_MAX) && ...
               (position6After(1) >= MONITOR_Y_MIN && position6After(1) <= MONITOR_Y_MAX && ...
                position6After(2) >= MONITOR_X_MIN && position6After(2) <= MONITOR_X_MAX) && ...
               (position7After(1) >= MONITOR_Y_MIN && position7After(1) <= MONITOR_Y_MAX && ...
                position7After(2) >= MONITOR_X_MIN && position7After(2) <= MONITOR_X_MAX) && ...
               (position8After(1) >= MONITOR_Y_MIN && position8After(1) <= MONITOR_Y_MAX && ...
                position8After(2) >= MONITOR_X_MIN && position8After(2) <= MONITOR_X_MAX) && ...
               (position9After(1) >= MONITOR_Y_MIN && position9After(1) <= MONITOR_Y_MAX && ...
                position9After(2) >= MONITOR_X_MIN && position9After(2) <= MONITOR_X_MAX) && ...
               (position10After(1) >= MONITOR_Y_MIN && position10After(1) <= MONITOR_Y_MAX && ...
                position10After(2) >= MONITOR_X_MIN && position10After(2) <= MONITOR_X_MAX)) ...
                    )

                action5 = true;
            end

            %% Label 6: Sitting
            if (position(1) >= CENTRE_Y_MIN && position(1) <= CENTRE_Y_MAX && ...
                position(2) >= CENTRE_X_MIN && position(2) <= CENTRE_X_MAX) ...
                && (...
              ((position1Before(1) >= CENTRE_Y_MIN && position1Before(1) <= CENTRE_Y_MAX && ...
                position1Before(2) >= CENTRE_X_MIN && position1Before(2) <= CENTRE_X_MAX) && ...
               (position2Before(1) >= CENTRE_Y_MIN && position2Before(1) <= CENTRE_Y_MAX && ...
                position2Before(2) >= CENTRE_X_MIN && position2Before(2) <= CENTRE_X_MAX) && ...
               (position3Before(1) >= CENTRE_Y_MIN && position3Before(1) <= CENTRE_Y_MAX && ...
                position3Before(2) >= CENTRE_X_MIN && position3Before(2) <= CENTRE_X_MAX) && ...
               (position4Before(1) >= CENTRE_Y_MIN && position4Before(1) <= CENTRE_Y_MAX && ...
                position4Before(2) >= CENTRE_X_MIN && position4Before(2) <= CENTRE_X_MAX) && ...
               (position5Before(1) >= CENTRE_Y_MIN && position5Before(1) <= CENTRE_Y_MAX && ...
                position5Before(2) >= CENTRE_X_MIN && position5Before(2) <= CENTRE_X_MAX) && ...
               (position6Before(1) >= CENTRE_Y_MIN && position6Before(1) <= CENTRE_Y_MAX && ...
                position6Before(2) >= CENTRE_X_MIN && position6Before(2) <= CENTRE_X_MAX) && ...
               (position7Before(1) >= CENTRE_Y_MIN && position7Before(1) <= CENTRE_Y_MAX && ...
                position7Before(2) >= CENTRE_X_MIN && position7Before(2) <= CENTRE_X_MAX) && ...
               (position8Before(1) >= CENTRE_Y_MIN && position8Before(1) <= CENTRE_Y_MAX && ...
                position8Before(2) >= CENTRE_X_MIN && position8Before(2) <= CENTRE_X_MAX) && ...
               (position9Before(1) >= CENTRE_Y_MIN && position9Before(1) <= CENTRE_Y_MAX && ...
                position9Before(2) >= CENTRE_X_MIN && position9Before(2) <= CENTRE_X_MAX) && ...
               (position10Before(1) >= CENTRE_Y_MIN && position10Before(1) <= CENTRE_Y_MAX && ...
                position10Before(2) >= CENTRE_X_MIN && position10Before(2) <= CENTRE_X_MAX)) ...
                || ...
              ((position1After(1) >= CENTRE_Y_MIN && position1After(1) <= CENTRE_Y_MAX && ...
                position1After(2) >= CENTRE_X_MIN && position1After(2) <= CENTRE_X_MAX) && ...
               (position2After(1) >= CENTRE_Y_MIN && position2After(1) <= CENTRE_Y_MAX && ...
                position2After(2) >= CENTRE_X_MIN && position2After(2) <= CENTRE_X_MAX) && ...
               (position3After(1) >= CENTRE_Y_MIN && position3After(1) <= CENTRE_Y_MAX && ...
                position3After(2) >= CENTRE_X_MIN && position3After(2) <= CENTRE_X_MAX) && ...
               (position4After(1) >= CENTRE_Y_MIN && position4After(1) <= CENTRE_Y_MAX && ...
                position4After(2) >= CENTRE_X_MIN && position4After(2) <= CENTRE_X_MAX) && ...
               (position5After(1) >= CENTRE_Y_MIN && position5After(1) <= CENTRE_Y_MAX && ...
                position5After(2) >= CENTRE_X_MIN && position5After(2) <= CENTRE_X_MAX) && ...
               (position6After(1) >= CENTRE_Y_MIN && position6After(1) <= CENTRE_Y_MAX && ...
                position6After(2) >= CENTRE_X_MIN && position6After(2) <= CENTRE_X_MAX) && ...
               (position7After(1) >= CENTRE_Y_MIN && position7After(1) <= CENTRE_Y_MAX && ...
                position7After(2) >= CENTRE_X_MIN && position7After(2) <= CENTRE_X_MAX) && ...
               (position8After(1) >= CENTRE_Y_MIN && position8After(1) <= CENTRE_Y_MAX && ...
                position8After(2) >= CENTRE_X_MIN && position8After(2) <= CENTRE_X_MAX) && ...
               (position9After(1) >= CENTRE_Y_MIN && position9After(1) <= CENTRE_Y_MAX && ...
                position9After(2) >= CENTRE_X_MIN && position9After(2) <= CENTRE_X_MAX) && ...
               (position10After(1) >= CENTRE_Y_MIN && position10After(1) <= CENTRE_Y_MAX && ...
                position10After(2) >= CENTRE_X_MIN && position10After(2) <= CENTRE_X_MAX)) ...
                    )

                action6 = true;
            end

            %% Label 7: Whiteboard
            if (position(1) >= BOARD_Y_MIN && position(1) <= BOARD_Y_MAX && ...
                position(2) >= BOARD_X_MIN && position(2) <= BOARD_X_MAX) ...
                && (...
              ((position1Before(1) >= BOARD_Y_MIN && position1Before(1) <= BOARD_Y_MAX && ...
                position1Before(2) >= BOARD_X_MIN && position1Before(2) <= BOARD_X_MAX) && ...
               (position2Before(1) >= BOARD_Y_MIN && position2Before(1) <= BOARD_Y_MAX && ...
                position2Before(2) >= BOARD_X_MIN && position2Before(2) <= BOARD_X_MAX) && ...
               (position3Before(1) >= BOARD_Y_MIN && position3Before(1) <= BOARD_Y_MAX && ...
                position3Before(2) >= BOARD_X_MIN && position3Before(2) <= BOARD_X_MAX) && ...
               (position4Before(1) >= BOARD_Y_MIN && position4Before(1) <= BOARD_Y_MAX && ...
                position4Before(2) >= BOARD_X_MIN && position4Before(2) <= BOARD_X_MAX) && ...
               (position5Before(1) >= BOARD_Y_MIN && position5Before(1) <= BOARD_Y_MAX && ...
                position5Before(2) >= BOARD_X_MIN && position5Before(2) <= BOARD_X_MAX)) ...
                || ...
              ((position1After(1) >= BOARD_Y_MIN && position1After(1) <= BOARD_Y_MAX && ...
                position1After(2) >= BOARD_X_MIN && position1After(2) <= BOARD_X_MAX) && ...
               (position2After(1) >= BOARD_Y_MIN && position2After(1) <= BOARD_Y_MAX && ...
                position2After(2) >= BOARD_X_MIN && position2After(2) <= BOARD_X_MAX) && ...
               (position3After(1) >= BOARD_Y_MIN && position3After(1) <= BOARD_Y_MAX && ...
                position3After(2) >= BOARD_X_MIN && position3After(2) <= BOARD_X_MAX) && ...
               (position4After(1) >= BOARD_Y_MIN && position4After(1) <= BOARD_Y_MAX && ...
                position4After(2) >= BOARD_X_MIN && position4After(2) <= BOARD_X_MAX) && ...
               (position5After(1) >= BOARD_Y_MIN && position5After(1) <= BOARD_Y_MAX && ...
                position5After(2) >= BOARD_X_MIN && position5After(2) <= BOARD_X_MAX)) ...
                    )

                action7 = true;
            end

            %% Label 8: Near window
            if (position(1) >= WINDOW_Y_MIN && position(1) <= WINDOW_Y_MAX && ...
                position(2) >= WINDOW_X_MIN && position(2) <= WINDOW_X_MAX)

                action8 = true;
            end

            %% Lable 9: Near bookshelf
            % Last appearance in bottom of window
            lastPresentPosition = day_01{2}{currentFrame}(person, 1:2);
            foundLastPresence = false;
            i = 0;
            if isequal(lastPresentPosition, [0,0])
                while (currentFrame - (i + 1)) >= 1 % don't go beyond video boudaries
                    i = i + 1;
                    lastPresentPosition = day_01{2}{currentFrame - i}(person, 1:2);

                    if ~isequal(lastPresentPosition, [0, 0])
                        foundLastPresence = true;
                        break;
                    end
                end
            end

            lastBeenVisiblyNearShelf = false;
            if foundLastPresence
               if (lastPresentPosition(1) >= SELVES1_Y_MIN && lastPresentPosition(1) <= SELVES1_Y_MAX && ...
                   lastPresentPosition(2) >= SELVES1_X_MIN && lastPresentPosition(2) <= SELVES1_X_MAX) || ...
                  (lastPresentPosition(1) >= SELVES2_Y_MIN && lastPresentPosition(1) <= SELVES2_Y_MAX && ...
                   lastPresentPosition(2) >= SELVES2_X_MIN && lastPresentPosition(2) <= SELVES2_X_MAX)

                   lastBeenVisiblyNearShelf = true;
               end
            end

            if ((position(1) >= SELVES1_Y_MIN && position(1) <= SELVES1_Y_MAX && ...
                 position(2) >= SELVES1_X_MIN && position(2) <= SELVES1_X_MAX) || ...
                (position(1) >= SELVES2_Y_MIN && position(1) <= SELVES2_Y_MAX && ...
                 position(2) >= SELVES2_X_MIN && position(2) <= SELVES2_X_MAX) ...
               ) || lastBeenVisiblyNearShelf

                action9 = true;
            end

            %% Label 10: Talking to someone/Having a meeting
            % Only the tracked person will be allowed to have this labels, 
            % others will have to be annotated with actual action
            if person == 1
                for checkPerson = 2 : NUM_PEOPLE
                    checkedPositions = day_01{2}{currentFrame}(checkPerson, 1:2);
                    if ~isequal(checkedPositions, [0, 0])
                        action10 = true;
                        break;
                    end
                end
            end


            %% Take behaviour annotation decisions

            behaviourClass = 0;
            if action1
                behaviourClass = 1;
            elseif action10
                behaviourClass = 10;
            elseif action7
                behaviourClass = 7;
            elseif action8
                behaviourClass = 8;
            elseif action9
                behaviourClass = 9;
            elseif action5
                behaviourClass = 5;
            elseif action2
                behaviourClass = 2;
            elseif action3
                behaviourClass = 3;
            elseif action6
                behaviourClass = 6;
            elseif action4
                behaviourClass = 4;
            else
                behaviourClass = 11;
            end

            % Finally add annotation to each person to the current frame
            day_01{2}{currentFrame}(person, 3) = behaviourClass;

        end

        disp(['Frame ', num2str(currentFrame), ' annotated.']);
    end

    % Final save
    save([SAVE_DIR, DAY, '.mat'], 'day_01');

    %Time execution
    toc
    elapsedTime = toc;
    clear;
    disp('Process complete!');
end