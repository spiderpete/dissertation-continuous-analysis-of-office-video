function labels = behavior_label(click, horBound, verBounds)
% click = current positions(pos_behave) data
labels = 1;
if judge_boundrary(    click,0,           verBounds(1),0,       horBound)   ==1
    labels = 1;
elseif judge_boundrary(click,verBounds(1),verBounds(2),0,       horBound)   ==1
    labels = 2;
elseif judge_boundrary(click,verBounds(2),verBounds(3),0,       horBound)   ==1
    labels = 3;
elseif judge_boundrary(click,verBounds(3),verBounds(4),0,       horBound)   ==1
    labels = 4;
elseif judge_boundrary(click,verBounds(4),verBounds(5),0,       horBound)   ==1
    labels = 5;
elseif judge_boundrary(click,verBounds(5),1280,        0,       horBound)   ==1
    labels = 6;
elseif judge_boundrary(click,0,           verBounds(1),horBound,horBound*2) ==1
    labels = 7;
elseif judge_boundrary(click,verBounds(1),verBounds(2),horBound,horBound*2) ==1
    labels = 8;
elseif judge_boundrary(click,verBounds(2),verBounds(3),horBound,horBound*2) ==1
    labels = 9;
elseif judge_boundrary(click,verBounds(3),verBounds(4),horBound,horBound*2) ==1
    labels = 10;
elseif judge_boundrary(click,verBounds(4),verBounds(5),horBound,horBound*2) ==1
    labels = 11;
elseif judge_boundrary(click,verBounds(5),1280,        horBound,horBound*2) ==1
    labels = 12;
end



