%% position
% load postion data
% load list mat
positions = positions5;
ilist = ilist5;
for i=1000:length(positions)
    imshow(ilist{i});
    hold on 
    plot(positions(1,i,1),positions(1,i,2),'*');
%     line([positions(1,i,2) positions(1,i,1)],[positions(1,i-1,2) positions(1,i-1,1)]);
%     line([1 1],[300 300]);
    hold off
%     a = sum(sum(positions(1,i,:)))>0
%     behavior1(i)
    pause(0.05);
end

% a = find(positions>0);
% for i=1:length(a)
%     imshow(ilist{a(1)});
%     pause(0.05)
% end
% sum(sum(positions(1,1:100,:)))

%% behavior
behaviors = behavior1;
positions = positions1;
ilist = ilist1;
for i=1000:length(behaviors)
    if (positions(1,i,:)==positions(1,i-1,:))
        continue;
    end
    imshow(ilist{i});
    hold on
    plot(positions(1,i,1),positions(1,i,2),'*');
    hold off
    behaviors(i)
    pause(0.05);
end