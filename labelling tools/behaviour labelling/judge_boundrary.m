function [boolean_value] = judge_boundrary(click,xlower,xupper,ylower,yupper)
% click should be a vector with two columns    
    if (click(1,1)<xupper)&&(click(1,1)>xlower)&&(click(1,2)<yupper)&&(click(1,2)>ylower)
        boolean_value = 1;
    else
        boolean_value = 0;
    end
end