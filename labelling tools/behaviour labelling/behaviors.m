%% initialize
% compare two frame and see whether the center of mass moves 
global frame pos_behave
x = [213 426 640 853 1066];

% load positions
% load image list
% use to correct behavior
positions = new_postions;
ilist = new_ilist;


% use to label behavior
% positions = positions3;
% ilist = ilist3;

%%
pos_behave = zeros(length(positions),2);
behavior = ones(1,length(positions));
frame = 2;% larger than 2
% for frame=2:length(positions)-1
while(1)
    frame
    distanceX = abs(positions(1,frame,1)-positions(1,frame-1,1));
    distanceY = abs(positions(1,frame,2)-positions(1,frame-1,2));
    % i guess here cause the problem
    % i set the difference to be current frame with the next frame which is
    % totally wrong abs(positions(1,frame,1)-positions(1,frame+1,1));
    distance = sqrt(distanceX.^2 + distanceY.^2);
    if frame <= 8
        if distance < 5
            behavior(frame) = behavior(frame-1);
        else
            disp('      distance > 5 ');
            I = imread(ilist{frame});
            imageHandle = imshow(I);
            hold on
            hline(360);
            vline(x);
            add_text();
            hold off
            set(imageHandle,'ButtonDownFcn',@ClickCallback);
            pause
            hold on
            plot([pos_behave(frame,1)],[pos_behave(frame,2)],'r *');
            hold off
            pause(0.1)
            click = pos_behave(frame,:);
            behavior_label(click)
            behavior(frame)=behavior_label(click);
            %         if judge_boundrary()
        end
        behavior(frame)
    else
        if (sum(positions(1,frame,:)<1)) && (sum(sum(positions(1,frame-8:frame,:)))>0)
            for i=1:8
                I = imread(ilist{frame});
                imageHandle = imshow(I);
                hold on
                hline(360);
                vline(x);
                add_text();
                hold off
                set(imageHandle,'ButtonDownFcn',@ClickCallback);
                pause
                hold on
                plot([pos_behave(frame,1)],[pos_behave(frame,2)],'r *');
                hold off
                pause(0.1)
                click = pos_behave(frame,:);
                behavior_label(click)
                behavior(frame)=behavior_label(click);
                frame = frame+1;
            end
        else
            if distance < 5
                behavior(frame) = behavior(frame-1);
            else
                I = imread(ilist{frame});
                imageHandle = imshow(I);
                hold on
                hline(360);
                vline(x);
                add_text();
                hold off
                set(imageHandle,'ButtonDownFcn',@ClickCallback);
                pause
                hold on
                plot([pos_behave(frame,1)],[pos_behave(frame,2)],'r *');
                hold off
                pause(0.1)
                click = pos_behave(frame,:);
                behavior_label(click)
                behavior(frame)=behavior_label(click);
            end
%             behavior(frame)
        end
    end
    if frame > length(positions)-1
        break;
    else
        frame = frame +1;
    end
end


