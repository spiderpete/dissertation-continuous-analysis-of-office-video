% Time execution
tic

% Custamisable parameters
DAY_NUM = '01';
INIT_FRAME = 1;

% Stationary parameters
diagonal = sqrt(720*720 + 1280* 1280);
DAY = ['Day', DAY_NUM];
DIR = './Data/OFFICE/';
READ_DIR_AUTO = [DIR, 'Ground Truths/GT/GT - Auto/'];
READ_DIR_MANUAL = [DIR, 'Ground Truths/GT/GT - Positions/'];

dayFile = dayFiles.(DAY).file;
path = [DIR, dayFile, '/inspacecam163'];
fileList = generateFileList(path);

finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                    % of the video so the frame
                                                    % names can be annotated
matches = strfind(fileList, finishFrameName);
FINISH_FRAME = find(not(cellfun('isempty', matches)));

% Load auto-annotated frames and notes
auto = load([READ_DIR_AUTO, 'Day', DAY_NUM, '.mat']);           % variable = day_01
autoData = auto.day_01;
manual = load([READ_DIR_MANUAL, 'Day', DAY_NUM, '.mat']);       % variable = day_01
manualData = manual.day_01;
NUM_PEOPLE = size(manualData{2}{1}, 1);                         % # of people constant all throught

% Discrepencies holding array
errors = zeros(NUM_PEOPLE, FINISH_FRAME);

for currentFrame = INIT_FRAME : FINISH_FRAME
    
    for person = 1 : NUM_PEOPLE
        try
            manualPosition = manualData{2}{currentFrame}(person, 1:2);
        catch
            manualPosition = [0, 0];
        end
        try 
            automaticPosition = autoData{2}{currentFrame}(person, 1:2);
        catch
            automaticPosition = [0, 0];
        end
        errors(person, currentFrame) = pdist2(manualPosition, automaticPosition, 'euclidean');
    end
    
end

bins = zeros(NUM_PEOPLE, 8);
for person = 1 : NUM_PEOPLE
    for bin = 1 : 8
       bins(person, bin) = sum(errors(person,:) < bin*0.1*diagonal);
    end
end