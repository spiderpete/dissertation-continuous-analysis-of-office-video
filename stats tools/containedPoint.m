function [ isContained ] = containedPoint( point, rectangle )
%CONTINEDPOINT checks if 'point' is wihtin the boundaries of 'rectangle'
%   point is a 2D point
%   rectangle has 4 arguments: x, y, width, height
%
%   isContained returns whether 'point' is contained within or is on the
%   boundaries of 'rectangle'

    isContained = false;
    if (rectangle(1) <= point(1) && point(1) <= rectangle(1) + rectangle(3) && ...
            rectangle(2) <= point(2) && point(2) <= rectangle(2) + rectangle(4))
        isContained = true;
    end
    
end

