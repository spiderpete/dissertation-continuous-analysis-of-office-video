% Custamisable parameters
Algorithms = {'Colours','Edges','Colours+Edges'};

compareAlgorithm(Algorithms{1});
disp('------------------------------------------------------------------');
pause;
compareAlgorithm(Algorithms{2});
disp('------------------------------------------------------------------');
pause;
compareAlgorithm(Algorithms{3});
disp('------------------------------------------------------------------');
disp('Complete!');
clear;
