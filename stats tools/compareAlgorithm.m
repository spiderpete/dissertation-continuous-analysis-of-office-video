function compareAlgorithm( algorithm )
%COMPAREALGORITHM Summary of this function goes here
%   Detailed explanation goes here
    
    DAYS = {'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20'};
    INIT_FRAME = 1;
    SUBJECT = 1;
    BOUNDING_BOX_OVERLAP_THRESHOLD = 0.4;
    INTERSECTION_AREA_OVERLAP_WITH_GT_THRESHOLD = 0.8;
    SAVE_DIR = './Edge Inspection/';
    
    
    

    for DAY_NUM = DAYS
        DAY_NUM = DAY_NUM{1};

        % Stationary parameters
        diagonal = sqrt(720*720 + 1280* 1280);
        DAY = ['Day', DAY_NUM];
        DIR = './Data/OFFICE/';
        READ_DIR_MANUAL = [DIR, 'Ground Truths/GT/GT - Positions/'];
        
        if strcmp(algorithm, 'Colours')
            folder = 'GT - Auto/';
        elseif strcmp(algorithm, 'Edges')
            folder = 'GT - Edges/';
        elseif strcmp(algorithm, 'Colours+Edges')
            folder = 'GT - Colour and Edges/';
        else
            disp('Something is wrong!');
        end
        
        
        READ_DIR_AUTO = [DIR, 'Ground Truths/GT/', folder];
        EXECUTION_ON_WINDOWS = false;

        % Property parameters
        heightVideo1 = 490;
        heightVideo2 = 300;
        heightVideo3 = 355;
        heightVideo4 = 245;
        widthVideo1 = 240;
        widthVideo2 = 190;
        widthVideo3 = 275;
        widthVideo4 = 110;

        if strcmp(DAY_NUM, '01') || strcmp(DAY_NUM, '02') || strcmp(DAY_NUM, '03') || strcmp(DAY_NUM, '04') || strcmp(DAY_NUM, '05') || strcmp(DAY_NUM, '06') || strcmp(DAY_NUM, '07') || strcmp(DAY_NUM, '08') || strcmp(DAY_NUM, '09') || strcmp(DAY_NUM, '10') || strcmp(DAY_NUM, '11') || strcmp(DAY_NUM, '12')
            heightVideo = heightVideo1;
            widthVideo = widthVideo1;
        elseif strcmp(DAY_NUM, '13') || strcmp(DAY_NUM, '14')
            heightVideo = heightVideo2;
            widthVideo = widthVideo2;
        elseif strcmp(DAY_NUM, '15') || strcmp(DAY_NUM, '16') || strcmp(DAY_NUM, '17')
            heightVideo = heightVideo3;
            widthVideo = widthVideo3;
        elseif strcmp(DAY_NUM, '18') || strcmp(DAY_NUM, '19') || strcmp(DAY_NUM, '20')
            heightVideo = heightVideo4;
            widthVideo = widthVideo4;
        else 
            disp('Something is wrong!!');
        end

        dayFile = dayFiles.(DAY).file;
        path = [DIR, dayFile, '/inspacecam163'];
        fileList = generateFileList(path, EXECUTION_ON_WINDOWS);

        finishFrameName = dayFiles.(DAY).endFrame;          % Go until the absolute end
                                                            % of the video so the frame
                                                            % names can be annotated
        matches = strfind(fileList, finishFrameName);
        FINISH_FRAME = find(not(cellfun('isempty', matches)));

        % Load auto-annotated frames and notes
        
        manual = load([READ_DIR_MANUAL, 'Day', DAY_NUM, '.mat']);       % variable = day_01
        manualData = manual.day_01;
        
        auto = load([READ_DIR_AUTO, 'Day', DAY_NUM, '.mat']);           % variable = day_01
        autoData = auto.day_01;
        
        if strcmp(algorithm, 'Edges') || strcmp(algorithm, 'Colours+Edges')
            autoNotes = load([READ_DIR_AUTO, 'Day', DAY_NUM, '_notes', '.mat']);           % variable = day_01
            autoDataNotes = autoNotes.notes;
        end
        
        %% For display purposes
        frame = uint32(rand * FINISH_FRAME);
        attempts = 1;
        display = true;
        while frame == 0 || ...
                manualData{2}{frame}(SUBJECT,1) == 0 && manualData{2}{frame}(SUBJECT,2) == 0 || ...
                autoData{2}{frame}(SUBJECT,1) == 0 && autoData{2}{frame}(SUBJECT,2) == 0
            if attempts > 100
                disp('Tried 100 times to find non-zero frames for diplay purposes');
                display = false;
                break;
            end
        
            frame = uint32(rand * FINISH_FRAME);
            bboxManualDisplay = 0;
            bboxAutoDiplay = 0;
            
            attempts = attempts + 1;
        end
        
        
        %%

        % Stats
        numTruePositives = 0;
        for currentFrame = INIT_FRAME : FINISH_FRAME

            % Special case for emplty room: only can be True Positive in it was
            % automatically detected as such as well
            if manualData{2}{currentFrame}(SUBJECT,1) == 0 && manualData{2}{currentFrame}(SUBJECT,2) == 0
                if autoData{2}{currentFrame}(SUBJECT,1) == 0 && autoData{2}{currentFrame}(SUBJECT,2) == 0
                    numTruePositives = numTruePositives + 1;
                    continue;
                else
                    continue;
                end
            end

            % Special case for DETECTED emplty room: only can be True Positive in it was
            % automatically detected as such as well
            if autoData{2}{currentFrame}(SUBJECT,1) == 0 && autoData{2}{currentFrame}(SUBJECT,2) == 0
                if manualData{2}{currentFrame}(SUBJECT,1) ~= 0 && manualData{2}{currentFrame}(SUBJECT,2) ~= 0
                    continue;
                end
            end
            
            % Construct Bounding Boxes
            xGT = manualData{2}{currentFrame}(SUBJECT,1) - (widthVideo/2);
            if xGT < 1
                xGT = 1;
            end
            yGT = manualData{2}{currentFrame}(SUBJECT,2) - (heightVideo/2);
            if yGT < 1
                yGT = 1;
            end
            bboxGT = [xGT, yGT, widthVideo, heightVideo];
            
            % For display purposes
            if currentFrame == frame
                bboxManualDisplay = bboxGT;
            end

            
            xAuto = autoData{2}{currentFrame}(SUBJECT,1) - widthVideo/2;
            if xAuto < 1
                xAuto = 1;
            end
            yAuto = autoData{2}{currentFrame}(SUBJECT,2) - heightVideo/2;
            if yAuto < 1
                yAuto = 1;
            end
            
            if strcmp(algorithm, 'Colours')
                bboxAuto = [xAuto, yAuto, widthVideo, heightVideo];
            else
                bboxAuto = autoDataNotes{currentFrame}(SUBJECT,:);
            end
            
            % For display purposes
            if currentFrame == frame
                bboxAutoDiplay = bboxAuto;
            end

            overlap = bboxOverlapRatio(bboxGT, bboxAuto);
            
            % Check if Detected (Auto) Bounding Box is within GT Bouding Box (Is Red in Green)
            point1 = bboxAuto(1:2);
            point2 = [bboxAuto(1) + bboxAuto(1), bboxAuto(2)];
            point3 = [bboxAuto(1), bboxAuto(2) + bboxAuto(2)];
            point4 = [bboxAuto(1) + bboxAuto(1), bboxAuto(2) + bboxAuto(2)];
            corner1 = containedPoint(point1, bboxGT);
            corner2 = containedPoint(point2, bboxGT);
            corner3 = containedPoint(point3, bboxGT);
            corner4 = containedPoint(point4, bboxGT);
            if corner1 && corner2 && corner3 && corner4
                numTruePositives = numTruePositives + 1;
                continue;
            end
            
            % Accumate statistics
            if overlap >= BOUNDING_BOX_OVERLAP_THRESHOLD
                numTruePositives = numTruePositives + 1;
                continue
            end
            
            % If Detected (Auto) Bounding Box is mostly within GT bounding
            % Box (Is Red mostly in Green)
            autoArea = bboxAuto(3) * bboxAuto(4);
            intersectArea = rectint(bboxGT, bboxAuto);
            if intersectArea/autoArea >  INTERSECTION_AREA_OVERLAP_WITH_GT_THRESHOLD
                numTruePositives = numTruePositives + 1;
                continue
            end
        end

        accuracy = (numTruePositives*1.0/FINISH_FRAME)*100;
        disp([algorithm, ': True Positives for video ', DAY_NUM, ': ', num2str(accuracy), '% (', num2str(numTruePositives), '/', num2str(FINISH_FRAME), ')']);
            
        % For display purposes
        if display
            sceneFrame = imread(manualData{1}{frame});
            imshow(sceneFrame);
            hold on;
            plot(manualData{2}{frame}(SUBJECT,1), manualData{2}{frame}(SUBJECT,2), 'g*');
            plot(autoData{2}{frame}(SUBJECT,1), autoData{2}{frame}(SUBJECT,2), 'r*');

            rectangle('Position', bboxManualDisplay, 'EdgeColor', 'g', 'LineWidth', 3);
            rectangle('Position', bboxAutoDiplay, 'EdgeColor', 'r', 'LineWidth', 3);

            drawnow;
            hold off;
            
            name = [SAVE_DIR, algorithm, ' - video ', DAY_NUM, ' - Detection Accuracy.tif'];
            saveas(gcf, name);
        end
        
    end
    
    clear;
end

